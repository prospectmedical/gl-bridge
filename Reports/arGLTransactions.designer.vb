<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class arGLTransactions 
  Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub
    
    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(arGLTransactions))
    Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
    Me.Label3 = New DataDynamics.ActiveReports.Label
    Me.lblEntity = New DataDynamics.ActiveReports.Label
    Me.Line1 = New DataDynamics.ActiveReports.Line
    Me.lblReportName1 = New DataDynamics.ActiveReports.Label
    Me.picLogo = New DataDynamics.ActiveReports.Picture
    Me.Label2 = New DataDynamics.ActiveReports.Label
    Me.Label5 = New DataDynamics.ActiveReports.Label
    Me.Detail1 = New DataDynamics.ActiveReports.Detail
    Me.tbAccountNumber = New DataDynamics.ActiveReports.TextBox
    Me.tbDebitAmount = New DataDynamics.ActiveReports.TextBox
    Me.tbCreditAmount = New DataDynamics.ActiveReports.TextBox
    Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
    Me.lblDateTime = New DataDynamics.ActiveReports.Label
    Me.Line2 = New DataDynamics.ActiveReports.Line
    Me.ReportInfo1 = New DataDynamics.ActiveReports.ReportInfo
    Me.ReportHeader1 = New DataDynamics.ActiveReports.ReportHeader
    Me.ReportFooter1 = New DataDynamics.ActiveReports.ReportFooter
    Me.tbDebitAmountTotal = New DataDynamics.ActiveReports.TextBox
    Me.Label4 = New DataDynamics.ActiveReports.Label
    Me.tbCreditAmountTotal = New DataDynamics.ActiveReports.TextBox
    Me.Label1 = New DataDynamics.ActiveReports.Label
    Me.tbErrorMessage = New DataDynamics.ActiveReports.TextBox
    Me.tbTotalErrors = New DataDynamics.ActiveReports.TextBox
    Me.lblreportName2 = New DataDynamics.ActiveReports.Label
    CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblEntity, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblReportName1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbAccountNumber, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbDebitAmount, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbCreditAmount, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblDateTime, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.ReportInfo1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbDebitAmountTotal, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbCreditAmountTotal, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbErrorMessage, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbTotalErrors, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblreportName2, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
    '
    'PageHeader1
    '
    Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label3, Me.lblEntity, Me.Line1, Me.lblReportName1, Me.picLogo, Me.Label2, Me.lblreportName2, Me.Label5, Me.Label1})
    Me.PageHeader1.Height = 1.083333!
    Me.PageHeader1.Name = "PageHeader1"
    '
    'Label3
    '
    Me.Label3.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label3.Border.LeftColor = System.Drawing.Color.Black
    Me.Label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label3.Border.RightColor = System.Drawing.Color.Black
    Me.Label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label3.Border.TopColor = System.Drawing.Color.Black
    Me.Label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label3.Height = 0.1875!
    Me.Label3.HyperLink = ""
    Me.Label3.Left = 0.0!
    Me.Label3.Name = "Label3"
    Me.Label3.Style = "color: Teal; ddo-char-set: 1; text-align: left; font-weight: bold; font-size: 10p" & _
        "t; "
    Me.Label3.Text = "Account Number"
    Me.Label3.Top = 0.875!
    Me.Label3.Width = 1.25!
    '
    'lblEntity
    '
    Me.lblEntity.Border.BottomColor = System.Drawing.Color.Black
    Me.lblEntity.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblEntity.Border.LeftColor = System.Drawing.Color.Black
    Me.lblEntity.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblEntity.Border.RightColor = System.Drawing.Color.Black
    Me.lblEntity.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblEntity.Border.TopColor = System.Drawing.Color.Black
    Me.lblEntity.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblEntity.Height = 0.25!
    Me.lblEntity.HyperLink = ""
    Me.lblEntity.Left = 1.625!
    Me.lblEntity.Name = "lblEntity"
    Me.lblEntity.Style = "color: Teal; ddo-char-set: 1; text-align: center; font-weight: bold; font-size: 1" & _
        "6pt; "
    Me.lblEntity.Text = "lblEntity"
    Me.lblEntity.Top = 0.0!
    Me.lblEntity.Width = 4.25!
    '
    'Line1
    '
    Me.Line1.Border.BottomColor = System.Drawing.Color.Black
    Me.Line1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Line1.Border.LeftColor = System.Drawing.Color.Black
    Me.Line1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Line1.Border.RightColor = System.Drawing.Color.Black
    Me.Line1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Line1.Border.TopColor = System.Drawing.Color.Black
    Me.Line1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Line1.Height = 0.0!
    Me.Line1.Left = 0.0!
    Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Line1.LineWeight = 1.0!
    Me.Line1.Name = "Line1"
    Me.Line1.Top = 0.8125!
    Me.Line1.Width = 7.5!
    Me.Line1.X1 = 0.0!
    Me.Line1.X2 = 7.5!
    Me.Line1.Y1 = 0.8125!
    Me.Line1.Y2 = 0.8125!
    '
    'lblReportName1
    '
    Me.lblReportName1.Border.BottomColor = System.Drawing.Color.Black
    Me.lblReportName1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblReportName1.Border.LeftColor = System.Drawing.Color.Black
    Me.lblReportName1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblReportName1.Border.RightColor = System.Drawing.Color.Black
    Me.lblReportName1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblReportName1.Border.TopColor = System.Drawing.Color.Black
    Me.lblReportName1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblReportName1.Height = 0.25!
    Me.lblReportName1.HyperLink = ""
    Me.lblReportName1.Left = 1.625!
    Me.lblReportName1.Name = "lblReportName1"
    Me.lblReportName1.Style = "color: Teal; ddo-char-set: 1; text-align: center; font-weight: bold; font-size: 1" & _
        "6pt; "
    Me.lblReportName1.Text = "lblReportName1"
    Me.lblReportName1.Top = 0.25!
    Me.lblReportName1.Width = 4.25!
    '
    'picLogo
    '
    Me.picLogo.Border.BottomColor = System.Drawing.Color.Black
    Me.picLogo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.picLogo.Border.LeftColor = System.Drawing.Color.Black
    Me.picLogo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.picLogo.Border.RightColor = System.Drawing.Color.Black
    Me.picLogo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.picLogo.Border.TopColor = System.Drawing.Color.Black
    Me.picLogo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.picLogo.Height = 0.4375!
    Me.picLogo.Image = CType(resources.GetObject("picLogo.Image"), System.Drawing.Image)
    Me.picLogo.ImageData = CType(resources.GetObject("picLogo.ImageData"), System.IO.Stream)
    Me.picLogo.Left = 0.0!
    Me.picLogo.LineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.picLogo.LineWeight = 0.0!
    Me.picLogo.Name = "picLogo"
    Me.picLogo.SizeMode = DataDynamics.ActiveReports.SizeModes.Zoom
    Me.picLogo.Top = 0.0!
    Me.picLogo.Width = 1.552083!
    '
    'Label2
    '
    Me.Label2.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label2.Border.LeftColor = System.Drawing.Color.Black
    Me.Label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label2.Border.RightColor = System.Drawing.Color.Black
    Me.Label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label2.Border.TopColor = System.Drawing.Color.Black
    Me.Label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label2.Height = 0.1875!
    Me.Label2.HyperLink = ""
    Me.Label2.Left = 1.3125!
    Me.Label2.Name = "Label2"
    Me.Label2.Style = "color: Teal; ddo-char-set: 1; text-align: right; font-weight: bold; font-size: 10" & _
        "pt; "
    Me.Label2.Text = "Debit Amount"
    Me.Label2.Top = 0.875!
    Me.Label2.Width = 1.125!
    '
    'Label5
    '
    Me.Label5.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label5.Border.LeftColor = System.Drawing.Color.Black
    Me.Label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label5.Border.RightColor = System.Drawing.Color.Black
    Me.Label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label5.Border.TopColor = System.Drawing.Color.Black
    Me.Label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label5.Height = 0.1875!
    Me.Label5.HyperLink = ""
    Me.Label5.Left = 2.5!
    Me.Label5.Name = "Label5"
    Me.Label5.Style = "color: Teal; ddo-char-set: 1; text-align: right; font-weight: bold; font-size: 10" & _
        "pt; "
    Me.Label5.Text = "Credit Amount"
    Me.Label5.Top = 0.875!
    Me.Label5.Width = 1.125!
    '
    'Detail1
    '
    Me.Detail1.ColumnSpacing = 0.0!
    Me.Detail1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.tbAccountNumber, Me.tbDebitAmount, Me.tbCreditAmount, Me.tbErrorMessage})
    Me.Detail1.Height = 0.2083333!
    Me.Detail1.Name = "Detail1"
    '
    'tbAccountNumber
    '
    Me.tbAccountNumber.Border.BottomColor = System.Drawing.Color.Black
    Me.tbAccountNumber.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbAccountNumber.Border.LeftColor = System.Drawing.Color.Black
    Me.tbAccountNumber.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbAccountNumber.Border.RightColor = System.Drawing.Color.Black
    Me.tbAccountNumber.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbAccountNumber.Border.TopColor = System.Drawing.Color.Black
    Me.tbAccountNumber.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbAccountNumber.DataField = "FullAccountNumber"
    Me.tbAccountNumber.Height = 0.1875!
    Me.tbAccountNumber.Left = 0.0!
    Me.tbAccountNumber.Name = "tbAccountNumber"
    Me.tbAccountNumber.Style = "font-size: 10pt; "
    Me.tbAccountNumber.Text = Nothing
    Me.tbAccountNumber.Top = 0.0!
    Me.tbAccountNumber.Width = 1.25!
    '
    'tbDebitAmount
    '
    Me.tbDebitAmount.Border.BottomColor = System.Drawing.Color.Black
    Me.tbDebitAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbDebitAmount.Border.LeftColor = System.Drawing.Color.Black
    Me.tbDebitAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbDebitAmount.Border.RightColor = System.Drawing.Color.Black
    Me.tbDebitAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbDebitAmount.Border.TopColor = System.Drawing.Color.Black
    Me.tbDebitAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbDebitAmount.Height = 0.1875!
    Me.tbDebitAmount.Left = 1.3125!
    Me.tbDebitAmount.Name = "tbDebitAmount"
    Me.tbDebitAmount.OutputFormat = resources.GetString("tbDebitAmount.OutputFormat")
    Me.tbDebitAmount.Style = "text-align: right; font-size: 10pt; "
    Me.tbDebitAmount.Text = Nothing
    Me.tbDebitAmount.Top = 0.0!
    Me.tbDebitAmount.Width = 1.125!
    '
    'tbCreditAmount
    '
    Me.tbCreditAmount.Border.BottomColor = System.Drawing.Color.Black
    Me.tbCreditAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbCreditAmount.Border.LeftColor = System.Drawing.Color.Black
    Me.tbCreditAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbCreditAmount.Border.RightColor = System.Drawing.Color.Black
    Me.tbCreditAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbCreditAmount.Border.TopColor = System.Drawing.Color.Black
    Me.tbCreditAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbCreditAmount.Height = 0.1875!
    Me.tbCreditAmount.Left = 2.5!
    Me.tbCreditAmount.Name = "tbCreditAmount"
    Me.tbCreditAmount.OutputFormat = resources.GetString("tbCreditAmount.OutputFormat")
    Me.tbCreditAmount.Style = "text-align: right; font-size: 10pt; "
    Me.tbCreditAmount.Text = Nothing
    Me.tbCreditAmount.Top = 0.0!
    Me.tbCreditAmount.Width = 1.125!
    '
    'PageFooter1
    '
    Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblDateTime, Me.Line2, Me.ReportInfo1})
    Me.PageFooter1.Height = 0.28125!
    Me.PageFooter1.Name = "PageFooter1"
    '
    'lblDateTime
    '
    Me.lblDateTime.Border.BottomColor = System.Drawing.Color.Black
    Me.lblDateTime.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblDateTime.Border.LeftColor = System.Drawing.Color.Black
    Me.lblDateTime.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblDateTime.Border.RightColor = System.Drawing.Color.Black
    Me.lblDateTime.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblDateTime.Border.TopColor = System.Drawing.Color.Black
    Me.lblDateTime.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblDateTime.Height = 0.1875!
    Me.lblDateTime.HyperLink = ""
    Me.lblDateTime.Left = 0.0!
    Me.lblDateTime.Name = "lblDateTime"
    Me.lblDateTime.Style = "color: Teal; ddo-char-set: 1; text-align: left; font-weight: normal; font-size: 8" & _
        "pt; "
    Me.lblDateTime.Text = "C"
    Me.lblDateTime.Top = 0.0625!
    Me.lblDateTime.Width = 3.75!
    '
    'Line2
    '
    Me.Line2.Border.BottomColor = System.Drawing.Color.Black
    Me.Line2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Line2.Border.LeftColor = System.Drawing.Color.Black
    Me.Line2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Line2.Border.RightColor = System.Drawing.Color.Black
    Me.Line2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Line2.Border.TopColor = System.Drawing.Color.Black
    Me.Line2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Line2.Height = 0.0!
    Me.Line2.Left = 0.0!
    Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Line2.LineWeight = 1.0!
    Me.Line2.Name = "Line2"
    Me.Line2.Top = 0.0!
    Me.Line2.Width = 7.5!
    Me.Line2.X1 = 0.0!
    Me.Line2.X2 = 7.5!
    Me.Line2.Y1 = 0.0!
    Me.Line2.Y2 = 0.0!
    '
    'ReportInfo1
    '
    Me.ReportInfo1.Border.BottomColor = System.Drawing.Color.Black
    Me.ReportInfo1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.ReportInfo1.Border.LeftColor = System.Drawing.Color.Black
    Me.ReportInfo1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.ReportInfo1.Border.RightColor = System.Drawing.Color.Black
    Me.ReportInfo1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.ReportInfo1.Border.TopColor = System.Drawing.Color.Black
    Me.ReportInfo1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.ReportInfo1.FormatString = "Page {PageNumber} of {PageCount}"
    Me.ReportInfo1.Height = 0.1875!
    Me.ReportInfo1.Left = 4.625!
    Me.ReportInfo1.Name = "ReportInfo1"
    Me.ReportInfo1.Style = "color: Teal; text-align: right; font-weight: bold; "
    Me.ReportInfo1.Top = 0.0625!
    Me.ReportInfo1.Width = 2.8125!
    '
    'ReportHeader1
    '
    Me.ReportHeader1.Height = 0.0!
    Me.ReportHeader1.Name = "ReportHeader1"
    Me.ReportHeader1.Visible = False
    '
    'ReportFooter1
    '
    Me.ReportFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.tbDebitAmountTotal, Me.Label4, Me.tbCreditAmountTotal, Me.tbTotalErrors})
    Me.ReportFooter1.Height = 0.2083333!
    Me.ReportFooter1.Name = "ReportFooter1"
    '
    'tbDebitAmountTotal
    '
    Me.tbDebitAmountTotal.Border.BottomColor = System.Drawing.Color.Black
    Me.tbDebitAmountTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbDebitAmountTotal.Border.LeftColor = System.Drawing.Color.Black
    Me.tbDebitAmountTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbDebitAmountTotal.Border.RightColor = System.Drawing.Color.Black
    Me.tbDebitAmountTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbDebitAmountTotal.Border.TopColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.tbDebitAmountTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.[Double]
    Me.tbDebitAmountTotal.Height = 0.1875!
    Me.tbDebitAmountTotal.Left = 1.3125!
    Me.tbDebitAmountTotal.Name = "tbDebitAmountTotal"
    Me.tbDebitAmountTotal.OutputFormat = resources.GetString("tbDebitAmountTotal.OutputFormat")
    Me.tbDebitAmountTotal.Style = "text-align: right; font-size: 10pt; "
    Me.tbDebitAmountTotal.Text = Nothing
    Me.tbDebitAmountTotal.Top = 0.0!
    Me.tbDebitAmountTotal.Width = 1.125!
    '
    'Label4
    '
    Me.Label4.Border.BottomColor = System.Drawing.Color.Black
    Me.Label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label4.Border.LeftColor = System.Drawing.Color.Black
    Me.Label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label4.Border.RightColor = System.Drawing.Color.Black
    Me.Label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label4.Border.TopColor = System.Drawing.Color.Black
    Me.Label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label4.Height = 0.1875!
    Me.Label4.HyperLink = ""
    Me.Label4.Left = 0.0625!
    Me.Label4.Name = "Label4"
    Me.Label4.Style = "color: Teal; ddo-char-set: 1; text-align: right; font-weight: bold; font-size: 10" & _
        "pt; "
    Me.Label4.Text = "Total:"
    Me.Label4.Top = 0.0!
    Me.Label4.Width = 1.1875!
    '
    'tbCreditAmountTotal
    '
    Me.tbCreditAmountTotal.Border.BottomColor = System.Drawing.Color.Black
    Me.tbCreditAmountTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbCreditAmountTotal.Border.LeftColor = System.Drawing.Color.Black
    Me.tbCreditAmountTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbCreditAmountTotal.Border.RightColor = System.Drawing.Color.Black
    Me.tbCreditAmountTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbCreditAmountTotal.Border.TopColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.tbCreditAmountTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.[Double]
    Me.tbCreditAmountTotal.Height = 0.1875!
    Me.tbCreditAmountTotal.Left = 2.5!
    Me.tbCreditAmountTotal.Name = "tbCreditAmountTotal"
    Me.tbCreditAmountTotal.OutputFormat = resources.GetString("tbCreditAmountTotal.OutputFormat")
    Me.tbCreditAmountTotal.Style = "text-align: right; font-size: 10pt; "
    Me.tbCreditAmountTotal.Text = Nothing
    Me.tbCreditAmountTotal.Top = 0.0!
    Me.tbCreditAmountTotal.Width = 1.125!
    '
    'Label1
    '
    Me.Label1.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label1.Border.LeftColor = System.Drawing.Color.Black
    Me.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label1.Border.RightColor = System.Drawing.Color.Black
    Me.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label1.Border.TopColor = System.Drawing.Color.Black
    Me.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.Label1.Height = 0.1875!
    Me.Label1.HyperLink = ""
    Me.Label1.Left = 3.6875!
    Me.Label1.Name = "Label1"
    Me.Label1.Style = "color: Teal; ddo-char-set: 1; text-align: left; font-weight: bold; font-size: 10p" & _
        "t; "
    Me.Label1.Text = "Error Message"
    Me.Label1.Top = 0.875!
    Me.Label1.Width = 3.75!
    '
    'tbErrorMessage
    '
    Me.tbErrorMessage.Border.BottomColor = System.Drawing.Color.Black
    Me.tbErrorMessage.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbErrorMessage.Border.LeftColor = System.Drawing.Color.Black
    Me.tbErrorMessage.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbErrorMessage.Border.RightColor = System.Drawing.Color.Black
    Me.tbErrorMessage.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbErrorMessage.Border.TopColor = System.Drawing.Color.Black
    Me.tbErrorMessage.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbErrorMessage.Height = 0.1875!
    Me.tbErrorMessage.Left = 3.6875!
    Me.tbErrorMessage.Name = "tbErrorMessage"
    Me.tbErrorMessage.OutputFormat = resources.GetString("tbErrorMessage.OutputFormat")
    Me.tbErrorMessage.Style = "color: Red; text-align: left; font-size: 10pt; "
    Me.tbErrorMessage.Text = Nothing
    Me.tbErrorMessage.Top = 0.0!
    Me.tbErrorMessage.Width = 3.75!
    '
    'tbTotalErrors
    '
    Me.tbTotalErrors.Border.BottomColor = System.Drawing.Color.Black
    Me.tbTotalErrors.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbTotalErrors.Border.LeftColor = System.Drawing.Color.Black
    Me.tbTotalErrors.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbTotalErrors.Border.RightColor = System.Drawing.Color.Black
    Me.tbTotalErrors.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.tbTotalErrors.Border.TopColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
    Me.tbTotalErrors.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.[Double]
    Me.tbTotalErrors.Height = 0.1875!
    Me.tbTotalErrors.Left = 3.6875!
    Me.tbTotalErrors.Name = "tbTotalErrors"
    Me.tbTotalErrors.OutputFormat = resources.GetString("tbTotalErrors.OutputFormat")
    Me.tbTotalErrors.Style = "color: Red; text-align: left; font-size: 10pt; "
    Me.tbTotalErrors.Text = Nothing
    Me.tbTotalErrors.Top = 0.0!
    Me.tbTotalErrors.Width = 1.3125!
    '
    'lblreportName2
    '
    Me.lblreportName2.Border.BottomColor = System.Drawing.Color.Black
    Me.lblreportName2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblreportName2.Border.LeftColor = System.Drawing.Color.Black
    Me.lblreportName2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblreportName2.Border.RightColor = System.Drawing.Color.Black
    Me.lblreportName2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblreportName2.Border.TopColor = System.Drawing.Color.Black
    Me.lblreportName2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
    Me.lblreportName2.Height = 0.25!
    Me.lblreportName2.HyperLink = ""
    Me.lblreportName2.Left = 1.625!
    Me.lblreportName2.Name = "lblreportName2"
    Me.lblreportName2.Style = "color: Teal; ddo-char-set: 1; text-align: center; font-weight: bold; font-size: 1" & _
        "6pt; "
    Me.lblreportName2.Text = "lblReportName2"
    Me.lblreportName2.Top = 0.5!
    Me.lblreportName2.Width = 4.25!
    '
    'arGLTransactions
    '
    Me.MasterReport = False
    Me.PageSettings.Margins.Bottom = 0.5!
    Me.PageSettings.Margins.Left = 0.5!
    Me.PageSettings.Margins.Right = 0.5!
    Me.PageSettings.Margins.Top = 0.5!
    Me.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait
    Me.PageSettings.PaperHeight = 11.0!
    Me.PageSettings.PaperWidth = 8.5!
    Me.PrintWidth = 7.5!
    Me.Sections.Add(Me.ReportHeader1)
    Me.Sections.Add(Me.PageHeader1)
    Me.Sections.Add(Me.Detail1)
    Me.Sections.Add(Me.PageFooter1)
    Me.Sections.Add(Me.ReportFooter1)
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                "l; font-size: 10pt; color: Black; ", "Normal"))
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; ", "Heading1", "Normal"))
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                "lic; ", "Heading2", "Normal"))
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ", "Heading3", "Normal"))
    CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblEntity, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblReportName1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbAccountNumber, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbDebitAmount, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbCreditAmount, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblDateTime, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.ReportInfo1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbDebitAmountTotal, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbCreditAmountTotal, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbErrorMessage, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbTotalErrors, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblreportName2, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

  End Sub
  Private WithEvents Label3 As DataDynamics.ActiveReports.Label
  Private WithEvents lblEntity As DataDynamics.ActiveReports.Label
  Private WithEvents Line1 As DataDynamics.ActiveReports.Line
  Private WithEvents lblReportName1 As DataDynamics.ActiveReports.Label
  Private WithEvents picLogo As DataDynamics.ActiveReports.Picture
  Private WithEvents Label2 As DataDynamics.ActiveReports.Label
  Private WithEvents tbAccountNumber As DataDynamics.ActiveReports.TextBox
  Private WithEvents tbDebitAmount As DataDynamics.ActiveReports.TextBox
  Private WithEvents lblDateTime As DataDynamics.ActiveReports.Label
  Private WithEvents Line2 As DataDynamics.ActiveReports.Line
  Friend WithEvents ReportInfo1 As DataDynamics.ActiveReports.ReportInfo
  Friend WithEvents ReportHeader1 As DataDynamics.ActiveReports.ReportHeader
  Friend WithEvents ReportFooter1 As DataDynamics.ActiveReports.ReportFooter
  Private WithEvents tbDebitAmountTotal As DataDynamics.ActiveReports.TextBox
  Private WithEvents Label4 As DataDynamics.ActiveReports.Label
  Private WithEvents Label5 As DataDynamics.ActiveReports.Label
  Private WithEvents tbCreditAmount As DataDynamics.ActiveReports.TextBox
  Private WithEvents tbCreditAmountTotal As DataDynamics.ActiveReports.TextBox
  Private WithEvents Label1 As DataDynamics.ActiveReports.Label
  Private WithEvents tbErrorMessage As DataDynamics.ActiveReports.TextBox
  Private WithEvents tbTotalErrors As DataDynamics.ActiveReports.TextBox
  Private WithEvents lblreportName2 As DataDynamics.ActiveReports.Label
End Class 
