#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class arDepartmentListByNumber
#Region "Declarations"
  Private moRdr As OleDbDataReader
  Private msReportName1 As String
  Private miGLEntityID As Int32
#End Region

#Region "Routines"
  Public Function Component(ByVal ReportName1 As String, ByVal GLEntityID As Int32) As Boolean
    Dim oGLEntity As New GLEntity

    msReportName1 = ReportName1
    miGLEntityID = GLEntityID
    lblEntity.Text = oGLEntity.Description(miGLEntityID)
    lblReportName1.Text = msReportName1
    Return True
  End Function
#End Region

#Region "Data"
  Private Sub arDepartmentListByNumber_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
    Dim oDepartment As New Department

    moRdr = oDepartment.ListByNumber(miGLEntityID)
    If moRdr.HasRows Then
      Me.DataSource = moRdr
    Else
      OKMessage("Print " & msReportName1, "Error: No data found." & vbCrLf & "Processing Canceled!", 3)
      Me.Cancel()
    End If
    lblDateTime.Text = "Created on " & Format$(Now, "MM/dd/yyyy") & " at " & Format$(Now, "hh:mm:ss tt")
  End Sub

  Private Sub arDepartmentListByNumber_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportEnd
    CloseRDR(moRdr)
  End Sub
#End Region

#Region "Report Sections"
  Private Sub PageHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageHeader1.Format
    Exit Sub
  End Sub

  Private Sub Detail1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail1.Format
    Exit Sub
  End Sub

  Private Sub PageFooter1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageFooter1.Format
    Exit Sub
  End Sub
#End Region
End Class
