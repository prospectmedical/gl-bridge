<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class arGLTransactionDetail 
  Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub
    
    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(arGLTransactionDetail))
    Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
    Me.lblEntity = New DataDynamics.ActiveReports.Label
    Me.Line1 = New DataDynamics.ActiveReports.Line
    Me.lblReportName1 = New DataDynamics.ActiveReports.Label
    Me.picLogo = New DataDynamics.ActiveReports.Picture
    Me.lblReportName2 = New DataDynamics.ActiveReports.Label
    Me.Detail1 = New DataDynamics.ActiveReports.Detail
    Me.tbDeatailItemNumber = New DataDynamics.ActiveReports.TextBox
    Me.TextBox1 = New DataDynamics.ActiveReports.TextBox
    Me.TextBox2 = New DataDynamics.ActiveReports.TextBox
    Me.TextBox3 = New DataDynamics.ActiveReports.TextBox
    Me.TextBox4 = New DataDynamics.ActiveReports.TextBox
    Me.TextBox5 = New DataDynamics.ActiveReports.TextBox
    Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
    Me.lblDateTime = New DataDynamics.ActiveReports.Label
    Me.Line2 = New DataDynamics.ActiveReports.Line
    Me.tbPageNumber = New DataDynamics.ActiveReports.ReportInfo
    Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader
    Me.tbDepartment = New DataDynamics.ActiveReports.TextBox
    Me.Label8 = New DataDynamics.ActiveReports.Label
    Me.Label9 = New DataDynamics.ActiveReports.Label
    Me.Label10 = New DataDynamics.ActiveReports.Label
    Me.Label11 = New DataDynamics.ActiveReports.Label
    Me.Label12 = New DataDynamics.ActiveReports.Label
    Me.Label13 = New DataDynamics.ActiveReports.Label
    Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter
    Me.TextBox12 = New DataDynamics.ActiveReports.TextBox
    Me.lblTotalCost = New DataDynamics.ActiveReports.Label
    Me.GroupHeader2 = New DataDynamics.ActiveReports.GroupHeader
    Me.GroupFooter2 = New DataDynamics.ActiveReports.GroupFooter
    Me.GroupHeader3 = New DataDynamics.ActiveReports.GroupHeader
    Me.GroupFooter3 = New DataDynamics.ActiveReports.GroupFooter
    Me.tbItemNumber = New DataDynamics.ActiveReports.TextBox
    Me.tbItemDescription = New DataDynamics.ActiveReports.TextBox
    Me.tbChargeable = New DataDynamics.ActiveReports.TextBox
    Me.tbUnitOfMeasure = New DataDynamics.ActiveReports.TextBox
    Me.tbQuantity = New DataDynamics.ActiveReports.TextBox
    Me.tbCost = New DataDynamics.ActiveReports.TextBox
    Me.ReportHeader1 = New DataDynamics.ActiveReports.ReportHeader
    Me.ReportFooter1 = New DataDynamics.ActiveReports.ReportFooter
    Me.Label1 = New DataDynamics.ActiveReports.Label
    Me.TextBox6 = New DataDynamics.ActiveReports.TextBox
    Me.lblMessage = New DataDynamics.ActiveReports.Label
    CType(Me.lblEntity, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblReportName1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblReportName2, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbDeatailItemNumber, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblDateTime, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbPageNumber, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbDepartment, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TextBox12, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblTotalCost, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbItemNumber, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbItemDescription, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbChargeable, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbUnitOfMeasure, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbQuantity, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbCost, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblMessage, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
    '
    'PageHeader1
    '
    Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblEntity, Me.Line1, Me.lblReportName1, Me.picLogo, Me.lblReportName2})
    Me.PageHeader1.Height = 1.0625!
    Me.PageHeader1.Name = "PageHeader1"
    '
    'lblEntity
    '
    Me.lblEntity.Height = 0.312!
    Me.lblEntity.HyperLink = ""
    Me.lblEntity.Left = 1.625!
    Me.lblEntity.Name = "lblEntity"
    Me.lblEntity.Style = "color: Teal; font-size: 16pt; font-weight: bold; text-align: center; ddo-char-set" & _
        ": 1"
    Me.lblEntity.Text = "lblEntity"
    Me.lblEntity.Top = 0.0!
    Me.lblEntity.Width = 6.75!
    '
    'Line1
    '
    Me.Line1.Height = 0.0!
    Me.Line1.Left = 0.0!
    Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Line1.LineWeight = 1.0!
    Me.Line1.Name = "Line1"
    Me.Line1.Top = 1.0!
    Me.Line1.Width = 9.9375!
    Me.Line1.X1 = 0.0!
    Me.Line1.X2 = 9.9375!
    Me.Line1.Y1 = 1.0!
    Me.Line1.Y2 = 1.0!
    '
    'lblReportName1
    '
    Me.lblReportName1.Height = 0.313!
    Me.lblReportName1.HyperLink = ""
    Me.lblReportName1.Left = 1.625!
    Me.lblReportName1.Name = "lblReportName1"
    Me.lblReportName1.Style = "color: Teal; font-size: 16pt; font-weight: bold; text-align: center; ddo-char-set" & _
        ": 1"
    Me.lblReportName1.Text = "lblReportName1"
    Me.lblReportName1.Top = 0.312!
    Me.lblReportName1.Width = 6.75!
    '
    'picLogo
    '
    Me.picLogo.Height = 0.4375!
    Me.picLogo.ImageData = CType(resources.GetObject("picLogo.ImageData"), System.IO.Stream)
    Me.picLogo.Left = 0.0!
    Me.picLogo.LineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.picLogo.Name = "picLogo"
    Me.picLogo.SizeMode = DataDynamics.ActiveReports.SizeModes.Zoom
    Me.picLogo.Top = 0.0!
    Me.picLogo.Width = 1.552083!
    '
    'lblReportName2
    '
    Me.lblReportName2.Height = 0.312!
    Me.lblReportName2.HyperLink = ""
    Me.lblReportName2.Left = 1.625!
    Me.lblReportName2.Name = "lblReportName2"
    Me.lblReportName2.Style = "color: Teal; font-size: 16pt; font-weight: bold; text-align: center; ddo-char-set" & _
        ": 1"
    Me.lblReportName2.Text = "lblReportName2"
    Me.lblReportName2.Top = 0.625!
    Me.lblReportName2.Width = 6.75!
    '
    'Detail1
    '
    Me.Detail1.ColumnSpacing = 0.0!
    Me.Detail1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.tbDeatailItemNumber, Me.TextBox1, Me.TextBox2, Me.TextBox3, Me.TextBox4, Me.TextBox5})
    Me.Detail1.Height = 0.2!
    Me.Detail1.Name = "Detail1"
    Me.Detail1.Visible = False
    '
    'tbDeatailItemNumber
    '
    Me.tbDeatailItemNumber.DataField = "ItemNumber"
    Me.tbDeatailItemNumber.Height = 0.1875!
    Me.tbDeatailItemNumber.Left = 0.0!
    Me.tbDeatailItemNumber.Name = "tbDeatailItemNumber"
    Me.tbDeatailItemNumber.Style = "color: Gold; font-size: 10pt"
    Me.tbDeatailItemNumber.Text = Nothing
    Me.tbDeatailItemNumber.Top = 0.0!
    Me.tbDeatailItemNumber.Width = 0.9375!
    '
    'TextBox1
    '
    Me.TextBox1.DataField = "ItemDescription"
    Me.TextBox1.Height = 0.1875!
    Me.TextBox1.Left = 1.0!
    Me.TextBox1.Name = "TextBox1"
    Me.TextBox1.Style = "color: Gold; font-size: 10pt"
    Me.TextBox1.Text = Nothing
    Me.TextBox1.Top = 0.0!
    Me.TextBox1.Width = 3.75!
    '
    'TextBox2
    '
    Me.TextBox2.DataField = "Chargeable"
    Me.TextBox2.Height = 0.1875!
    Me.TextBox2.Left = 4.8125!
    Me.TextBox2.Name = "TextBox2"
    Me.TextBox2.Style = "color: Gold; font-size: 10pt; text-align: center"
    Me.TextBox2.Text = Nothing
    Me.TextBox2.Top = 0.0!
    Me.TextBox2.Width = 0.8125!
    '
    'TextBox3
    '
    Me.TextBox3.DataField = "UnitOfMeasure"
    Me.TextBox3.Height = 0.1875!
    Me.TextBox3.Left = 5.6875!
    Me.TextBox3.Name = "TextBox3"
    Me.TextBox3.Style = "color: Gold; font-size: 10pt; text-align: center"
    Me.TextBox3.Text = Nothing
    Me.TextBox3.Top = 0.0!
    Me.TextBox3.Width = 1.125!
    '
    'TextBox4
    '
    Me.TextBox4.DataField = "Quantity"
    Me.TextBox4.Height = 0.1875!
    Me.TextBox4.Left = 6.875!
    Me.TextBox4.Name = "TextBox4"
    Me.TextBox4.Style = "color: Gold; font-size: 10pt; text-align: right"
    Me.TextBox4.Text = Nothing
    Me.TextBox4.Top = 0.0!
    Me.TextBox4.Width = 0.875!
    '
    'TextBox5
    '
    Me.TextBox5.DataField = "Amount"
    Me.TextBox5.Height = 0.1875!
    Me.TextBox5.Left = 7.875!
    Me.TextBox5.Name = "TextBox5"
    Me.TextBox5.Style = "color: Gold; font-size: 10pt; text-align: right"
    Me.TextBox5.Text = Nothing
    Me.TextBox5.Top = 0.0!
    Me.TextBox5.Width = 1.125!
    '
    'PageFooter1
    '
    Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblDateTime, Me.Line2, Me.tbPageNumber})
    Me.PageFooter1.Height = 0.2708334!
    Me.PageFooter1.Name = "PageFooter1"
    '
    'lblDateTime
    '
    Me.lblDateTime.Height = 0.1875!
    Me.lblDateTime.HyperLink = ""
    Me.lblDateTime.Left = 0.0!
    Me.lblDateTime.Name = "lblDateTime"
    Me.lblDateTime.Style = "color: Teal; font-size: 8pt; font-weight: normal; text-align: left; ddo-char-set:" & _
        " 1"
    Me.lblDateTime.Text = "C"
    Me.lblDateTime.Top = 0.06200001!
    Me.lblDateTime.Width = 3.75!
    '
    'Line2
    '
    Me.Line2.Height = 0.0!
    Me.Line2.Left = 0.0!
    Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Line2.LineWeight = 1.0!
    Me.Line2.Name = "Line2"
    Me.Line2.Top = 0.0!
    Me.Line2.Width = 9.9375!
    Me.Line2.X1 = 0.0!
    Me.Line2.X2 = 9.9375!
    Me.Line2.Y1 = 0.0!
    Me.Line2.Y2 = 0.0!
    '
    'tbPageNumber
    '
    Me.tbPageNumber.FormatString = "Page {PageNumber} of {PageCount}"
    Me.tbPageNumber.Height = 0.1875!
    Me.tbPageNumber.Left = 7.25!
    Me.tbPageNumber.Name = "tbPageNumber"
    Me.tbPageNumber.Style = "color: Teal; font-weight: bold; text-align: right"
    Me.tbPageNumber.SummaryGroup = "GroupHeader1"
    Me.tbPageNumber.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
    Me.tbPageNumber.Top = 0.06200001!
    Me.tbPageNumber.Width = 2.6875!
    '
    'GroupHeader1
    '
    Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.tbDepartment, Me.Label8, Me.Label9, Me.Label10, Me.Label11, Me.Label12, Me.Label13})
    Me.GroupHeader1.DataField = "FullAccountNumber"
    Me.GroupHeader1.Height = 0.3854167!
    Me.GroupHeader1.KeepTogether = True
    Me.GroupHeader1.Name = "GroupHeader1"
    Me.GroupHeader1.RepeatStyle = DataDynamics.ActiveReports.RepeatStyle.OnPageIncludeNoDetail
    '
    'tbDepartment
    '
    Me.tbDepartment.Height = 0.1875!
    Me.tbDepartment.Left = 0.0!
    Me.tbDepartment.Name = "tbDepartment"
    Me.tbDepartment.Style = "font-size: 10pt; font-weight: bold"
    Me.tbDepartment.Text = Nothing
    Me.tbDepartment.Top = 0.0!
    Me.tbDepartment.Width = 9.6875!
    '
    'Label8
    '
    Me.Label8.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label8.Height = 0.1875!
    Me.Label8.HyperLink = ""
    Me.Label8.Left = 0.0!
    Me.Label8.Name = "Label8"
    Me.Label8.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: " & _
        "1"
    Me.Label8.Text = "Item Number"
    Me.Label8.Top = 0.1875!
    Me.Label8.Width = 0.9375!
    '
    'Label9
    '
    Me.Label9.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label9.Height = 0.1875!
    Me.Label9.HyperLink = ""
    Me.Label9.Left = 1.0!
    Me.Label9.Name = "Label9"
    Me.Label9.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: " & _
        "1"
    Me.Label9.Text = "Item Description"
    Me.Label9.Top = 0.1875!
    Me.Label9.Width = 3.75!
    '
    'Label10
    '
    Me.Label10.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label10.Height = 0.1875!
    Me.Label10.HyperLink = ""
    Me.Label10.Left = 4.8125!
    Me.Label10.Name = "Label10"
    Me.Label10.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: center; ddo-char-set" & _
        ": 1"
    Me.Label10.Text = "Chargeable"
    Me.Label10.Top = 0.1875!
    Me.Label10.Width = 0.8125!
    '
    'Label11
    '
    Me.Label11.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label11.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label11.Height = 0.1875!
    Me.Label11.HyperLink = ""
    Me.Label11.Left = 5.6875!
    Me.Label11.Name = "Label11"
    Me.Label11.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: center; ddo-char-set" & _
        ": 1"
    Me.Label11.Text = "Unit Of Measure"
    Me.Label11.Top = 0.1875!
    Me.Label11.Width = 1.125!
    '
    'Label12
    '
    Me.Label12.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label12.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label12.Height = 0.1875!
    Me.Label12.HyperLink = ""
    Me.Label12.Left = 6.875!
    Me.Label12.Name = "Label12"
    Me.Label12.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: right; ddo-char-set:" & _
        " 1"
    Me.Label12.Text = "Quantity"
    Me.Label12.Top = 0.1875!
    Me.Label12.Width = 0.9375!
    '
    'Label13
    '
    Me.Label13.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label13.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label13.Height = 0.1875!
    Me.Label13.HyperLink = ""
    Me.Label13.Left = 7.875!
    Me.Label13.Name = "Label13"
    Me.Label13.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: right; ddo-char-set:" & _
        " 1"
    Me.Label13.Text = "Cost"
    Me.Label13.Top = 0.1875!
    Me.Label13.Width = 1.1875!
    '
    'GroupFooter1
    '
    Me.GroupFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox12, Me.lblTotalCost, Me.lblMessage})
    Me.GroupFooter1.Height = 0.2!
    Me.GroupFooter1.Name = "GroupFooter1"
    Me.GroupFooter1.NewPage = DataDynamics.ActiveReports.NewPage.After
    '
    'TextBox12
    '
    Me.TextBox12.Border.TopColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.TextBox12.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.[Double]
    Me.TextBox12.DataField = "Amount"
    Me.TextBox12.Height = 0.1875!
    Me.TextBox12.Left = 7.875!
    Me.TextBox12.Name = "TextBox12"
    Me.TextBox12.OutputFormat = resources.GetString("TextBox12.OutputFormat")
    Me.TextBox12.Style = "font-size: 10pt; text-align: right"
    Me.TextBox12.SummaryGroup = "GroupHeader1"
    Me.TextBox12.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
    Me.TextBox12.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
    Me.TextBox12.Text = Nothing
    Me.TextBox12.Top = 0.0!
    Me.TextBox12.Width = 1.125!
    '
    'lblTotalCost
    '
    Me.lblTotalCost.Height = 0.1875!
    Me.lblTotalCost.HyperLink = ""
    Me.lblTotalCost.Left = 5.6875!
    Me.lblTotalCost.Name = "lblTotalCost"
    Me.lblTotalCost.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: right; ddo-char-set:" & _
        " 1"
    Me.lblTotalCost.Text = "Total Cost:"
    Me.lblTotalCost.Top = 0.0!
    Me.lblTotalCost.Width = 2.125!
    '
    'GroupHeader2
    '
    Me.GroupHeader2.DataField = "ItemNumber"
    Me.GroupHeader2.Height = 0.0!
    Me.GroupHeader2.Name = "GroupHeader2"
    Me.GroupHeader2.Visible = False
    '
    'GroupFooter2
    '
    Me.GroupFooter2.Height = 0.0!
    Me.GroupFooter2.Name = "GroupFooter2"
    Me.GroupFooter2.Visible = False
    '
    'GroupHeader3
    '
    Me.GroupHeader3.DataField = "Chargeable"
    Me.GroupHeader3.Height = 0.0!
    Me.GroupHeader3.Name = "GroupHeader3"
    Me.GroupHeader3.Visible = False
    '
    'GroupFooter3
    '
    Me.GroupFooter3.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.tbItemNumber, Me.tbItemDescription, Me.tbChargeable, Me.tbUnitOfMeasure, Me.tbQuantity, Me.tbCost})
    Me.GroupFooter3.Height = 0.2!
    Me.GroupFooter3.Name = "GroupFooter3"
    '
    'tbItemNumber
    '
    Me.tbItemNumber.DataField = "ItemNumber"
    Me.tbItemNumber.Height = 0.1875!
    Me.tbItemNumber.Left = 0.0!
    Me.tbItemNumber.Name = "tbItemNumber"
    Me.tbItemNumber.Style = "font-size: 10pt"
    Me.tbItemNumber.Text = Nothing
    Me.tbItemNumber.Top = 0.0!
    Me.tbItemNumber.Width = 0.9375!
    '
    'tbItemDescription
    '
    Me.tbItemDescription.DataField = "ItemDescription"
    Me.tbItemDescription.Height = 0.1875!
    Me.tbItemDescription.Left = 1.0!
    Me.tbItemDescription.Name = "tbItemDescription"
    Me.tbItemDescription.Style = "font-size: 10pt"
    Me.tbItemDescription.Text = Nothing
    Me.tbItemDescription.Top = 0.0!
    Me.tbItemDescription.Width = 3.75!
    '
    'tbChargeable
    '
    Me.tbChargeable.Height = 0.1875!
    Me.tbChargeable.Left = 4.8125!
    Me.tbChargeable.Name = "tbChargeable"
    Me.tbChargeable.Style = "font-size: 10pt; text-align: center"
    Me.tbChargeable.Text = Nothing
    Me.tbChargeable.Top = 0.0!
    Me.tbChargeable.Width = 0.8125!
    '
    'tbUnitOfMeasure
    '
    Me.tbUnitOfMeasure.DataField = "UnitOfMeasure"
    Me.tbUnitOfMeasure.Height = 0.1875!
    Me.tbUnitOfMeasure.Left = 5.6875!
    Me.tbUnitOfMeasure.Name = "tbUnitOfMeasure"
    Me.tbUnitOfMeasure.Style = "font-size: 10pt; text-align: center"
    Me.tbUnitOfMeasure.Text = Nothing
    Me.tbUnitOfMeasure.Top = 0.0!
    Me.tbUnitOfMeasure.Width = 1.125!
    '
    'tbQuantity
    '
    Me.tbQuantity.DataField = "Quantity"
    Me.tbQuantity.Height = 0.1875!
    Me.tbQuantity.Left = 6.875!
    Me.tbQuantity.Name = "tbQuantity"
    Me.tbQuantity.OutputFormat = resources.GetString("tbQuantity.OutputFormat")
    Me.tbQuantity.Style = "font-size: 10pt; text-align: right"
    Me.tbQuantity.SummaryGroup = "GroupHeader3"
    Me.tbQuantity.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
    Me.tbQuantity.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
    Me.tbQuantity.Text = Nothing
    Me.tbQuantity.Top = 0.0!
    Me.tbQuantity.Width = 0.875!
    '
    'tbCost
    '
    Me.tbCost.DataField = "Amount"
    Me.tbCost.Height = 0.1875!
    Me.tbCost.Left = 7.875!
    Me.tbCost.Name = "tbCost"
    Me.tbCost.OutputFormat = resources.GetString("tbCost.OutputFormat")
    Me.tbCost.Style = "font-size: 10pt; text-align: right"
    Me.tbCost.SummaryGroup = "GroupHeader3"
    Me.tbCost.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
    Me.tbCost.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
    Me.tbCost.Text = Nothing
    Me.tbCost.Top = 0.0!
    Me.tbCost.Width = 1.125!
    '
    'ReportHeader1
    '
    Me.ReportHeader1.Height = 0.0!
    Me.ReportHeader1.Name = "ReportHeader1"
    Me.ReportHeader1.Visible = False
    '
    'ReportFooter1
    '
    Me.ReportFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label1, Me.TextBox6})
    Me.ReportFooter1.Height = 0.25!
    Me.ReportFooter1.Name = "ReportFooter1"
    Me.ReportFooter1.NewPage = DataDynamics.ActiveReports.NewPage.Before
    '
    'Label1
    '
    Me.Label1.Height = 0.1875!
    Me.Label1.HyperLink = ""
    Me.Label1.Left = 5.6875!
    Me.Label1.Name = "Label1"
    Me.Label1.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: right; ddo-char-set:" & _
        " 1"
    Me.Label1.Text = "Grand Total:"
    Me.Label1.Top = 0.0!
    Me.Label1.Width = 2.125!
    '
    'TextBox6
    '
    Me.TextBox6.Border.TopColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.TextBox6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.[Double]
    Me.TextBox6.DataField = "Amount"
    Me.TextBox6.Height = 0.1875!
    Me.TextBox6.Left = 7.875!
    Me.TextBox6.Name = "TextBox6"
    Me.TextBox6.OutputFormat = resources.GetString("TextBox6.OutputFormat")
    Me.TextBox6.Style = "font-size: 10pt; text-align: right"
    Me.TextBox6.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
    Me.TextBox6.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
    Me.TextBox6.Text = Nothing
    Me.TextBox6.Top = 0.0!
    Me.TextBox6.Width = 1.125!
    '
    'lblMessage
    '
    Me.lblMessage.Height = 0.1875!
    Me.lblMessage.HyperLink = ""
    Me.lblMessage.Left = 1.0!
    Me.lblMessage.Name = "lblMessage"
    Me.lblMessage.Style = "color: Black; font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set:" & _
        " 1"
    Me.lblMessage.Text = "No Transactions Found"
    Me.lblMessage.Top = 0.0!
    Me.lblMessage.Width = 3.75!
    '
    'arGLTransactionDetail
    '
    Me.MasterReport = False
    Me.PageSettings.Margins.Bottom = 0.5!
    Me.PageSettings.Margins.Left = 0.5!
    Me.PageSettings.Margins.Right = 0.5!
    Me.PageSettings.Margins.Top = 0.5!
    Me.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Landscape
    Me.PageSettings.PaperHeight = 11.0!
    Me.PageSettings.PaperWidth = 8.5!
    Me.PrintWidth = 10.0!
    Me.Sections.Add(Me.ReportHeader1)
    Me.Sections.Add(Me.PageHeader1)
    Me.Sections.Add(Me.GroupHeader1)
    Me.Sections.Add(Me.GroupHeader2)
    Me.Sections.Add(Me.GroupHeader3)
    Me.Sections.Add(Me.Detail1)
    Me.Sections.Add(Me.GroupFooter3)
    Me.Sections.Add(Me.GroupFooter2)
    Me.Sections.Add(Me.GroupFooter1)
    Me.Sections.Add(Me.PageFooter1)
    Me.Sections.Add(Me.ReportFooter1)
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                "l; font-size: 10pt; color: Black", "Normal"))
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                "lic", "Heading2", "Normal"))
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
    CType(Me.lblEntity, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblReportName1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblReportName2, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbDeatailItemNumber, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblDateTime, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbPageNumber, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbDepartment, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TextBox12, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblTotalCost, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbItemNumber, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbItemDescription, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbChargeable, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbUnitOfMeasure, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbQuantity, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbCost, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblMessage, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

  End Sub
  Private WithEvents lblEntity As DataDynamics.ActiveReports.Label
  Private WithEvents Line1 As DataDynamics.ActiveReports.Line
  Private WithEvents lblReportName1 As DataDynamics.ActiveReports.Label
  Private WithEvents picLogo As DataDynamics.ActiveReports.Picture
  Private WithEvents lblReportName2 As DataDynamics.ActiveReports.Label
  Private WithEvents lblDateTime As DataDynamics.ActiveReports.Label
  Private WithEvents Line2 As DataDynamics.ActiveReports.Line
  Friend WithEvents tbPageNumber As DataDynamics.ActiveReports.ReportInfo
  Private WithEvents tbDeatailItemNumber As DataDynamics.ActiveReports.TextBox
  Friend WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader
  Friend WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter
  Private WithEvents TextBox1 As DataDynamics.ActiveReports.TextBox
  Private WithEvents TextBox2 As DataDynamics.ActiveReports.TextBox
  Private WithEvents TextBox3 As DataDynamics.ActiveReports.TextBox
  Private WithEvents TextBox4 As DataDynamics.ActiveReports.TextBox
  Private WithEvents TextBox5 As DataDynamics.ActiveReports.TextBox
  Friend WithEvents GroupHeader2 As DataDynamics.ActiveReports.GroupHeader
  Friend WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter
  Friend WithEvents GroupHeader3 As DataDynamics.ActiveReports.GroupHeader
  Friend WithEvents GroupFooter3 As DataDynamics.ActiveReports.GroupFooter
  Private WithEvents tbItemNumber As DataDynamics.ActiveReports.TextBox
  Private WithEvents tbItemDescription As DataDynamics.ActiveReports.TextBox
  Private WithEvents tbChargeable As DataDynamics.ActiveReports.TextBox
  Private WithEvents tbUnitOfMeasure As DataDynamics.ActiveReports.TextBox
  Private WithEvents tbQuantity As DataDynamics.ActiveReports.TextBox
  Private WithEvents tbCost As DataDynamics.ActiveReports.TextBox
  Private WithEvents TextBox12 As DataDynamics.ActiveReports.TextBox
  Private WithEvents lblTotalCost As DataDynamics.ActiveReports.Label
  Private WithEvents tbDepartment As DataDynamics.ActiveReports.TextBox
  Private WithEvents Label8 As DataDynamics.ActiveReports.Label
  Private WithEvents Label9 As DataDynamics.ActiveReports.Label
  Private WithEvents Label10 As DataDynamics.ActiveReports.Label
  Private WithEvents Label11 As DataDynamics.ActiveReports.Label
  Private WithEvents Label12 As DataDynamics.ActiveReports.Label
  Private WithEvents Label13 As DataDynamics.ActiveReports.Label
  Friend WithEvents ReportHeader1 As DataDynamics.ActiveReports.ReportHeader
  Friend WithEvents ReportFooter1 As DataDynamics.ActiveReports.ReportFooter
  Private WithEvents Label1 As DataDynamics.ActiveReports.Label
  Private WithEvents TextBox6 As DataDynamics.ActiveReports.TextBox
  Private WithEvents lblMessage As DataDynamics.ActiveReports.Label
End Class
