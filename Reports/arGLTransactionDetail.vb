#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class arGLTransactionDetail 
#Region "Declarations"
  Private moRdr As OleDbDataReader
  Private msReportName1, msCreditAccountNumber, msCreditSubAccountNumber, msReportEmailSelected, msDepartmentSelected As String
  Private msAccountDescription As String
  Private miMonthSelected, miYearSelected, miGLEntityID As Int32
  Private mbCreateBlankReport As Boolean
  Private moGLTransactions As New GLTransactions
#End Region

#Region "Routines"
  Public Function Component(ByVal ReportName1 As String, ByVal GLEntityID As Int32, ByVal YearSelected As Int32, ByVal MonthSelected As Int32, _
  ByVal ReportEmailSelected As String, ByVal DepartmentSelected As String, Optional ByVal CreateBlankReport As Boolean = False) As Boolean
    Dim oGLEntity As New GLEntity
    Dim iWork As Int32

    msReportName1 = ReportName1
    miMonthSelected = MonthSelected
    miYearSelected = YearSelected
    miGLEntityID = GLEntityID
    mbCreateBlankReport = CreateBlankReport
    msCreditAccountNumber = oGLEntity.CreditAccountNumber(miGLEntityID)
    msCreditSubAccountNumber = oGLEntity.CreditSubAccountNumber(miGLEntityID)
    msReportEmailSelected = ReportEmailSelected
    msDepartmentSelected = DepartmentSelected
    lblEntity.Text = oGLEntity.Description(miGLEntityID)
    iWork = InStr(1, msDepartmentSelected, ":")
    If iWork > 1 Then
      If iWork < Len(msDepartmentSelected) Then
        lblReportName1.Text = Trim$(Mid$(msDepartmentSelected, iWork + 1, Len(msDepartmentSelected)))
      Else
        lblReportName1.Text = vbNullString
      End If
      If Len(lblReportName1.Text) > 0 Then lblReportName1.Text = lblReportName1.Text & " "
      lblReportName1.Text = lblReportName1.Text & "(" & Trim$(Mid$(msDepartmentSelected, 1, iWork - 1)) & ")"
    Else
      lblReportName1.Text = msDepartmentSelected
    End If
    lblReportName2.Text = msReportName1 & " " & MonthName(miMonthSelected) & " " & miYearSelected
    Return True
  End Function
#End Region

#Region "Data"
  Private Sub arGLTransactionDetail_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
    Dim bFound As Boolean = False

    Try
      moRdr = moGLTransactions.DetailByMonth(miGLEntityID, miYearSelected, miMonthSelected, msCreditAccountNumber, _
      msCreditSubAccountNumber, msReportEmailSelected, msDepartmentSelected)
      If moRdr.HasRows Then
        Me.DataSource = moRdr
        lblMessage.Visible = False
      Else
        If mbCreateBlankReport Then
          lblMessage.Visible = True
          Me.DataSource = moRdr
        Else
          OKMessage("Print " & msReportName1, "Error: No data found." & vbCrLf & "Processing Canceled!", 3)
          Me.Cancel()
        End If
      End If
      'lblCriteria.Text = "Selection Criteria: Report Email=" & msReportEmailSelected & "     Department=" & msDepartmentSelected
      lblDateTime.Text = "Created on " & Format$(Now, "MM/dd/yyyy") & " at " & Format$(Now, "hh:mm:ss tt")
      msAccountDescription = vbNullString
    Catch oException As Exception
      If mbCreateBlankReport Then
        lblMessage.Visible = True
        Me.DataSource = Nothing
      Else
        OKMessage("Print " & msReportName1, "Error: No data found." & vbCrLf & "Processing Canceled!", 3)
        Me.Cancel()
      End If
    Finally
    End Try
  End Sub

  Private Sub arGLTransactionDetail_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportEnd
    CloseRDR(moRdr)
  End Sub
#End Region

#Region "Report Sections"
  Private Sub ReportHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReportHeader1.Format
    Exit Sub
  End Sub

  Private Sub arGLTransactionDetail_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
    Exit Sub
  End Sub

  Private Sub PageHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageHeader1.Format
    Exit Sub
  End Sub

  Private Sub GroupHeader4_Format(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Exit Sub
  End Sub

  Private Sub GroupHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupHeader1.Format
    Dim sDepartmentName As String

    If msAccountDescription = vbNullString Then
      msAccountDescription = SetString(Me.Fields("FullAccountNumber").Value)
      Try
        sDepartmentName = SetString(Me.Fields("DepartmentName").Value)
      Catch exExec As Exception
        sDepartmentName = vbNullString
      Finally
      End Try
      If msAccountDescription <> vbNullString AndAlso sDepartmentName <> vbNullString Then msAccountDescription = msAccountDescription & ": "
      msAccountDescription = msAccountDescription & sDepartmentName
    End If
    tbDepartment.Value = msAccountDescription
  End Sub

  Private Sub GroupHeader2_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupHeader2.Format
    Exit Sub
  End Sub

  Private Sub GroupHeader3_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupHeader3.Format
    Exit Sub
  End Sub

  Private Sub Detail1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail1.Format
    Dim sWork As String

    sWork = SetString(Me.Fields("FullAccountNumber").Value)
    Me.PageHeader1.AddBookmark("Accounts\" & Mid$(sWork, 1, 5) & "...\" & sWork)
  End Sub

  Private Sub GroupFooter3_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupFooter3.Format
    If SetString(tbItemNumber.Text) = vbNullString AndAlso SetString(tbItemDescription.Text) = vbNullString _
    AndAlso SetString(tbUnitOfMeasure.Text) = vbNullString Then
      tbChargeable.Value = vbNullString
      tbQuantity.Value = vbNullString
      tbCost.Value = vbNullString
    Else
      If SetBoolean(Me.Fields("Chargeable").Value) Then
        tbChargeable.Value = "Yes"
      Else
        tbChargeable.Value = "No"
      End If
    End If
  End Sub

  Private Sub GroupFooter2_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupFooter2.Format
    Exit Sub
  End Sub

  Private Sub GroupFooter1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupFooter1.Format
    msAccountDescription = vbNullString
  End Sub

  Private Sub GroupFooter4_Format(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Exit Sub
  End Sub

  Private Sub PageFooter1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageFooter1.Format
    Exit Sub
  End Sub

  Private Sub ReportFooter1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReportFooter1.Format
    Me.ReportFooter1.AddBookmark("Accounts\Grand Total")
    tbPageNumber.Visible = False
  End Sub
#End Region
End Class
