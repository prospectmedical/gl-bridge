<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class arDepartmentListByName 
  Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub
    
    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(arDepartmentListByName))
    Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
    Me.picLogo = New DataDynamics.ActiveReports.Picture
    Me.Line1 = New DataDynamics.ActiveReports.Line
    Me.lblReportName1 = New DataDynamics.ActiveReports.Label
    Me.Label3 = New DataDynamics.ActiveReports.Label
    Me.Label5 = New DataDynamics.ActiveReports.Label
    Me.Label1 = New DataDynamics.ActiveReports.Label
    Me.Label2 = New DataDynamics.ActiveReports.Label
    Me.lblEntity = New DataDynamics.ActiveReports.Label
    Me.Detail1 = New DataDynamics.ActiveReports.Detail
    Me.tbDepartmentNumber = New DataDynamics.ActiveReports.TextBox
    Me.tbReportEmailAddress = New DataDynamics.ActiveReports.TextBox
    Me.tbDepartmentName = New DataDynamics.ActiveReports.TextBox
    Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
    Me.lblDateTime = New DataDynamics.ActiveReports.Label
    Me.Line2 = New DataDynamics.ActiveReports.Line
    Me.ReportInfo1 = New DataDynamics.ActiveReports.ReportInfo
    CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblReportName1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblEntity, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbDepartmentNumber, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbReportEmailAddress, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.tbDepartmentName, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.lblDateTime, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.ReportInfo1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
    '
    'PageHeader1
    '
    Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.picLogo, Me.Line1, Me.lblReportName1, Me.Label3, Me.Label5, Me.Label1, Me.Label2, Me.lblEntity})
    Me.PageHeader1.Height = 1.010417!
    Me.PageHeader1.Name = "PageHeader1"
    '
    'picLogo
    '
    Me.picLogo.Height = 0.4375!
    Me.picLogo.ImageData = CType(resources.GetObject("picLogo.ImageData"), System.IO.Stream)
    Me.picLogo.Left = 0.0!
    Me.picLogo.LineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.picLogo.Name = "picLogo"
    Me.picLogo.SizeMode = DataDynamics.ActiveReports.SizeModes.Zoom
    Me.picLogo.Top = 0.0!
    Me.picLogo.Width = 1.552083!
    '
    'Line1
    '
    Me.Line1.Height = 0.0!
    Me.Line1.Left = 0.0!
    Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Line1.LineWeight = 1.0!
    Me.Line1.Name = "Line1"
    Me.Line1.Top = 0.5625!
    Me.Line1.Width = 7.5!
    Me.Line1.X1 = 0.0!
    Me.Line1.X2 = 7.5!
    Me.Line1.Y1 = 0.5625!
    Me.Line1.Y2 = 0.5625!
    '
    'lblReportName1
    '
    Me.lblReportName1.Height = 0.25!
    Me.lblReportName1.HyperLink = ""
    Me.lblReportName1.Left = 1.625!
    Me.lblReportName1.Name = "lblReportName1"
    Me.lblReportName1.Style = "color: Teal; font-size: 16pt; font-weight: bold; text-align: center; ddo-char-set" & _
        ": 1"
    Me.lblReportName1.Text = "lblReportName1"
    Me.lblReportName1.Top = 0.25!
    Me.lblReportName1.Width = 4.25!
    '
    'Label3
    '
    Me.Label3.Height = 0.19!
    Me.Label3.HyperLink = ""
    Me.Label3.Left = 4.0625!
    Me.Label3.Name = "Label3"
    Me.Label3.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: center; ddo-char-set" & _
        ": 1"
    Me.Label3.Text = "Department"
    Me.Label3.Top = 0.625!
    Me.Label3.Width = 0.875!
    '
    'Label5
    '
    Me.Label5.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label5.Height = 0.1875!
    Me.Label5.HyperLink = ""
    Me.Label5.Left = 0.0!
    Me.Label5.Name = "Label5"
    Me.Label5.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: " & _
        "1"
    Me.Label5.Text = "Department Name"
    Me.Label5.Top = 0.8125!
    Me.Label5.Width = 4.0!
    '
    'Label1
    '
    Me.Label1.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label1.Height = 0.1875!
    Me.Label1.HyperLink = ""
    Me.Label1.Left = 5.0!
    Me.Label1.Name = "Label1"
    Me.Label1.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: left; ddo-char-set: " & _
        "1"
    Me.Label1.Text = "Report Email Address"
    Me.Label1.Top = 0.8125!
    Me.Label1.Width = 2.25!
    '
    'Label2
    '
    Me.Label2.Border.BottomColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid
    Me.Label2.Height = 0.19!
    Me.Label2.HyperLink = ""
    Me.Label2.Left = 4.0625!
    Me.Label2.Name = "Label2"
    Me.Label2.Style = "color: Teal; font-size: 10pt; font-weight: bold; text-align: center; ddo-char-set" & _
        ": 1"
    Me.Label2.Text = "Number"
    Me.Label2.Top = 0.8125!
    Me.Label2.Width = 0.875!
    '
    'lblEntity
    '
    Me.lblEntity.Height = 0.25!
    Me.lblEntity.HyperLink = ""
    Me.lblEntity.Left = 1.625!
    Me.lblEntity.Name = "lblEntity"
    Me.lblEntity.Style = "color: Teal; font-size: 16pt; font-weight: bold; text-align: center; ddo-char-set" & _
        ": 1"
    Me.lblEntity.Text = "lblEntity"
    Me.lblEntity.Top = 0.0!
    Me.lblEntity.Width = 4.25!
    '
    'Detail1
    '
    Me.Detail1.ColumnSpacing = 0.0!
    Me.Detail1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.tbDepartmentNumber, Me.tbReportEmailAddress, Me.tbDepartmentName})
    Me.Detail1.Height = 0.1979167!
    Me.Detail1.Name = "Detail1"
    '
    'tbDepartmentNumber
    '
    Me.tbDepartmentNumber.DataField = "DepartmentNumber"
    Me.tbDepartmentNumber.Height = 0.1875!
    Me.tbDepartmentNumber.Left = 4.0625!
    Me.tbDepartmentNumber.Name = "tbDepartmentNumber"
    Me.tbDepartmentNumber.Style = "font-size: 10pt; text-align: center"
    Me.tbDepartmentNumber.Text = Nothing
    Me.tbDepartmentNumber.Top = 0.0!
    Me.tbDepartmentNumber.Width = 0.875!
    '
    'tbReportEmailAddress
    '
    Me.tbReportEmailAddress.DataField = "ReportEmailAddress"
    Me.tbReportEmailAddress.Height = 0.1875!
    Me.tbReportEmailAddress.Left = 5.0!
    Me.tbReportEmailAddress.Name = "tbReportEmailAddress"
    Me.tbReportEmailAddress.Style = "font-size: 10pt"
    Me.tbReportEmailAddress.Text = Nothing
    Me.tbReportEmailAddress.Top = 0.0!
    Me.tbReportEmailAddress.Width = 2.25!
    '
    'tbDepartmentName
    '
    Me.tbDepartmentName.DataField = "DepartmentName"
    Me.tbDepartmentName.Height = 0.1875!
    Me.tbDepartmentName.Left = 0.0!
    Me.tbDepartmentName.Name = "tbDepartmentName"
    Me.tbDepartmentName.Style = "font-size: 10pt"
    Me.tbDepartmentName.Text = Nothing
    Me.tbDepartmentName.Top = 0.0!
    Me.tbDepartmentName.Width = 4.0!
    '
    'PageFooter1
    '
    Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblDateTime, Me.Line2, Me.ReportInfo1})
    Me.PageFooter1.Height = 0.2708333!
    Me.PageFooter1.Name = "PageFooter1"
    '
    'lblDateTime
    '
    Me.lblDateTime.Height = 0.1875!
    Me.lblDateTime.HyperLink = ""
    Me.lblDateTime.Left = 0.0!
    Me.lblDateTime.Name = "lblDateTime"
    Me.lblDateTime.Style = "color: Teal; font-size: 8pt; font-weight: normal; text-align: left; ddo-char-set:" & _
        " 1"
    Me.lblDateTime.Text = "C"
    Me.lblDateTime.Top = 0.0625!
    Me.lblDateTime.Width = 3.75!
    '
    'Line2
    '
    Me.Line2.Height = 0.0!
    Me.Line2.Left = 0.0!
    Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
    Me.Line2.LineWeight = 1.0!
    Me.Line2.Name = "Line2"
    Me.Line2.Top = 0.0!
    Me.Line2.Width = 7.5!
    Me.Line2.X1 = 0.0!
    Me.Line2.X2 = 7.5!
    Me.Line2.Y1 = 0.0!
    Me.Line2.Y2 = 0.0!
    '
    'ReportInfo1
    '
    Me.ReportInfo1.FormatString = "Page {PageNumber} of {PageCount}"
    Me.ReportInfo1.Height = 0.1875!
    Me.ReportInfo1.Left = 4.625!
    Me.ReportInfo1.Name = "ReportInfo1"
    Me.ReportInfo1.Style = "color: Teal; font-weight: bold; text-align: right"
    Me.ReportInfo1.Top = 0.0625!
    Me.ReportInfo1.Width = 2.8125!
    '
    'arDepartmentListByName
    '
    Me.MasterReport = False
    Me.PageSettings.Margins.Bottom = 0.5!
    Me.PageSettings.Margins.Left = 0.5!
    Me.PageSettings.Margins.Right = 0.5!
    Me.PageSettings.Margins.Top = 0.5!
    Me.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait
    Me.PageSettings.PaperHeight = 11.0!
    Me.PageSettings.PaperWidth = 8.5!
    Me.PrintWidth = 7.5!
    Me.Sections.Add(Me.PageHeader1)
    Me.Sections.Add(Me.Detail1)
    Me.Sections.Add(Me.PageFooter1)
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                "l; font-size: 10pt; color: Black", "Normal"))
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                "lic", "Heading2", "Normal"))
    Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
    CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblReportName1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblEntity, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbDepartmentNumber, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbReportEmailAddress, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.tbDepartmentName, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.lblDateTime, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.ReportInfo1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

  End Sub
  Private WithEvents picLogo As DataDynamics.ActiveReports.Picture
  Private WithEvents Line1 As DataDynamics.ActiveReports.Line
  Private WithEvents lblReportName1 As DataDynamics.ActiveReports.Label
  Private WithEvents Label3 As DataDynamics.ActiveReports.Label
  Private WithEvents Label5 As DataDynamics.ActiveReports.Label
  Private WithEvents Label1 As DataDynamics.ActiveReports.Label
  Private WithEvents Label2 As DataDynamics.ActiveReports.Label
  Private WithEvents tbDepartmentNumber As DataDynamics.ActiveReports.TextBox
  Private WithEvents tbReportEmailAddress As DataDynamics.ActiveReports.TextBox
  Private WithEvents tbDepartmentName As DataDynamics.ActiveReports.TextBox
  Private WithEvents lblDateTime As DataDynamics.ActiveReports.Label
  Private WithEvents Line2 As DataDynamics.ActiveReports.Line
  Friend WithEvents ReportInfo1 As DataDynamics.ActiveReports.ReportInfo
  Private WithEvents lblEntity As DataDynamics.ActiveReports.Label
End Class 
