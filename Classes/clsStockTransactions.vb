﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class StockTransactions
#Region "Declarations"
  Private msAccountNumber, msSubAccountNumber, msItemNumber, msItemDescription, msUnitOfMeasure As String
  Private miGLEntityID, miTransactionYear, miTransactionMonth As Int32
  Private mbChargeable As Boolean
  Private mdQuantity, mdAmount As Double
#End Region

#Region "Class Properties"
  Public WriteOnly Property GLEntityID() As Int32
    Set(ByVal value As Int32)
      miGLEntityID = value
    End Set
  End Property

  Public WriteOnly Property TransactionYear() As Int32
    Set(ByVal value As Int32)
      miTransactionYear = value
    End Set
  End Property

  Public WriteOnly Property TransactionMonth() As Int32
    Set(ByVal value As Int32)
      miTransactionMonth = value
    End Set
  End Property

  Public WriteOnly Property AccountNumber() As String
    Set(ByVal value As String)
      msAccountNumber = value
    End Set
  End Property

  Public WriteOnly Property SubAccountNumber() As String
    Set(ByVal value As String)
      msSubAccountNumber = value
    End Set
  End Property

  Public WriteOnly Property ItemNumber() As String
    Set(ByVal value As String)
      msItemNumber = value
    End Set
  End Property

  Public WriteOnly Property ItemDescription() As String
    Set(ByVal value As String)
      msItemDescription = value
    End Set
  End Property

  Public WriteOnly Property Chargeable() As Boolean
    Set(ByVal value As Boolean)
      mbChargeable = value
    End Set
  End Property

  Public WriteOnly Property UnitOfMeasure() As String
    Set(ByVal value As String)
      msUnitOfMeasure = value
    End Set
  End Property

  Public WriteOnly Property Quantity() As Double
    Set(ByVal value As Double)
      mdQuantity = value
    End Set
  End Property

  Public WriteOnly Property Amount() As Double
    Set(ByVal value As Double)
      mdAmount = value
    End Set
  End Property
#End Region

#Region "Routines"
  Public Sub New()
    Initialize()
  End Sub

  Private Sub Initialize()
    Exit Sub
  End Sub
#End Region

#Region "DeleteMonthTransactions"
  Public ReadOnly Property DeleteMonthlyStockTransactions(ByVal TransactionYear As Int32, ByVal TransactionMonth As Int32) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand

      DeleteMonthlyStockTransactions = False
      If OpenDBMessage(oDBC) <> 0 Then Return False
      Try
        oCmd.CommandText = "dStockTransactionsByYearMonth"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.CommandTimeout = giCommandTimeoutLong
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = TransactionYear
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = TransactionMonth
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@AccountID", OleDbType.Numeric).Value = giAccountID
        oCmd.Connection = oDBC
        oCmd.ExecuteScalar()
        DeleteMonthlyStockTransactions = True
      Catch oException As Exception
      Finally
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
    End Get
  End Property
#End Region

#Region "InsertRecord"
  Public ReadOnly Property InsertRecord() As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim bInserted As Boolean = False

      If OpenDBMessage(oDBC) <> 0 Then Return bInserted
      Try
        oCmd.CommandText = "aStockTransaction"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = miGLEntityID
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = miTransactionYear
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = miTransactionMonth
        oCmd.Parameters.Add("@AccountNumber", OleDbType.VarChar, 10).Value = NullDBField(msAccountNumber)
        oCmd.Parameters.Add("@SubAccountNumber", OleDbType.VarChar, 5).Value = NullDBField(msSubAccountNumber)
        oCmd.Parameters.Add("@ItemNumber", OleDbType.VarChar, 10).Value = NullDBField(msItemNumber)
        oCmd.Parameters.Add("@ItemDescription", OleDbType.VarChar, 255).Value = NullDBField(msItemDescription)
        oCmd.Parameters.Add("@Chargeable", OleDbType.Boolean).Value = mbChargeable
        oCmd.Parameters.Add("@UnitOfMeasure", OleDbType.VarChar, 5).Value = NullDBField(msUnitOfMeasure)
        oCmd.Parameters.Add("@Quantity", OleDbType.Double).Value = mdQuantity
        oCmd.Parameters.Add("@Amount", OleDbType.Double).Value = mdAmount
        oCmd.Parameters.Add("@GUID", OleDbType.VarChar, 36).Value = System.Guid.NewGuid().ToString
        oCmd.Connection = oDBC
        oCmd.ExecuteScalar()
        bInserted = True
      Catch exExc As Exception
      Finally
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return bInserted
    End Get
  End Property
#End Region

#Region "StartImport"
  Public ReadOnly Property StartImport(ByVal TransactionYear As Int32, ByVal TransactionMonth As Int32) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim bDelete As Boolean

      bDelete = False
      If OpenDBMessage(oDBC) <> 0 Then Return bDelete
      Try
        oCmd.CommandText = "aStartImport"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = TransactionYear
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = TransactionMonth
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@AccountID", OleDbType.Numeric).Value = giAccountID
        oCmd.Connection = oDBC
        oCmd.ExecuteScalar()
        bDelete = True
      Catch exExc As Exception
      Finally
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return bDelete
    End Get
  End Property
#End Region

#Region "EndImport"
  Public ReadOnly Property EndImport(ByVal TransactionYear As Int32, ByVal TransactionMonth As Int32) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim bDelete As Boolean

      bDelete = False
      If OpenDBMessage(oDBC) <> 0 Then Return bDelete
      Try
        oCmd.CommandText = "aEndImport"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = TransactionYear
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = TransactionMonth
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@AccountID", OleDbType.Numeric).Value = giAccountID
        oCmd.Connection = oDBC
        oCmd.ExecuteScalar()
        bDelete = True
      Catch exExc As Exception
      Finally
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return bDelete
    End Get
  End Property
#End Region

#Region "WriteSkippedRecord"
  Public ReadOnly Property WriteSkippedRecord(ByVal InterfaceHistoryHeaderID As Int32, ByVal Record As String, ByVal RecordCount As Int32) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim bInserted As Boolean = False

      If Trim$(Record) = vbNullString Then Return True
      If OpenDBMessage(oDBC) <> 0 Then Return bInserted
      Try
        oCmd.CommandText = "aSkippedRecords"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@InterfaceHistoryHeaderID", OleDbType.Numeric).Value = InterfaceHistoryHeaderID
        oCmd.Parameters.Add("@Record", OleDbType.VarChar, 250).Value = Record
        oCmd.Parameters.Add("@RecordCount", OleDbType.Numeric).Value = RecordCount
        oCmd.Parameters.Add("@GUID", OleDbType.VarChar, 36).Value = System.Guid.NewGuid().ToString
        oCmd.Connection = oDBC
        oCmd.ExecuteScalar()
        bInserted = True
      Catch exExc As Exception
      Finally
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return bInserted
    End Get
  End Property
#End Region

#Region "SkippedRecordsCount"
  Public ReadOnly Property SkippedRecordsCount(ByVal InterfaceHistoryHeaderID As Int32) As Int32
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim iRecordCount As Int32

      If OpenDBMessage(oDBC) <> 0 Then Return 0
      oCmd.CommandText = "sSkippedRecordCount"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryHeaderID", OleDbType.VarChar, 3).Value = InterfaceHistoryHeaderID
      Try
        oCmd.Connection = oDBC
        iRecordCount = SetInt32(oCmd.ExecuteScalar)
      Catch exExc As Exception
        iRecordCount = 0
      Finally
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return iRecordCount
    End Get
  End Property
#End Region
End Class
