﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class GLTransactions
#Region "Declarations"
  Private miDepartmentID, miGLEntityID, miRowCount As Int32
  Private msDepartmentNumber, msDepartmentName, msReportEmailAddress, msGUID As String
  Private mbDuplicateDepartmentNumber As Boolean
#End Region

#Region "Class Properties"
  Public ReadOnly Property RowCount() As Int32
    Get
      Return miRowCount
    End Get
  End Property
#End Region

#Region "Routines"
  'Private Function EmailFileName(ByVal EmailAddress As String, ByVal SiemensHospitalCode As String) As String
  'Dim iWork As Int32

  'EmailFileName = EmailAddress
  'iWork = InStr(1, EmailFileName, "@")
  'If iWork > 0 Then
  '  If iWork = 1 Then
  '    EmailFileName = vbNullString
  '  Else
  '    EmailFileName = Mid$(EmailFileName, 1, iWork - 1)
  '  End If
  'End If
  'EmailFileName = Replace(EmailFileName, ".", vbNullString)
  'EmailFileName = gsReportEmailWorkPath & "GLB_" & SiemensHospitalCode & "_" & EmailFileName & ".pdf"
  'End Function
#End Region

#Region "SummaryByMonth"
  Public ReadOnly Property SummaryByMonth(ByVal GLEntityID As Int32, ByVal YearSelected As Int32, ByVal MonthSelected As Int32, _
  ByVal CreditAccountNumber As String, ByVal CreditSubAccountNumber As String) As OleDbDataReader
    Get
      Dim oDBC As OleDbConnection = New OleDb.OleDbConnection
      Dim oCmd As OleDbCommand = New OleDb.OleDbCommand

      If OpenDBMessage(oDBC) <> 0 Then
        SummaryByMonth = Nothing
        Exit Property
      End If
      oCmd.CommandText = "sGLTransactions"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.CommandTimeout = giCommandTimeoutLong
      oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
      oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
      oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = YearSelected
      oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = MonthSelected
      oCmd.Parameters.Add("@CreditAccountNumber", OleDbType.VarChar, 10).Value = CreditAccountNumber
      oCmd.Parameters.Add("@CreditSubAccountNumber", OleDbType.VarChar, 5).Value = CreditSubAccountNumber
      Try
        oCmd.Connection = oDBC
        SummaryByMonth = oCmd.ExecuteReader()
      Catch exExc As Exception
        SummaryByMonth = Nothing
      Finally
      End Try
    End Get
  End Property
#End Region

#Region "DetailByMonth"
  Public ReadOnly Property DetailByMonth(ByVal GLEntityID As Int32, ByVal YearSelected As Int32, ByVal MonthSelected As Int32, _
  ByVal CreditAccountNumber As String, ByVal CreditSubAccountNumber As String, ByVal ReportEmailSelected As String, _
  ByVal DepartmentSelected As String) As OleDbDataReader
    Get
      Dim oDBC As OleDbConnection = New OleDb.OleDbConnection
      Dim oCmd As OleDbCommand = New OleDb.OleDbCommand
      Dim sReportEmailAddress, sDepartmentNumber As String
      Dim iWork As Int32

      If OpenDBMessage(oDBC) <> 0 Then
        DetailByMonth = Nothing
        Exit Property
      End If
      sReportEmailAddress = LCase$(Trim$(ReportEmailSelected))
      sDepartmentNumber = LCase$(Trim$(DepartmentSelected))
      iWork = InStr(1, sDepartmentNumber, ":")
      If iWork = 7 Then
        sDepartmentNumber = Trim$(Mid$(sDepartmentNumber, 1, 6))
      Else
        sDepartmentNumber = "all "
      End If
      If Mid$(sReportEmailAddress, 1, 4) <> "all " AndAlso Mid$(sDepartmentNumber, 1, 4) <> "all " Then
        oCmd.CommandText = "sGLTransactionDetailByEmailDepartment"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = YearSelected
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = MonthSelected
        oCmd.Parameters.Add("@CreditAccountNumber", OleDbType.VarChar, 10).Value = CreditAccountNumber
        oCmd.Parameters.Add("@CreditSubAccountNumber", OleDbType.VarChar, 5).Value = CreditSubAccountNumber
        oCmd.Parameters.Add("@ReportEmailAddress", OleDbType.VarChar, 101).Value = sReportEmailAddress
        oCmd.Parameters.Add("@DepartmentNumber", OleDbType.VarChar, 7).Value = sDepartmentNumber
      ElseIf Mid$(sReportEmailAddress, 1, 4) <> "all " AndAlso Mid$(sDepartmentNumber, 1, 4) = "all " Then
        oCmd.CommandText = "sGLTransactionDetailByReportEmailAddress"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = YearSelected
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = MonthSelected
        oCmd.Parameters.Add("@CreditAccountNumber", OleDbType.VarChar, 10).Value = CreditAccountNumber
        oCmd.Parameters.Add("@CreditSubAccountNumber", OleDbType.VarChar, 5).Value = CreditSubAccountNumber
        oCmd.Parameters.Add("@ReportEmailAddress", OleDbType.VarChar, 101).Value = sReportEmailAddress
      ElseIf Mid$(sReportEmailAddress, 1, 4) = "all " AndAlso Mid$(sDepartmentNumber, 1, 4) <> "all " Then
        oCmd.CommandText = "sGLTransactionDetailByDepartment"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = YearSelected
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = MonthSelected
        oCmd.Parameters.Add("@CreditAccountNumber", OleDbType.VarChar, 10).Value = CreditAccountNumber
        oCmd.Parameters.Add("@CreditSubAccountNumber", OleDbType.VarChar, 5).Value = CreditSubAccountNumber
        oCmd.Parameters.Add("@DepartmentNumber", OleDbType.VarChar, 7).Value = sDepartmentNumber
      Else
        oCmd.CommandText = "sGLTransactionDetailAll"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = YearSelected
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = MonthSelected
        oCmd.Parameters.Add("@CreditAccountNumber", OleDbType.VarChar, 10).Value = CreditAccountNumber
        oCmd.Parameters.Add("@CreditSubAccountNumber", OleDbType.VarChar, 5).Value = CreditSubAccountNumber
      End If
      Try
        oCmd.CommandTimeout = giCommandTimeoutLong
        oCmd.Connection = oDBC
        DetailByMonth = oCmd.ExecuteReader()
      Catch exExc As Exception
        DetailByMonth = Nothing
      Finally
      End Try
    End Get
  End Property
#End Region

#Region "GrandTotal"
  Public ReadOnly Property GrandTotal(ByVal GLEntityID As Int32, ByVal YearSelected As Int32, ByVal MonthSelected As Int32, _
  ByVal CreditAccountNumber As String, ByVal CreditSubAccountNumber As String, ByVal ReportEmailSelected As String, _
  ByVal DepartmentSelected As String) As Double
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim sReportEmailAddress, sDepartmentNumber As String
      Dim iWork As Int32
      Dim dGrandTotal As Double = 0

      If OpenDBMessage(oDBC) <> 0 Then Return 0
      sReportEmailAddress = LCase$(Trim$(ReportEmailSelected))
      sDepartmentNumber = LCase$(Trim$(DepartmentSelected))
      iWork = InStr(1, sDepartmentNumber, ":")
      If iWork = 7 Then
        sDepartmentNumber = Trim$(Mid$(sDepartmentNumber, 1, 6))
      Else
        sDepartmentNumber = "all "
      End If
      If Mid$(sReportEmailAddress, 1, 4) <> "all " AndAlso Mid$(sDepartmentNumber, 1, 4) <> "all " Then
        oCmd.CommandText = "sGLTransactionDetailByEmailDepartmentGT"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = YearSelected
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = MonthSelected
        oCmd.Parameters.Add("@CreditAccountNumber", OleDbType.VarChar, 10).Value = CreditAccountNumber
        oCmd.Parameters.Add("@CreditSubAccountNumber", OleDbType.VarChar, 5).Value = CreditSubAccountNumber
        oCmd.Parameters.Add("@ReportEmailAddress", OleDbType.VarChar, 101).Value = sReportEmailAddress
        oCmd.Parameters.Add("@DepartmentNumber", OleDbType.VarChar, 7).Value = sDepartmentNumber
      ElseIf Mid$(sReportEmailAddress, 1, 4) <> "all " AndAlso Mid$(sDepartmentNumber, 1, 4) = "all " Then
        oCmd.CommandText = "sGLTransactionDetailByReportEmailAddressGT"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = YearSelected
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = MonthSelected
        oCmd.Parameters.Add("@CreditAccountNumber", OleDbType.VarChar, 10).Value = CreditAccountNumber
        oCmd.Parameters.Add("@CreditSubAccountNumber", OleDbType.VarChar, 5).Value = CreditSubAccountNumber
        oCmd.Parameters.Add("@ReportEmailAddress", OleDbType.VarChar, 101).Value = sReportEmailAddress
      ElseIf Mid$(sReportEmailAddress, 1, 4) = "all " AndAlso Mid$(sDepartmentNumber, 1, 4) <> "all " Then
        oCmd.CommandText = "sGLTransactionDetailByDepartmentGT"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = YearSelected
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = MonthSelected
        oCmd.Parameters.Add("@CreditAccountNumber", OleDbType.VarChar, 10).Value = CreditAccountNumber
        oCmd.Parameters.Add("@CreditSubAccountNumber", OleDbType.VarChar, 5).Value = CreditSubAccountNumber
        oCmd.Parameters.Add("@DepartmentNumber", OleDbType.VarChar, 7).Value = sDepartmentNumber
      Else
        oCmd.CommandText = "sGLTransactionDetailAllGT"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
        oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = YearSelected
        oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = MonthSelected
        oCmd.Parameters.Add("@CreditAccountNumber", OleDbType.VarChar, 10).Value = CreditAccountNumber
        oCmd.Parameters.Add("@CreditSubAccountNumber", OleDbType.VarChar, 5).Value = CreditSubAccountNumber
      End If
      Try
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader(CommandBehavior.SingleRow)
        If oRdr.HasRows Then
          oRdr.Read()
          dGrandTotal = SetDouble(oRdr!Amount)
        End If
      Catch exExc As Exception
        dGrandTotal = 0
      Finally
        CloseRDR(oRdr)
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return dGrandTotal
    End Get
  End Property
#End Region

#Region "DetailEmailTrackingComplete"
  Public ReadOnly Property DetailEmailTrackingComplete(ByVal GLEntityID As Int32, ByVal Year As Int32, ByVal Month As Int32) As Int32
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim iDetailEmailTrackingID As Int32

      If OpenDBMessage(oDBC) <> 0 Then Return -1
      Try
        oCmd.CommandText = "sDetailEmailTrackingComplete"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
        oCmd.Parameters.Add("@Year", OleDbType.Numeric).Value = Year
        oCmd.Parameters.Add("@Month", OleDbType.Numeric).Value = Month
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader()
        If oRdr.HasRows Then
          oRdr.Read()
          iDetailEmailTrackingID = SetInt32(oRdr!DetailEmailTrackingID)
        Else
          iDetailEmailTrackingID = 0
        End If
      Catch exExc As Exception
        iDetailEmailTrackingID = -1
      Finally
        CloseCMD(oCmd)
        CloseRDR(oRdr)
        CloseDB(oDBC)
      End Try
      Return iDetailEmailTrackingID
    End Get
  End Property
#End Region

#Region "DetailEmailTrackingInsert"
  Public ReadOnly Property DetailEmailTrackingInsert(ByVal GLEntityID As Int32, ByVal Year As Int32, ByVal Month As Int32) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim bSuccess As Boolean = False
      Dim iDetailEmailTrackingID As Int32

      If OpenDBMessage(oDBC) <> 0 Then Return bSuccess
      Try
        oCmd.CommandText = "aDetailEmailTracking"
        oCmd.CommandType = CommandType.StoredProcedure
        oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
        oCmd.Parameters.Add("@Year", OleDbType.Numeric).Value = Year
        oCmd.Parameters.Add("@Month", OleDbType.Numeric).Value = Month
        oCmd.Parameters.Add("@GUID", OleDbType.VarChar, 36).Value = System.Guid.NewGuid().ToString
        oCmd.Parameters.Add("@AccountID", OleDbType.Numeric).Value = giAccountID
        oCmd.Parameters.Add("@NewID", OleDbType.Numeric).Direction = ParameterDirection.Output
        oCmd.Connection = oDBC
        oCmd.ExecuteScalar()
        iDetailEmailTrackingID = SetInt32(oCmd.Parameters("@NewID").Value)
        If iDetailEmailTrackingID > 0 Then bSuccess = True
      Catch exExc As Exception
      Finally
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return bSuccess
    End Get
  End Property
#End Region

#Region "DetailEmailTrackingDelete"
  Public ReadOnly Property DetailEmailTrackingDelete(ByVal DetailEmailTrackingID As Int32) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oSCmd As OleDbCommand = New OleDbCommand
      Dim oDCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      'Dim sGUID As String
      Dim bDelete As Boolean

      bDelete = False
      'sGUID = vbNullString
      If OpenDBMessage(oDBC) <> 0 Then Return bDelete

      ' check guid
      'oSCmd.CommandText = "sDetailEmailTracking"
      'oSCmd.CommandType = CommandType.StoredProcedure
      'oSCmd.Parameters.Add("@DetailEmailTrackingID", OleDbType.Numeric).Value = DetailEmailTrackingID

      'Try
      '  oSCmd.Connection = oDBC
      '  oRdr = oSCmd.ExecuteReader
      '  oRdr.Read()
      '  sGUID = SetString(oRdr!GUID)
      'Catch exExc As Exception
      '  CloseCMD(oSCmd)
      '  CloseRDR(oRdr)
      '  CloseDB(oDBC)
      '  Return bDelete
      'Finally
      '  CloseCMD(oSCmd)
      '  CloseRDR(oRdr)
      'End Try
      'If msGUID <> sGUID Then
      '  If DataConflict("Delete Detail Email Tracking") = MsgBoxResult.No Then
      '    CloseDB(oDBC)
      '    Return bDelete
      '  End If
      'End If

      ' delete record
      Try
        msGUID = System.Guid.NewGuid().ToString
        oDCmd.CommandText = "dDetailEmailTracking"
        oDCmd.CommandType = CommandType.StoredProcedure
        oDCmd.Parameters.Add("@DetailEmailTrackingID", OleDbType.Numeric).Value = DetailEmailTrackingID
        oDCmd.Parameters.Add("@GUID", OleDbType.VarChar, 36).Value = msGUID
        oDCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oDCmd.Parameters.Add("@AccountID", OleDbType.Numeric).Value = giAccountID
        oDCmd.Connection = oDBC
        oDCmd.ExecuteScalar()
        bDelete = True
      Catch exExc As Exception
      Finally
        CloseCMD(oDCmd)
        CloseDB(oDBC)
      End Try
      Return bDelete
    End Get
  End Property
#End Region

#Region "CheckImportFile"
  Public ReadOnly Property CheckImportFile(ByVal CheckPath As String, ByVal CheckFile As String) As Boolean
    Get
      Dim oFileInfo As IO.FileInfo
      Dim sYear, sMonth As String
      Dim dtCurrentDate As Date = DateAdd(DateInterval.Month, -1, Now)
      Dim bMonthFileExists, bProcessedFileExists As Boolean

      sYear = SetString(Year(dtCurrentDate))
      sMonth = Format$(Month(dtCurrentDate), "0#")
      oFileInfo = New IO.FileInfo(gsImportPath & gsImportFileName & sYear & sMonth & "." & gsImportFileExtension)
      bMonthFileExists = oFileInfo.Exists
      oFileInfo = New IO.FileInfo(gsProcessedPath & gsImportFileName & sYear & sMonth & "." & gsImportFileExtension)
      bProcessedFileExists = oFileInfo.Exists
      oFileInfo = New IO.FileInfo(gsImportPath & gsImportFileName & "200812001." & gsImportFileExtension)
      If Not oFileInfo.Exists Then Return True
      If bMonthFileExists Or bProcessedFileExists Then Return False
      oFileInfo.MoveTo(gsImportPath & gsImportFileName & sYear & sMonth & "." & gsImportFileExtension)
      oFileInfo = Nothing
      Return True
    End Get
  End Property
#End Region

#Region "DetailEmailArray"
  Public Function DetailEmailArray(ByVal GLEntityID As Int32, ByRef DepartmentNumber As ArrayList, ByRef DepartmentName As ArrayList, ByRef ReportEmailAddress As ArrayList) As Boolean
    Dim oDBC As OleDbConnection = New OleDb.OleDbConnection
    Dim oCmd As OleDbCommand = New OleDb.OleDbCommand
    Dim oRdr As OleDbDataReader = Nothing

    DepartmentNumber = New ArrayList
    DepartmentName = New ArrayList
    ReportEmailAddress = New ArrayList
    If OpenDBMessage(oDBC) <> 0 Then Return Nothing
    oCmd.CommandText = "sGLTransactionDetailEmail"
    oCmd.CommandType = CommandType.StoredProcedure
    oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
    oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
    oCmd.CommandType = CommandType.StoredProcedure
    miRowCount = -1
    Try
      oCmd.Connection = oDBC
      oRdr = oCmd.ExecuteReader
      If oRdr.HasRows Then
        While oRdr.Read
          DepartmentNumber.Add(SetString(oRdr!DepartmentNumber))
          DepartmentName.Add(SetString(oRdr!DepartmentName))
          ReportEmailAddress.Add(SetString(oRdr!ReportEmailAddress))
        End While
        DetailEmailArray = True
      Else
        DetailEmailArray = False
      End If
    Catch exExc As Exception
      DetailEmailArray = False
    Finally
      CloseCMD(oCmd)
      CloseRDR(oRdr)
      CloseDB(oDBC)
    End Try
  End Function
#End Region
End Class