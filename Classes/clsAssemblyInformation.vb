#Region "Init"
Option Explicit On
Option Strict On
#End Region

Public Class AssemblyInformation
#Region "Declarations"
#End Region

#Region "Class Properties"
  Public ReadOnly Property FileVersion() As String
    Get
      Dim sWork As String

      sWork = gsVersion
      If Len(sWork) > 8 Then
        If UCase$(Mid$(sWork, 1, 8)) = "VERSION " Then sWork = Mid$(sWork, 9, Len(sWork))
      End If
      Return sWork
    End Get
  End Property

  Public ReadOnly Property AssemblyVersion() As String
    Get
      Dim sWork As String

      sWork = gsVersion
      If Len(sWork) > 8 Then
        If UCase$(Mid$(sWork, 1, 8)) = "VERSION " Then sWork = Mid$(sWork, 9, Len(sWork))
      End If
      Return sWork
    End Get
  End Property

  Public ReadOnly Property Title() As String
    Get
      Return gsProductName
    End Get
  End Property

  Public ReadOnly Property Product() As String
    Get
      Return gsProductName
    End Get
  End Property

  Public ReadOnly Property SettingType() As String
    Get
      Return gsSettingType
    End Get
  End Property

  Public ReadOnly Property LogPath() As String
    Get
      Return gsLogPath
    End Get
  End Property

  Public ReadOnly Property ErrorFile() As String
    Get
      Return gsErrorFile
    End Get
  End Property

  Public ReadOnly Property ApplicationPath() As String
    Get
      Return gsAppPath
    End Get
  End Property

  Public ReadOnly Property ImportPath() As String
    Get
      Return gsImportPath
    End Get
  End Property

  Public ReadOnly Property ImportFileName() As String
    Get
      Return gsImportFileName & "yyyymm." & gsImportFileExtension
    End Get
  End Property

  Public ReadOnly Property ProcessedPath() As String
    Get
      Return gsProcessedPath
    End Get
  End Property

  Public ReadOnly Property ExportPath() As String
    Get
      Return gsExportPath
    End Get
  End Property

  Public ReadOnly Property CompanyName() As String
    Get
      Try
        Return SetString(Application.CompanyName)
      Catch exExc As Exception
        Return "Unavailable"
      Finally
      End Try
    End Get
  End Property

  Public ReadOnly Property Copyright() As String
    Get
      Try
        Return SetString(My.Application.Info.Copyright)
      Catch ex As Exception
        Return "Unavailable"
      Finally
      End Try
    End Get
  End Property

  Public ReadOnly Property Trademark() As String
    Get
      Try
        Return SetString(My.Application.Info.Trademark)
      Catch ex As Exception
        Return "Unavailable"
      Finally
      End Try
    End Get
  End Property

  Public ReadOnly Property Description() As String
    Get
      Try
        Return SetString(My.Application.Info.Description)
      Catch ex As Exception
        Return "Unavailable"
      Finally
      End Try
    End Get
  End Property

  Public ReadOnly Property ActiveReportsVersion() As String
    Get
      Try
        Dim ARInstance As New DataDynamics.ActiveReports.ActiveReport
        Return ARInstance.Version
      Catch ex As Exception
        Return "Unavailable"
      End Try
    End Get
  End Property
#End Region
End Class
