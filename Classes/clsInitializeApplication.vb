#Region "Init"
Option Explicit On 
Option Strict On
Imports System.IO
Imports System.Data.OleDb
#End Region

Public Class InitializeApplication
#Region "Declarations"
  Private msExportPath, msLogPath, msDocumentPath, msImportFileSearch, msImportPath, msProcessedPath As String
  Private msExportBackupPath, msDefaultImportFileLocation, msDefaultImportFileName, msReportEmailWorkPath, msEmailSender As String
#End Region

#Region "Class Properties"
  Public ReadOnly Property ExportPath() As String
    Get
      Return msExportPath
    End Get
  End Property

  Public ReadOnly Property LogPath() As String
    Get
      Return msLogPath
    End Get
  End Property

  Public ReadOnly Property DocumentPath() As String
    Get
      Return msDocumentPath
    End Get
  End Property

  Public ReadOnly Property ImportPath() As String
    Get
      Return msImportPath
    End Get
  End Property

  Public ReadOnly Property ImportFileSearch() As String
    Get
      Return msImportFileSearch
    End Get
  End Property

  Public ReadOnly Property ProcessedPath() As String
    Get
      Return msProcessedPath
    End Get
  End Property

  Public ReadOnly Property ExportBackupPath() As String
    Get
      Return msExportBackupPath
    End Get
  End Property

  Public ReadOnly Property DefaultImportFileLocation() As String
    Get
      Return msDefaultImportFileLocation
    End Get
  End Property

  Public ReadOnly Property DefaultImportFileName() As String
    Get
      Return msDefaultImportFileName
    End Get
  End Property

  Public ReadOnly Property ReportEmailWorkPath() As String
    Get
      Return msReportEmailWorkPath
    End Get
  End Property

  Public ReadOnly Property EmailSender() As String
    Get
      Return msEmailSender
    End Get
  End Property
#End Region

#Region "Version Check"
  Public ReadOnly Property VersionCheck(ByVal Version As String) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim bWork As Boolean

      If OpenDBMessage(oDBC) <> 0 Then Return False
      oCmd.CommandText = "sVersionCheck"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@Version", OleDbType.VarChar, 25).Value = Version
      Try
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader
        oRdr.Read()
        If oRdr.HasRows Then
          bWork = True
        Else
          bWork = False
        End If
      Catch exExc As Exception
        bWork = False
      Finally
        CloseRDR(oRdr)
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return bWork
    End Get
  End Property
#End Region

#Region "New"
  Public Sub New()
    Exit Sub
  End Sub
#End Region

#Region "GetSettings"
  Public Function GetSettings(ByVal INILocation As String, ByVal INIFileName As String) As Boolean
    Dim oFileStream As New FileStream(AddBackslash(INILocation) & INIFileName, FileMode.Open, FileAccess.Read)
    Dim oStreamReader As New StreamReader(oFileStream)
    Dim sInRecord, sItemType, sItemSetting As String
    Dim iWork, iINICounter As Int32
    Dim iINICount As Int32 = 9
    Dim bValid As Boolean = False

    oStreamReader.BaseStream.Seek(0, SeekOrigin.Begin)
    While oStreamReader.Peek() > -1
      sInRecord = Trim$(oStreamReader.ReadLine())
      If Len(sInRecord) <= 0 Then GoTo NextRecord
      If Mid$(sInRecord, 1, 1) = ";" Then GoTo NextRecord
      iWork = InStr(1, sInRecord, " ")
      If iWork <= 3 Or iWork >= Len(sInRecord) Then GoTo NextRecord
      sItemType = Trim$(LCase$(Mid$(sInRecord, 1, iWork - 1)))
      sItemSetting = Trim$(Mid$(sInRecord, iWork + 1, Len(sInRecord)))
      Select Case sItemType
        Case "[exportpath]"
          msExportPath = AddBackslash(sItemSetting)
          iINICounter += 1
        Case "[logpath]"
          msLogPath = SetString(sItemSetting)
          iINICounter += 1
        Case "[importpath]"
          msImportPath = SetString(sItemSetting)
          iINICounter += 1
        Case "[processedpath]"
          msProcessedPath = SetString(sItemSetting)
          iINICounter += 1
        Case "[exportbackuppath]"
          msExportBackupPath = SetString(sItemSetting)
          iINICounter += 1
        Case "[defaultimportfilelocation]"
          msDefaultImportFileLocation = SetString(sItemSetting)
          iINICounter += 1
        Case "[defaultimportfilename]"
          msDefaultImportFileName = SetString(sItemSetting)
          iINICounter += 1
        Case "[reportemailworkpath]"
          msReportEmailWorkPath = SetString(sItemSetting)
          iINICounter += 1
        Case "[emailsender]"
          msEmailSender = SetString(sItemSetting)
          iINICounter += 1
        Case Else
          iINICounter += 1
      End Select
NextRecord:
    End While
    oStreamReader.Close()
    oFileStream.Close()
    If iINICount = iINICounter Then bValid = True
    Return bValid
  End Function
#End Region
End Class
