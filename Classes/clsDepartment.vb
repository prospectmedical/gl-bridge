﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class Department
#Region "Declarations"
  Private miDepartmentID, miGLEntityID, miRowCount As Int32
  Private msDepartmentNumber, msDepartmentName, msReportEmailAddress, msGUID As String
  ' Private mbDuplicateDepartmentNumber As Boolean
#End Region

#Region "Class Properties"
  Public Property DepartmentID() As Int32
    Get
      Return miDepartmentID
    End Get
    Set(ByVal value As Int32)
      miDepartmentID = value
    End Set
  End Property

  Public Property GLEntityID() As Int32
    Get
      Return miGLEntityID
    End Get
    Set(ByVal value As Int32)
      miGLEntityID = value
    End Set
  End Property

  Public Property DepartmentNumber() As String
    Get
      Return msDepartmentNumber
    End Get
    Set(ByVal value As String)
      msDepartmentNumber = value
    End Set
  End Property

  Public Property DepartmentName() As String
    Get
      Return msDepartmentName
    End Get
    Set(ByVal value As String)
      msDepartmentName = value
    End Set
  End Property

  Public Property ReportEmailAddress() As String
    Get
      Return msReportEmailAddress
    End Get
    Set(ByVal value As String)
      msReportEmailAddress = value
    End Set
  End Property

  Public ReadOnly Property DuplicateDepartmentNumber() As Boolean
    Get
      'Return mbDuplicateDepartmentNumber
      Return DuplicateDepartmentNumber(miGLEntityID, miDepartmentID, DepartmentNumber)
    End Get
  End Property

  Public ReadOnly Property RowCount() As Int32
    Get
      Return miRowCount
    End Get
  End Property
#End Region

#Region "Routines"
  Public Sub New()
    Initialize()
  End Sub

  Public Sub Initialize()
    miDepartmentID = 0
    miGLEntityID = 0
    msDepartmentNumber = vbNullString
    msDepartmentName = vbNullString
    msReportEmailAddress = vbNullString
    '  mbDuplicateDepartmentNumber = False
  End Sub
#End Region

#Region "DepartmentGrid"
  Public ReadOnly Property DepartmentGrid() As DataGridTableStyle
    Get
      Dim oDataGridTextBoxColumn As DataGridTextBoxColumn

      DepartmentGrid = New DataGridTableStyle

      oDataGridTextBoxColumn = New DataGridTextBoxColumn
      oDataGridTextBoxColumn.MappingName = "DepartmentID"
      oDataGridTextBoxColumn.HeaderText = "Department ID"
      oDataGridTextBoxColumn.Width = giIDGridWidth
      DepartmentGrid.GridColumnStyles.Add(oDataGridTextBoxColumn)

      oDataGridTextBoxColumn = New DataGridTextBoxColumn
      oDataGridTextBoxColumn.MappingName = "DepartmentNumber"
      oDataGridTextBoxColumn.HeaderText = "Department Number"
      oDataGridTextBoxColumn.Width = 125
      oDataGridTextBoxColumn.Alignment = HorizontalAlignment.Center
      oDataGridTextBoxColumn.TextBox.Enabled = False
      DepartmentGrid.GridColumnStyles.Add(oDataGridTextBoxColumn)

      oDataGridTextBoxColumn = New DataGridTextBoxColumn
      oDataGridTextBoxColumn.MappingName = "DepartmentName"
      oDataGridTextBoxColumn.HeaderText = "Department Name"
      oDataGridTextBoxColumn.Width = 285
      oDataGridTextBoxColumn.Alignment = HorizontalAlignment.Left
      oDataGridTextBoxColumn.NullText = vbNullString
      oDataGridTextBoxColumn.TextBox.Enabled = False
      DepartmentGrid.GridColumnStyles.Add(oDataGridTextBoxColumn)
    End Get
  End Property
#End Region

#Region "GetList"
  Public ReadOnly Property GetList(ByVal GLEntityID As Int32) As DataTable
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim oDataTable As DataTable = New DataTable
      Dim oDataColumn As DataColumn
      Dim oDataRow As DataRow

      oDataColumn = New DataColumn("DepartmentID", System.Type.GetType("System.String"))
      oDataColumn.ReadOnly = False
      oDataColumn.Unique = False
      oDataTable.Columns.Add(oDataColumn)

      oDataColumn = New DataColumn("DepartmentNumber", System.Type.GetType("System.String"))
      oDataColumn.ReadOnly = False
      oDataColumn.Unique = False
      oDataTable.Columns.Add(oDataColumn)

      oDataColumn = New DataColumn("DepartmentName", System.Type.GetType("System.String"))
      oDataColumn.ReadOnly = False
      oDataColumn.Unique = False
      oDataTable.Columns.Add(oDataColumn)

      oDataTable.Clear()
      If OpenDBMessage(oDBC) <> 0 Then Return oDataTable
      oCmd.CommandText = "sDepartmentList"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
      oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID

      Try
        miRowCount = 0
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader
        While oRdr.Read
          oDataRow = oDataTable.NewRow
          oDataRow("DepartmentID") = SetInt32(oRdr!DepartmentID)
          oDataRow("DepartmentNumber") = SetString(oRdr!DepartmentNumber)
          oDataRow("DepartmentName") = SetString(oRdr!DepartmentName)
          oDataTable.Rows.Add(oDataRow)
          miRowCount = miRowCount + 1
        End While
        GetList = oDataTable
      Catch exExc As Exception
        miRowCount = 0
        GetList = Nothing
      Finally
        CloseRDR(oRdr)
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
    End Get
  End Property
#End Region

#Region "DepartmentList"
  Public ReadOnly Property DepartmentList(ByVal GLEntityID As Int32, Optional ByVal BlankValue As Boolean = False) As DataTable
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim oDataTable As DataTable = New DataTable

      SetListTable(oDataTable)
      If OpenDBMessage(oDBC) <> 0 Then Return oDataTable
      oCmd.CommandText = "sDepartmentList"
      oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
      oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
      oCmd.CommandType = CommandType.StoredProcedure
      miRowCount = 0
      If BlankValue Then SetListRow(oDataTable, " ", "0", miRowCount)
      SetListRow(oDataTable, "All Departments", "0", miRowCount)
      Try
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader
        While oRdr.Read
          SetListRow(oDataTable, SetString(oRdr!DepartmentNumber) & ": " & SetString(oRdr!DepartmentName), SetString(oRdr!DepartmentID), miRowCount)
        End While
      Catch exExc As Exception
        oDataTable.Clear()
      Finally
        CloseCMD(oCmd)
        CloseRDR(oRdr)
        CloseDB(oDBC)
      End Try
      Return oDataTable
    End Get
  End Property
#End Region

#Region "ReportEmailList"
  Public ReadOnly Property ReportEmailList(ByVal GLEntityID As Int32, Optional ByVal BlankValue As Boolean = False) As DataTable
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim oDataTable As DataTable = New DataTable

      SetListTable(oDataTable)
      If OpenDBMessage(oDBC) <> 0 Then Return oDataTable
      oCmd.CommandText = "sReportEmailList"
      oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
      oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
      oCmd.CommandType = CommandType.StoredProcedure
      miRowCount = 0
      If BlankValue Then SetListRow(oDataTable, " ", "0", miRowCount)
      SetListRow(oDataTable, "All Email Addresses", "0", miRowCount)
      Try
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader
        While oRdr.Read
          SetListRow(oDataTable, SetString(oRdr!ReportEmailAddress), "0", miRowCount)
        End While
      Catch exExc As Exception
        oDataTable.Clear()
      Finally
        CloseCMD(oCmd)
        CloseRDR(oRdr)
        CloseDB(oDBC)
      End Try
      Return oDataTable
    End Get
  End Property
#End Region

#Region "GetRecord"
  Public ReadOnly Property GetRecord(ByVal DepartmentID As Int32) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim bGetRecord As Boolean = False

      If OpenDBMessage(oDBC) <> 0 Then
        Initialize()
        Return False
      End If
      oCmd.CommandText = "sDepartment"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@DepartmentID", OleDbType.Numeric).Value = DepartmentID
      Try
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader(CommandBehavior.SingleRow)
        oRdr.Read()
        miDepartmentID = SetInt32(oRdr!DepartmentID)
        msDepartmentNumber = SetString(oRdr!DepartmentNumber)
        msDepartmentName = SetString(oRdr!DepartmentName)
        msReportEmailAddress = SetString(oRdr!ReportEmailAddress)
        msGUID = SetString(oRdr!GUID)
        bGetRecord = True
      Catch exExc As Exception
        Initialize()
        bGetRecord = False
      Finally
        CloseRDR(oRdr)
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return bGetRecord
    End Get
  End Property
#End Region

#Region "Valid"
  Public ReadOnly Property Valid() As Boolean
    Get
      Dim bWork As Boolean = False

      'mbDuplicateDepartmentNumber = DuplicateDepartmentNumber(miGLEntityID, miDepartmentID, DepartmentNumber)
      If Len(msDepartmentNumber) = 6 AndAlso Len(msDepartmentName) > 0 AndAlso miGLEntityID > 0 Then
        If Not DuplicateDepartmentNumber(miGLEntityID, miDepartmentID, DepartmentNumber) Then bWork = True
      End If
      Return bWork
    End Get
  End Property
#End Region

#Region "Update"
  Public ReadOnly Property Update() As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oSCmd As OleDbCommand = New OleDbCommand
      Dim oICmd As OleDbCommand = New OleDbCommand
      Dim oUCmd As OleDbCommand = New OleDbCommand
      Dim oDataAdapter As New OleDbDataAdapter
      Dim oDataTable As New DataTable
      Dim oRow As DataRow
      Dim bIsNew As Boolean, bUpdate As Boolean
      Dim sGUID As String

      bUpdate = False
      If OpenDBMessage(oDBC) <> 0 Then Return bUpdate

      oSCmd.CommandText = "sDepartment"
      oSCmd.CommandType = CommandType.StoredProcedure
      oSCmd.Parameters.Add("@DepartmentID", OleDbType.Numeric).Value = miDepartmentID
      oSCmd.Connection = oDBC

      oICmd.CommandText = "aDepartment"
      oICmd.CommandType = CommandType.StoredProcedure
      oICmd.Parameters.Add("@EntityID", OleDbType.Numeric).SourceColumn = "EntityID"
      oICmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).SourceColumn = "GLEntityID"
      oICmd.Parameters.Add("@DepartmentNumber", OleDbType.VarChar, 6).SourceColumn = "DepartmentNumber"
      oICmd.Parameters.Add("@DepartmentName", OleDbType.VarChar, 100).SourceColumn = "DepartmentName"
      oICmd.Parameters.Add("@ReportEmailAddress", OleDbType.VarChar, 2000).SourceColumn = "ReportEmailAddress"
      oICmd.Parameters.Add("@GUID", OleDbType.VarChar, 36).SourceColumn = "GUID"
      oICmd.Parameters.Add("@AccountID", OleDbType.Numeric).Value = giAccountID
      oICmd.Parameters.Add("@NewID", OleDbType.Numeric).Direction = ParameterDirection.Output
      oICmd.Connection = oDBC

      oUCmd.CommandText = "uDepartment"
      oUCmd.CommandType = CommandType.StoredProcedure
      oUCmd.Parameters.Add("@DepartmentID", OleDbType.Numeric).Value = miDepartmentID
      oUCmd.Parameters("@DepartmentID").SourceColumn = "DepartmentID"
      oUCmd.Parameters("@DepartmentID").SourceVersion = DataRowVersion.Original
      oUCmd.Parameters.Add("@EntityID", OleDbType.Numeric).SourceColumn = "EntityID"
      oUCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).SourceColumn = "GLEntityID"
      oUCmd.Parameters.Add("@DepartmentNumber", OleDbType.VarChar, 6).SourceColumn = "DepartmentNumber"
      oUCmd.Parameters.Add("@DepartmentName", OleDbType.VarChar, 100).SourceColumn = "DepartmentName"
      oUCmd.Parameters.Add("@ReportEmailAddress", OleDbType.VarChar, 2000).SourceColumn = "ReportEmailAddress"
      oUCmd.Parameters.Add("@GUID", OleDbType.VarChar, 36).SourceColumn = "GUID"
      oUCmd.Parameters.Add("@AccountID", OleDbType.Numeric).Value = giAccountID
      oUCmd.Connection = oDBC

      oDataAdapter.SelectCommand = oSCmd
      oDataAdapter.InsertCommand = oICmd
      oDataAdapter.UpdateCommand = oUCmd

      Try
        oDataAdapter.Fill(oDataTable)
        If oDataTable.Rows.Count = 0 Then
          oRow = oDataTable.NewRow
          oDataTable.Rows.Add(oRow)
          bIsNew = True
        End If
        If Not bIsNew Then
          If msGUID <> SetString(oDataTable.Rows(0).Item("GUID")) Then
            If DataConflict("Save Department Data") = MsgBoxResult.No Then
              Return bUpdate
            End If
          End If
        End If
        oDataTable.Rows(0).Item("EntityID") = giEntityID
        oDataTable.Rows(0).Item("GLEntityID") = miGLEntityID
        oDataTable.Rows(0).Item("DepartmentNumber") = msDepartmentNumber
        oDataTable.Rows(0).Item("DepartmentName") = msDepartmentName
        oDataTable.Rows(0).Item("ReportEmailAddress") = msReportEmailAddress
        oDataTable.Rows(0).Item("GUID") = System.Guid.NewGuid().ToString
        sGUID = CType(oDataTable.Rows(0).Item("GUID"), String)
        oDataAdapter.Update(oDataTable)
        If bIsNew Then miDepartmentID = SetInt32(oICmd.Parameters("@NewID").Value)
        msGUID = sGUID
        bUpdate = True
      Catch exExc As Exception
        bUpdate = False
      Finally
        CloseDB(oDBC)
        CloseCMD(oSCmd)
        CloseCMD(oICmd)
        CloseCMD(oUCmd)
        oDataAdapter.Dispose()
        oDataTable.Dispose()
        oRow = Nothing
      End Try
      Return bUpdate
    End Get
  End Property
#End Region

#Region "Delete"
  Public ReadOnly Property Delete(ByVal DepartmentID As Int32) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oSCmd As OleDbCommand = New OleDbCommand
      Dim oDCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim sGUID As String
      Dim bDelete As Boolean

      bDelete = False
      sGUID = vbNullString
      If OpenDBMessage(oDBC) <> 0 Then Return bDelete

      ' check guid
      oSCmd.CommandText = "sDepartment"
      oSCmd.CommandType = CommandType.StoredProcedure
      oSCmd.Parameters.Add("@DepartmentID", OleDbType.Numeric).Value = DepartmentID

      Try
        oSCmd.Connection = oDBC
        oRdr = oSCmd.ExecuteReader
        oRdr.Read()
        sGUID = SetString(oRdr!GUID)
      Catch exExc As Exception
        CloseCMD(oSCmd)
        CloseRDR(oRdr)
        CloseDB(oDBC)
        Return bDelete
      Finally
        CloseCMD(oSCmd)
        CloseRDR(oRdr)
      End Try
      If msGUID <> sGUID Then
        If DataConflict("Delete Department") = MsgBoxResult.No Then
          CloseDB(oDBC)
          Return bDelete
        End If
      End If

      ' delete record
      Try
        msGUID = System.Guid.NewGuid().ToString
        oDCmd.CommandText = "dDepartment"
        oDCmd.CommandType = CommandType.StoredProcedure
        oDCmd.Parameters.Add("@DepartmentID", OleDbType.Numeric).Value = DepartmentID
        oDCmd.Parameters.Add("@GUID", OleDbType.VarChar, 36).Value = msGUID
        oDCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
        oDCmd.Parameters.Add("@AccountID", OleDbType.Numeric).Value = giAccountID
        oDCmd.Connection = oDBC
        oDCmd.ExecuteScalar()
        bDelete = True
      Catch exExc As Exception
      Finally
        CloseCMD(oDCmd)
        CloseDB(oDBC)
      End Try
      Return bDelete
    End Get
  End Property
#End Region

#Region "DuplicateDepartmentNumber"
  Private ReadOnly Property DuplicateDepartmentNumber(ByVal GLEntityID As Int32, ByVal DepartmentID As Int32, ByVal DepartmentNumber As String) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing
      Dim bDuplicateDepartmentNumber As Boolean = False

      If OpenDBMessage(oDBC) <> 0 Then
        Initialize()
        Return False
      End If
      oCmd.CommandText = "sDuplicateDepartmentNumber"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
      oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
      oCmd.Parameters.Add("@DepartmentID", OleDbType.Numeric).Value = DepartmentID
      oCmd.Parameters.Add("@DepartmentNumber", OleDbType.VarChar, 6).Value = DepartmentNumber
      Try
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader(CommandBehavior.SingleRow)
        If oRdr.HasRows Then
          bDuplicateDepartmentNumber = True
        Else
          bDuplicateDepartmentNumber = False
        End If
      Catch exExc As Exception
        Initialize()
        bDuplicateDepartmentNumber = True
      Finally
        CloseRDR(oRdr)
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
      Return bDuplicateDepartmentNumber
    End Get
  End Property
#End Region

#Region "Missing"
  Public ReadOnly Property Missing(ByVal GLEntityID As Int32) As OleDbDataReader
    Get
      Dim oDBC As OleDbConnection = New OleDb.OleDbConnection
      Dim oCmd As OleDbCommand = New OleDb.OleDbCommand

      If OpenDBMessage(oDBC) <> 0 Then
        Missing = Nothing
        Exit Property
      End If
      oCmd.CommandText = "sDepartmentListMissing"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.CommandTimeout = giCommandTimeoutLong
      oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
      oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
      Try
        oCmd.Connection = oDBC
        Missing = oCmd.ExecuteReader()
        If Not Missing.HasRows Then Missing = Nothing
      Catch exExc As Exception
        Missing = Nothing
      Finally
      End Try
    End Get
  End Property
#End Region

#Region "ListByNumber"
  Public ReadOnly Property ListByNumber(ByVal GLEntityID As Int32) As OleDbDataReader
    Get
      Dim oDBC As OleDbConnection = New OleDb.OleDbConnection
      Dim oCmd As OleDbCommand = New OleDb.OleDbCommand

      If OpenDBMessage(oDBC) <> 0 Then
        ListByNumber = Nothing
        Exit Property
      End If
      oCmd.CommandText = "sDepartmentListByNumber"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
      oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
      Try
        oCmd.Connection = oDBC
        ListByNumber = oCmd.ExecuteReader()
      Catch exExc As Exception
        ListByNumber = Nothing
      Finally
      End Try
    End Get
  End Property
#End Region

#Region "ListByName"
  Public ReadOnly Property ListByName(ByVal GLEntityID As Int32) As OleDbDataReader
    Get
      Dim oDBC As OleDbConnection = New OleDb.OleDbConnection
      Dim oCmd As OleDbCommand = New OleDb.OleDbCommand

      If OpenDBMessage(oDBC) <> 0 Then
        ListByName = Nothing
        Exit Property
      End If
      oCmd.CommandText = "sDepartmentListByName"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
      oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
      Try
        oCmd.Connection = oDBC
        ListByName = oCmd.ExecuteReader()
      Catch exExc As Exception
        ListByName = Nothing
      Finally
      End Try
    End Get
  End Property
#End Region

#Region "MissingEmail"
  Public ReadOnly Property MissingEmail(ByVal GLEntityID As Int32) As OleDbDataReader
    Get
      Dim oDBC As OleDbConnection = New OleDb.OleDbConnection
      Dim oCmd As OleDbCommand = New OleDb.OleDbCommand

      If OpenDBMessage(oDBC) <> 0 Then
        MissingEmail = Nothing
        Exit Property
      End If
      oCmd.CommandText = "sDepartmentListMissingEmail"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
      oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = GLEntityID
      Try
        oCmd.Connection = oDBC
        MissingEmail = oCmd.ExecuteReader()
      Catch exExc As Exception
        MissingEmail = Nothing
      Finally
      End Try
    End Get
  End Property
#End Region
End Class