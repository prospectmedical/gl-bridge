#Region "Init"
Option Strict On
Option Explicit On
Imports System.Security.Principal
#End Region

Module StartUp
#Region "Declarations"

#End Region

#Region "Main/Start-up Routine"
  Public Sub Main()
    Dim sUserID, sDefaultImportFileLocation, sDefaultImportFileName As String
    Dim ofrmLogon As New frmLogon
    Dim ofrmSplash As New frmSplash
    Dim ofrmMessage As New frmMessage
    Dim oInitializeApplication As New InitializeApplication
    Dim oEntity As New Entity
    Dim oSettings As New Settings
    Dim oWindowsPrincipal As WindowsPrincipal
    Dim oWindowsIdentity As WindowsIdentity
    Dim oLogon As New Logon
    Dim oGLTransactions As New GLTransactions

    ' init global variables
    Try
      giEntityID = 1
      gsAppPath = ApplicationStartupPath()
      gsExportPath = gsAppPath & "Export\"
      gsVersion = "Version " & Application.ProductVersion
      gsServer = "SQLDB03"
      gsDatabaseName = "GLBridge"
      gsDBUser = "GLBridge"
      gsDBPassword = "glbapp7539"
      gsErrorFile = "LOG_" & Format$(Now, "yyyyMMdd") & "_" & Format$(Now, "hhmmss") & ".txt"
    Catch exExc As Exception
      If ofrmMessage.Component(gsDefaultApplicationName & " Global Initialization", "Error:" & vbCrLf & exExc.Message & vbCrLf & "Processing Canceled!") = 0 Then
        ofrmMessage.ShowDialog()
      End If
      End
    End Try

    ' get ini settings
    Try
      With oInitializeApplication
        If Not .GetSettings(gsAppPath, "GLBridge.ini") Then
          OKMessage(gsDefaultApplicationName, "Invalid or missing start-up file." & vbCrLf & "Processing Canceled!", 3)
          End
        End If
        gsExportPath = .ExportPath
        CreateDirectory(gsExportPath)
        gsLogPath = AddBackslash(.LogPath)
        gsImportPath = AddBackslash(.ImportPath)
        CreateDirectory(gsImportPath)
        gsProcessedPath = AddBackslash(.ProcessedPath)
        CreateDirectory(gsProcessedPath)
        gsExportBackupPath = AddBackslash(.ExportBackupPath)
        CreateDirectory(gsExportBackupPath)
        gsReportEmailWorkPath = AddBackslash(.ReportEmailWorkPath)
        CreateDirectory(gsExportBackupPath)
        gsEmailSender = .EmailSender
      End With
    Catch exExc As Exception
      If ofrmMessage.Component(gsDefaultApplicationName & " INI Settings", "Error:" & vbCrLf & exExc.Message & vbCrLf & "Processing Canceled!") = 0 Then
        ofrmMessage.ShowDialog()
      End If
      End
    Finally
    End Try

    gsConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=" & gsDBUser & ";" & _
    "Password=" & gsDBPassword & ";Initial Catalog=" & gsDatabaseName & ";Data Source=" & gsServer & _
    ";Use Procedure for Prepare=1;" & _
      "Auto Translate=True;Packet Size=4096;Workstation ID=APDMApplication;Use Encryption for Data=False;" & _
     "Tag with column collation when possible=False"

    ' check application version against the DB version(s)
    If Not oInitializeApplication.VersionCheck(Application.ProductVersion) Then
      If ofrmMessage.Component(gsDefaultApplicationName, "Error:" & vbCrLf & "The version of this application doesn't match the database version." & vbCrLf & "Processing Canceled!") = 0 Then
        ofrmMessage.ShowDialog()
      End If
      End
    End If

    'retain import file location/name
    sDefaultImportFileLocation = oInitializeApplication.DefaultImportFileLocation
    sDefaultImportFileName = oInitializeApplication.DefaultImportFileName

    oInitializeApplication = Nothing

    ' get entity settings
    Try
      With oEntity
        .GetEntity(giEntityID)
        If .Found Then
          gsEntityDescription = .EntityDescription
          gsImportFileName = .ImportFileName
          gsImportFileExtension = .ImportFileExtension
        Else
          If ofrmMessage.Component(gsDefaultApplicationName, "Error:" & vbCrLf & "Entity ID " & giEntityID & " was not found." & vbCrLf & "Processing Canceled!") = 0 Then
            ofrmMessage.ShowDialog()
          End If
          End
        End If
      End With
    Catch exExc As Exception
      If ofrmMessage.Component(gsDefaultApplicationName & " Entity Settings", "Error:" & vbCrLf & exExc.Message & vbCrLf & "Processing Canceled!") = 0 Then
        ofrmMessage.ShowDialog()
      End If
      End
    Finally
      oEntity = Nothing
    End Try

    ' get db settings
    Try
      With oSettings
        .GetSettings(gsSettingType)
        If .Found Then
          gsProductName = .ApplicationName
          gbBypassLogon = .BypassLogon
          gbForceLogon = .ForceLogon
          gbFadeIn = .FadeIn
        Else
          If ofrmMessage.Component(gsDefaultApplicationName & " Settings Type", "Error:" & vbCrLf & "Setting Type " & gsSettingType & " was not found." & vbCrLf & "Processing Canceled!") = 0 Then
            ofrmMessage.ShowDialog()
          End If
          End
        End If
      End With
      oSettings = Nothing
    Catch exExc As Exception
      OKMessage("Get DB Settings", exExc.Message, 3)
      If ofrmMessage.Component(gsDefaultApplicationName & " Settings Type", "Error:" & vbCrLf & exExc.Message & vbCrLf & "Processing Canceled!") = 0 Then
        ofrmMessage.ShowDialog()
      End If
      End
    End Try

    'check for a valid user id
    AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal)
    oWindowsPrincipal = CType(System.Threading.Thread.CurrentPrincipal, WindowsPrincipal)
    oWindowsIdentity = (CType(oWindowsPrincipal.Identity, WindowsIdentity))
    sUserID = CType(GetUser(oWindowsIdentity.Name), String)

    '   objLogon.UserID = UserID
    If oLogon.Authenticated(sUserID, vbNullString, False) Then
      giAccountID = oLogon.AccountID
      gsUserID = sUserID
      gbAllowMaintenance = oLogon.AllowMaintenance
      gbAllowImport = oLogon.AllowImport
      gbAllowReports = oLogon.AllowReports
      gbAllowUpload = oLogon.AllowUpload
      gbAllowEmail = oLogon.AllowEmail
      If gbAllowImport Then
        If Not oGLTransactions.CheckImportFile(sDefaultImportFileLocation, sDefaultImportFileName) Then
          OKMessage(gsDefaultApplicationName, "Error: Processing import file " & sDefaultImportFileLocation & sDefaultImportFileName & ".", 3)
        End If
      End If

      oLogon = Nothing
      If ofrmSplash.Component = 0 Then
        ofrmSplash.ShowDialog()
      Else
        End
      End If
    Else
      If ofrmMessage.Component(gsDefaultApplicationName & " Settings Type", "Error:" & vbCrLf & "Invalid User ID: " & sUserID & vbCrLf & "Processing Canceled!") = 0 Then
        ofrmMessage.ShowDialog()
      End If
      End
    End If
  End Sub
#End Region
End Module
