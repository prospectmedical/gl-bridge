#Region "Init"
Option Strict On
Option Explicit On
#End Region

Module [Global]
#Region "Variables"
  Public gbFadeIn, gbForceLogon, gbBypassLogon, gbAllowMaintenance, gbAllowImport, gbAllowReports, gbAllowUpload, gbAllowEmail As Boolean
  Public gsProductName, gsVersion, gsUserID, gsConnectionString, gsLogPath, gsErrorFile, gsExportPath, gsAppPath As String
  Public gsDBUser, gsDBPassword, gsDatabaseName, gsServer, gsImportPath, gsEntityDescription, gsReportEmailWorkPath As String
  Public gsImportFileName, gsImportFileExtension, gsProcessedPath, gsExportBackupPath, gsEmailSender As String
  Public giAccountID, giEntityID As Int32
#End Region

#Region "Constants"
  Public Const gsDefaultApplicationName As String = "G/L Bridge"
  Public Const gsSettingType As String = "Prod"
  Public Const giIDGridWidth As Int32 = 0
  Public goStartupPosition As System.Windows.Forms.FormStartPosition = FormStartPosition.CenterScreen
  Public Const giCommandTimeoutLong As Int32 = 240
#End Region

#Region "Forms"
  Public gmdiMain As New mdiMain
#End Region
End Module
