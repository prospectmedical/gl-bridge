﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("GLBridge")> 
<Assembly: AssemblyDescription("G/L Interface between PMM and Siemens")> 
<Assembly: AssemblyCompany("Crozer-Keystone Health System")> 
<Assembly: AssemblyProduct("GLBridge")> 
<Assembly: AssemblyCopyright("Copyright © Crozer-Keystone Health System 2008")> 
<Assembly: AssemblyTrademark("Crozer-Keystone Health System 2008")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("fbeb449b-efa9-403f-b0b6-be8dd7f50ead")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("4.6.2016.1101")> 
<Assembly: AssemblyFileVersion("4.6.2016.1101")> 
