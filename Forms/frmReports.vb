#Region "Init"
Option Explicit On
Option Strict On
#End Region

Public Class frmReports
#Region "Declarations"
  Private msReportName1, msReportName2 As String
  Private moSettings As New Settings
#End Region

#Region "Routines"
  Public Sub New()
    InitializeComponent()
  End Sub

  Public Function Component(ByVal ReportName1 As String, Optional ByVal ReportName2 As String = vbNullString) As Boolean
    moSettings.FormStartPosition(Me)
    Me.WindowState = FormWindowState.Maximized
    msReportName1 = ReportName1
    msReportName2 = ReportName2
    Me.Text = FullReportName()
    Me.arvMain.ReportViewer.ViewType = DataDynamics.ActiveReports.Viewer.ViewType.ContinuousScroll
    ModToolBar()
    Return True
  End Function

  Private Function FullReportName() As String
    Dim sWork As String

    sWork = Trim$(msReportName1)
    If LCase$(sWork) = "g/l transactions" Then sWork = "GL Transactions"
    If LCase$(sWork) = "g/l transaction detail" Then sWork = "GL Transaction Detail"
    If Trim$(msReportName2) <> vbNullString Then
      If sWork <> vbNullString Then sWork = sWork & " "
      sWork = sWork & msReportName2
    End If
    Return sWork
  End Function

  Private Sub ModToolBar()
    Dim oToolbarButton As New DataDynamics.ActiveReports.Toolbar.Button

    arvMain.Toolbar.Tools.RemoveAt(23)
    arvMain.Toolbar.Tools.RemoveAt(5)
    arvMain.Toolbar.Tools.RemoveAt(4)

    oToolbarButton = New DataDynamics.ActiveReports.Toolbar.Button
    oToolbarButton.Caption = "&Close"
    oToolbarButton.ToolTip = "Close Report"
    oToolbarButton.ButtonStyle = DataDynamics.ActiveReports.Toolbar.ButtonStyle.Text
    oToolbarButton.Id = 1000
    Me.arvMain.Toolbar.Tools.Insert(21, oToolbarButton)

    oToolbarButton = New DataDynamics.ActiveReports.Toolbar.Button
    oToolbarButton.Caption = "Excel"
    oToolbarButton.ToolTip = "Export Report To An Excel File"
    oToolbarButton.ButtonStyle = DataDynamics.ActiveReports.Toolbar.ButtonStyle.Text
    oToolbarButton.Id = 1001
    Me.arvMain.Toolbar.Tools.Insert(2, oToolbarButton)

    oToolbarButton = New DataDynamics.ActiveReports.Toolbar.Button
    oToolbarButton.Caption = "PDF"
    oToolbarButton.ToolTip = "Export Report To A PDF File"
    oToolbarButton.ButtonStyle = DataDynamics.ActiveReports.Toolbar.ButtonStyle.Text
    oToolbarButton.Id = 1002
    Me.arvMain.Toolbar.Tools.Insert(3, oToolbarButton)
  End Sub
#End Region

#Region "Form Events"
  Private Sub frmReports_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Exit Sub
  End Sub

  Private Sub frmReports_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    Me.WindowState = FormWindowState.Normal
  End Sub
#End Region

#Region "Controls"
  Private Sub arvMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles arvMain.Load
    Exit Sub
  End Sub

  Private Sub arvMain_ToolClick(ByVal sender As Object, ByVal e As DataDynamics.ActiveReports.Toolbar.ToolClickEventArgs) Handles arvMain.ToolClick
    Dim oPDFExport As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
    Dim oXLSExport As New DataDynamics.ActiveReports.Export.Xls.XlsExport
    Dim sExportFile As String

    Select Case e.Tool.Id
      Case 1000
        Me.Close()
      Case 1001
        Try
          With sfdReports
            .Title = "Export " & FullReportName() & " to Excel"
            .Filter = "Excel File|*.xls|All Files|*.*"
            .InitialDirectory = gsExportPath
            .FileName = FullReportName() & ".xls"
            .CheckFileExists = False
            .CheckPathExists = True
            .OverwritePrompt = True
            .ShowHelp = False
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
            sExportFile = .FileName
          End With
          oXLSExport.Export(arvMain.Document, sExportFile)
          OKMessage(FullReportName, "Export Complete!", 1)
        Catch exExc As Exception
          OKMessage(FullReportName, "Error: " & exExc.Message & vbCrLf & "Exporting the report." & vbCrLf & "Export Canceled!", 3)
        End Try
      Case 1002
        Try
          With sfdReports
            .Title = "Export " & FullReportName() & " to PDF"
            .Filter = "PDF File|*.pdf|All Files|*.*"
            .InitialDirectory = gsExportPath
            .FileName = FullReportName() & ".pdf"
            .CheckFileExists = False
            .CheckPathExists = True
            .OverwritePrompt = True
            .ShowHelp = False
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Exit Sub
            sExportFile = .FileName
          End With
          oPDFExport.Export(arvMain.Document, sExportFile)
          OKMessage(FullReportName, "Export Complete!", 1)
        Catch exExc As Exception
          OKMessage(FullReportName, "Error: " & exExc.Message & vbCrLf & "Exporting the report." & vbCrLf & "Export Canceled!", 3)
        End Try
    End Select
  End Sub
#End Region

#Region "Reports"
#Region "GLTransactions"
  Public ReadOnly Property GLTransactions(ByVal MonthSelected As Int32, ByVal YearSelected As Int32, ByVal GLEntityID As Int32) As Boolean
    Get
      Try
        Dim oarGLTransactions As New arGLTransactions
        With oarGLTransactions
          If Not .Component(msReportName1, MonthSelected, YearSelected, GLEntityID) Then Return False
          .PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait
          .Run()
        End With
        With Me.arvMain
          .TableOfContents.Enabled = False
          .TableOfContents.Visible = False
          .TableOfContents.Width = 0
          .Document = oarGLTransactions.Document
        End With
        Return True
      Catch exExc As DataDynamics.ActiveReports.ReportException
        OKMessage(Me.Text, exExc.Message, 3)
        Return False
      End Try
    End Get
  End Property
#End Region

#Region "GLTransactionDetail"
  Public ReadOnly Property GLTransactionDetail(ByVal GLEntityID As Int32, ByVal YearSelected As Int32, ByVal MonthSelected As Int32, _
  ByVal ReportEmailSelected As String, ByVal DepartmentSelected As String) As Boolean
    Get
      Try
        Dim oarGLTransactionDetail As New arGLTransactionDetail
        With oarGLTransactionDetail
          If Not .Component(msReportName1, GLEntityID, YearSelected, MonthSelected, ReportEmailSelected, DepartmentSelected) Then Return False
          .PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Landscape
          .Run()
        End With
        With Me.arvMain
          .TableOfContents.Enabled = True
          .TableOfContents.Visible = True
          .TableOfContents.Width = 200
          .Document = oarGLTransactionDetail.Document
        End With
        Return True
      Catch exExc As DataDynamics.ActiveReports.ReportException
        OKMessage(Me.Text, exExc.Message, 3)
        Return False
      End Try
    End Get
  End Property
#End Region

#Region "DepartmentListByNumber"
  Public ReadOnly Property DepartmentListByNumber(ByVal GLEntityID As Int32) As Boolean
    Get
      Try
        Dim oarDepartmentListByNumber As New arDepartmentListByNumber
        With oarDepartmentListByNumber
          If Not .Component(msReportName1, GLEntityID) Then Return False
          .PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait
          .Run()
        End With
        With Me.arvMain
          .TableOfContents.Enabled = False
          .TableOfContents.Visible = False
          .TableOfContents.Width = 0
          .Document = oarDepartmentListByNumber.Document
        End With
        Return True
      Catch exExc As DataDynamics.ActiveReports.ReportException
        OKMessage(Me.Text, exExc.Message, 3)
        Return False
      End Try
    End Get
  End Property
#End Region

#Region "DepartmentListByName"
  Public ReadOnly Property DepartmentListByName(ByVal GLEntityID As Int32) As Boolean
    Get
      Try
        Dim oarDepartmentListByName As New arDepartmentListByName
        With oarDepartmentListByName
          If Not .Component(msReportName1, GLEntityID) Then Return False
          .PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait
          .Run()

          '.Document.Printer.PrinterSettings.Copies = 1
          '.Document.Printer.PrinterName = "\\prntsrv02\QUEUE34367"
          '.Document.Print(False, False)

        End With
        With Me.arvMain
          .TableOfContents.Enabled = False
          .TableOfContents.Visible = False
          .TableOfContents.Width = 0
          .Document = oarDepartmentListByName.Document
        End With
        Return True
      Catch exExc As DataDynamics.ActiveReports.ReportException
        OKMessage(Me.Text, exExc.Message, 3)
        Return False
      End Try
    End Get
  End Property
#End Region

#Region "DepartmentListMissing"
  Public ReadOnly Property DepartmentListMissing(ByVal GLEntityID As Int32) As Boolean
    Get
      Try
        Dim oarDepartmentListMissing As New arDepartmentListMissing
        With oarDepartmentListMissing
          If Not .Component(msReportName1, GLEntityID) Then Return False
          .PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait
          .Run()
        End With
        With Me.arvMain
          .TableOfContents.Enabled = False
          .TableOfContents.Visible = False
          .TableOfContents.Width = 0
          .Document = oarDepartmentListMissing.Document
        End With
        Return True
      Catch exExc As DataDynamics.ActiveReports.ReportException
        OKMessage(Me.Text, exExc.Message, 3)
        Return False
      End Try
    End Get
  End Property
#End Region

#Region "DepartmentListMissingEMail"
  Public ReadOnly Property DepartmentListMissingEMail(ByVal GLEntityID As Int32) As Boolean
    Get
      Try
        Dim oarDepartmentListMissingEmail As New arDepartmentListMissingEmail
        With oarDepartmentListMissingEmail
          If Not .Component(msReportName1, GLEntityID) Then Return False
          .PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait
          .Run()
        End With
        With Me.arvMain
          .TableOfContents.Enabled = False
          .TableOfContents.Visible = False
          .TableOfContents.Width = 0
          .Document = oarDepartmentListMissingEmail.Document
        End With
        Return True
      Catch exExc As DataDynamics.ActiveReports.ReportException
        OKMessage(Me.Text, exExc.Message, 3)
        Return False
      End Try
    End Get
  End Property
#End Region
#End Region
End Class