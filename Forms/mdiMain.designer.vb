<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mdiMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mdiMain))
    Me.MenuStrip = New System.Windows.Forms.MenuStrip
    Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripSeparator
    Me.mnuMaintenance = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuDepartment = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuGLEntityMaintenance = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripSeparator
    Me.mnuMonthlyProcessing = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuImportInventoryTransactions = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuLine01 = New System.Windows.Forms.ToolStripSeparator
    Me.mnuUploadGLBatch = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuLine02 = New System.Windows.Forms.ToolStripSeparator
    Me.mnuGLTransactionDetailEmail = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuLine03 = New System.Windows.Forms.ToolStripSeparator
    Me.mnuReports = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuDepartmentList = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuDepartmentListByName = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuDepartmentListByNumber = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuMissingDepartments = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuMissingEmail = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuGLTransactions = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuTransactionDetail = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator
    Me.mnuWindows = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuCascade = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuTileVertical = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuTileHorizontal = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuArrangeIcons = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
    Me.mnuOpenItems = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator
    Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem
    Me.mnuAbout = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripSeparator
    Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
    Me.tmrCheck = New System.Windows.Forms.Timer(Me.components)
    Me.ssMain = New System.Windows.Forms.StatusStrip
    Me.tsslPicture = New System.Windows.Forms.ToolStripStatusLabel
    Me.tsslProcessing = New System.Windows.Forms.ToolStripStatusLabel
    Me.tsslSendingEmail = New System.Windows.Forms.ToolStripStatusLabel
    Me.tsslProcessingEMail = New System.Windows.Forms.ToolStripStatusLabel
    Me.tmrBlink = New System.Windows.Forms.Timer(Me.components)
    Me.MenuStrip.SuspendLayout()
    Me.ssMain.SuspendLayout()
    Me.SuspendLayout()
    '
    'MenuStrip
    '
    Me.MenuStrip.BackColor = System.Drawing.SystemColors.GradientActiveCaption
    Me.MenuStrip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
    Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuMaintenance, Me.mnuMonthlyProcessing, Me.mnuReports, Me.mnuWindows, Me.mnuHelp})
    Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip.MdiWindowListItem = Me.mnuWindows
    Me.MenuStrip.Name = "MenuStrip"
    Me.MenuStrip.Size = New System.Drawing.Size(986, 24)
    Me.MenuStrip.TabIndex = 5
    Me.MenuStrip.Text = "MenuStrip"
    '
    'mnuFile
    '
    Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExit, Me.ToolStripMenuItem8})
    Me.mnuFile.Image = CType(resources.GetObject("mnuFile.Image"), System.Drawing.Image)
    Me.mnuFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuFile.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder
    Me.mnuFile.Name = "mnuFile"
    Me.mnuFile.Size = New System.Drawing.Size(54, 20)
    Me.mnuFile.Text = "&File"
    '
    'mnuExit
    '
    Me.mnuExit.Image = CType(resources.GetObject("mnuExit.Image"), System.Drawing.Image)
    Me.mnuExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuExit.Name = "mnuExit"
    Me.mnuExit.Size = New System.Drawing.Size(106, 22)
    Me.mnuExit.Text = "E&xit"
    '
    'ToolStripMenuItem8
    '
    Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
    Me.ToolStripMenuItem8.Size = New System.Drawing.Size(103, 6)
    '
    'mnuMaintenance
    '
    Me.mnuMaintenance.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDepartment, Me.mnuGLEntityMaintenance, Me.ToolStripMenuItem7})
    Me.mnuMaintenance.Image = CType(resources.GetObject("mnuMaintenance.Image"), System.Drawing.Image)
    Me.mnuMaintenance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuMaintenance.Name = "mnuMaintenance"
    Me.mnuMaintenance.Size = New System.Drawing.Size(108, 20)
    Me.mnuMaintenance.Text = "&Maintenance"
    '
    'mnuDepartment
    '
    Me.mnuDepartment.Image = CType(resources.GetObject("mnuDepartment.Image"), System.Drawing.Image)
    Me.mnuDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuDepartment.Name = "mnuDepartment"
    Me.mnuDepartment.Size = New System.Drawing.Size(154, 22)
    Me.mnuDepartment.Text = "&Department"
    '
    'mnuGLEntityMaintenance
    '
    Me.mnuGLEntityMaintenance.Image = CType(resources.GetObject("mnuGLEntityMaintenance.Image"), System.Drawing.Image)
    Me.mnuGLEntityMaintenance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuGLEntityMaintenance.Name = "mnuGLEntityMaintenance"
    Me.mnuGLEntityMaintenance.Size = New System.Drawing.Size(154, 22)
    Me.mnuGLEntityMaintenance.Text = "&G/L Entity"
    '
    'ToolStripMenuItem7
    '
    Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
    Me.ToolStripMenuItem7.Size = New System.Drawing.Size(151, 6)
    '
    'mnuMonthlyProcessing
    '
    Me.mnuMonthlyProcessing.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuImportInventoryTransactions, Me.mnuLine01, Me.mnuUploadGLBatch, Me.mnuLine02, Me.mnuGLTransactionDetailEmail, Me.mnuLine03})
    Me.mnuMonthlyProcessing.Image = CType(resources.GetObject("mnuMonthlyProcessing.Image"), System.Drawing.Image)
    Me.mnuMonthlyProcessing.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuMonthlyProcessing.Name = "mnuMonthlyProcessing"
    Me.mnuMonthlyProcessing.Size = New System.Drawing.Size(145, 20)
    Me.mnuMonthlyProcessing.Text = "&Monthly &Processing"
    '
    'mnuImportInventoryTransactions
    '
    Me.mnuImportInventoryTransactions.Image = CType(resources.GetObject("mnuImportInventoryTransactions.Image"), System.Drawing.Image)
    Me.mnuImportInventoryTransactions.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuImportInventoryTransactions.Name = "mnuImportInventoryTransactions"
    Me.mnuImportInventoryTransactions.Size = New System.Drawing.Size(261, 22)
    Me.mnuImportInventoryTransactions.Text = "&Import Inventory Transactions"
    '
    'mnuLine01
    '
    Me.mnuLine01.Name = "mnuLine01"
    Me.mnuLine01.Size = New System.Drawing.Size(258, 6)
    '
    'mnuUploadGLBatch
    '
    Me.mnuUploadGLBatch.Image = CType(resources.GetObject("mnuUploadGLBatch.Image"), System.Drawing.Image)
    Me.mnuUploadGLBatch.Name = "mnuUploadGLBatch"
    Me.mnuUploadGLBatch.Size = New System.Drawing.Size(261, 22)
    Me.mnuUploadGLBatch.Text = "&Upload G/L Batch"
    '
    'mnuLine02
    '
    Me.mnuLine02.Name = "mnuLine02"
    Me.mnuLine02.Size = New System.Drawing.Size(258, 6)
    '
    'mnuGLTransactionDetailEmail
    '
    Me.mnuGLTransactionDetailEmail.Image = CType(resources.GetObject("mnuGLTransactionDetailEmail.Image"), System.Drawing.Image)
    Me.mnuGLTransactionDetailEmail.Name = "mnuGLTransactionDetailEmail"
    Me.mnuGLTransactionDetailEmail.Size = New System.Drawing.Size(261, 22)
    Me.mnuGLTransactionDetailEmail.Text = "G/L &Transaction Detail Email"
    '
    'mnuLine03
    '
    Me.mnuLine03.Name = "mnuLine03"
    Me.mnuLine03.Size = New System.Drawing.Size(258, 6)
    '
    'mnuReports
    '
    Me.mnuReports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDepartmentList, Me.mnuGLTransactions, Me.mnuTransactionDetail, Me.ToolStripMenuItem4})
    Me.mnuReports.Image = CType(resources.GetObject("mnuReports.Image"), System.Drawing.Image)
    Me.mnuReports.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuReports.Name = "mnuReports"
    Me.mnuReports.Size = New System.Drawing.Size(80, 20)
    Me.mnuReports.Text = "&Reports"
    '
    'mnuDepartmentList
    '
    Me.mnuDepartmentList.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDepartmentListByName, Me.mnuDepartmentListByNumber, Me.mnuMissingDepartments, Me.mnuMissingEmail})
    Me.mnuDepartmentList.Image = CType(resources.GetObject("mnuDepartmentList.Image"), System.Drawing.Image)
    Me.mnuDepartmentList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuDepartmentList.Name = "mnuDepartmentList"
    Me.mnuDepartmentList.Size = New System.Drawing.Size(248, 22)
    Me.mnuDepartmentList.Text = "Department &List"
    '
    'mnuDepartmentListByName
    '
    Me.mnuDepartmentListByName.Image = CType(resources.GetObject("mnuDepartmentListByName.Image"), System.Drawing.Image)
    Me.mnuDepartmentListByName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuDepartmentListByName.Name = "mnuDepartmentListByName"
    Me.mnuDepartmentListByName.Size = New System.Drawing.Size(218, 22)
    Me.mnuDepartmentListByName.Text = "By Department N&ame"
    '
    'mnuDepartmentListByNumber
    '
    Me.mnuDepartmentListByNumber.Image = CType(resources.GetObject("mnuDepartmentListByNumber.Image"), System.Drawing.Image)
    Me.mnuDepartmentListByNumber.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuDepartmentListByNumber.Name = "mnuDepartmentListByNumber"
    Me.mnuDepartmentListByNumber.Size = New System.Drawing.Size(218, 22)
    Me.mnuDepartmentListByNumber.Text = "By Department &Number"
    '
    'mnuMissingDepartments
    '
    Me.mnuMissingDepartments.Image = CType(resources.GetObject("mnuMissingDepartments.Image"), System.Drawing.Image)
    Me.mnuMissingDepartments.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuMissingDepartments.Name = "mnuMissingDepartments"
    Me.mnuMissingDepartments.Size = New System.Drawing.Size(218, 22)
    Me.mnuMissingDepartments.Text = "&Missing Departments"
    '
    'mnuMissingEmail
    '
    Me.mnuMissingEmail.Image = CType(resources.GetObject("mnuMissingEmail.Image"), System.Drawing.Image)
    Me.mnuMissingEmail.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuMissingEmail.Name = "mnuMissingEmail"
    Me.mnuMissingEmail.Size = New System.Drawing.Size(218, 22)
    Me.mnuMissingEmail.Text = "Missing &Email"
    '
    'mnuGLTransactions
    '
    Me.mnuGLTransactions.Image = CType(resources.GetObject("mnuGLTransactions.Image"), System.Drawing.Image)
    Me.mnuGLTransactions.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuGLTransactions.Name = "mnuGLTransactions"
    Me.mnuGLTransactions.Size = New System.Drawing.Size(248, 22)
    Me.mnuGLTransactions.Text = "G/L &Transactions"
    '
    'mnuTransactionDetail
    '
    Me.mnuTransactionDetail.Image = CType(resources.GetObject("mnuTransactionDetail.Image"), System.Drawing.Image)
    Me.mnuTransactionDetail.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuTransactionDetail.Name = "mnuTransactionDetail"
    Me.mnuTransactionDetail.Size = New System.Drawing.Size(211, 22)
    Me.mnuTransactionDetail.Text = "G/L Transaction &Detail"
    '
    'ToolStripMenuItem4
    '
    Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
    Me.ToolStripMenuItem4.Size = New System.Drawing.Size(245, 6)
    '
    'mnuWindows
    '
    Me.mnuWindows.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCascade, Me.mnuTileVertical, Me.mnuTileHorizontal, Me.mnuArrangeIcons, Me.ToolStripSeparator2, Me.mnuOpenItems, Me.ToolStripMenuItem5})
    Me.mnuWindows.Image = CType(resources.GetObject("mnuWindows.Image"), System.Drawing.Image)
    Me.mnuWindows.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuWindows.Name = "mnuWindows"
    Me.mnuWindows.Size = New System.Drawing.Size(85, 20)
    Me.mnuWindows.Text = "&Windows"
    '
    'mnuCascade
    '
    Me.mnuCascade.Image = CType(resources.GetObject("mnuCascade.Image"), System.Drawing.Image)
    Me.mnuCascade.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuCascade.Name = "mnuCascade"
    Me.mnuCascade.Size = New System.Drawing.Size(166, 22)
    Me.mnuCascade.Text = "&Cascade"
    '
    'mnuTileVertical
    '
    Me.mnuTileVertical.Image = CType(resources.GetObject("mnuTileVertical.Image"), System.Drawing.Image)
    Me.mnuTileVertical.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuTileVertical.Name = "mnuTileVertical"
    Me.mnuTileVertical.Size = New System.Drawing.Size(166, 22)
    Me.mnuTileVertical.Text = "Tile &Vertical"
    '
    'mnuTileHorizontal
    '
    Me.mnuTileHorizontal.Image = CType(resources.GetObject("mnuTileHorizontal.Image"), System.Drawing.Image)
    Me.mnuTileHorizontal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuTileHorizontal.Name = "mnuTileHorizontal"
    Me.mnuTileHorizontal.Size = New System.Drawing.Size(166, 22)
    Me.mnuTileHorizontal.Text = "Tile &Horizontal"
    '
    'mnuArrangeIcons
    '
    Me.mnuArrangeIcons.Image = CType(resources.GetObject("mnuArrangeIcons.Image"), System.Drawing.Image)
    Me.mnuArrangeIcons.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuArrangeIcons.Name = "mnuArrangeIcons"
    Me.mnuArrangeIcons.Size = New System.Drawing.Size(166, 22)
    Me.mnuArrangeIcons.Text = "&Arrange Icons"
    '
    'ToolStripSeparator2
    '
    Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
    Me.ToolStripSeparator2.Size = New System.Drawing.Size(163, 6)
    '
    'mnuOpenItems
    '
    Me.mnuOpenItems.Image = CType(resources.GetObject("mnuOpenItems.Image"), System.Drawing.Image)
    Me.mnuOpenItems.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuOpenItems.Name = "mnuOpenItems"
    Me.mnuOpenItems.Size = New System.Drawing.Size(166, 22)
    Me.mnuOpenItems.Text = "Open Items"
    '
    'ToolStripMenuItem5
    '
    Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
    Me.ToolStripMenuItem5.Size = New System.Drawing.Size(163, 6)
    '
    'mnuHelp
    '
    Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAbout, Me.ToolStripMenuItem6})
    Me.mnuHelp.Image = CType(resources.GetObject("mnuHelp.Image"), System.Drawing.Image)
    Me.mnuHelp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuHelp.ImageTransparentColor = System.Drawing.Color.Transparent
    Me.mnuHelp.Name = "mnuHelp"
    Me.mnuHelp.Size = New System.Drawing.Size(60, 20)
    Me.mnuHelp.Text = "&Help"
    '
    'mnuAbout
    '
    Me.mnuAbout.Image = CType(resources.GetObject("mnuAbout.Image"), System.Drawing.Image)
    Me.mnuAbout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.mnuAbout.Name = "mnuAbout"
    Me.mnuAbout.Size = New System.Drawing.Size(131, 22)
    Me.mnuAbout.Text = "&About ..."
    '
    'ToolStripMenuItem6
    '
    Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
    Me.ToolStripMenuItem6.Size = New System.Drawing.Size(128, 6)
    '
    'ssMain
    '
    Me.ssMain.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
    Me.ssMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslPicture, Me.tsslProcessing, Me.tsslSendingEmail, Me.tsslProcessingEMail})
    Me.ssMain.Location = New System.Drawing.Point(0, 748)
    Me.ssMain.Name = "ssMain"
    Me.ssMain.Size = New System.Drawing.Size(986, 22)
    Me.ssMain.TabIndex = 12
    Me.ssMain.Text = "StatusStrip1"
    '
    'tsslPicture
    '
    Me.tsslPicture.Image = CType(resources.GetObject("tsslPicture.Image"), System.Drawing.Image)
    Me.tsslPicture.Name = "tsslPicture"
    Me.tsslPicture.Size = New System.Drawing.Size(16, 17)
    '
    'tsslProcessing
    '
    Me.tsslProcessing.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.tsslProcessing.Name = "tsslProcessing"
    Me.tsslProcessing.Size = New System.Drawing.Size(74, 17)
    Me.tsslProcessing.Text = "tsslProcessing"
    '
    'tsslSendingEmail
    '
    Me.tsslSendingEmail.Image = CType(resources.GetObject("tsslSendingEmail.Image"), System.Drawing.Image)
    Me.tsslSendingEmail.Name = "tsslSendingEmail"
    Me.tsslSendingEmail.Size = New System.Drawing.Size(16, 17)
    '
    'tsslProcessingEMail
    '
    Me.tsslProcessingEMail.Name = "tsslProcessingEMail"
    Me.tsslProcessingEMail.Size = New System.Drawing.Size(98, 17)
    Me.tsslProcessingEMail.Text = "tsslProcessingEMail"
    '
    'tmrBlink
    '
    '
    'mdiMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(986, 770)
    Me.Controls.Add(Me.ssMain)
    Me.Controls.Add(Me.MenuStrip)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.IsMdiContainer = True
    Me.MinimumSize = New System.Drawing.Size(583, 418)
    Me.Name = "mdiMain"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "G/L Bridge"
    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    Me.MenuStrip.ResumeLayout(False)
    Me.MenuStrip.PerformLayout()
    Me.ssMain.ResumeLayout(False)
    Me.ssMain.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuAbout As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuArrangeIcons As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuWindows As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuCascade As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuTileVertical As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuTileHorizontal As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
  Friend WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
  Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents mnuOpenItems As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuReports As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents tmrCheck As System.Windows.Forms.Timer
  Friend WithEvents ssMain As System.Windows.Forms.StatusStrip
  Friend WithEvents tsslProcessing As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents mnuMaintenance As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuMonthlyProcessing As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuImportInventoryTransactions As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuGLTransactions As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuTransactionDetail As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuGLEntityMaintenance As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuDepartment As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuDepartmentList As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuDepartmentListByNumber As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuDepartmentListByName As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuMissingDepartments As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuMissingEmail As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents tsslPicture As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents tmrBlink As System.Windows.Forms.Timer
  Friend WithEvents tsslSendingEmail As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents tsslProcessingEMail As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents mnuLine01 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents mnuUploadGLBatch As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuLine02 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents mnuGLTransactionDetailEmail As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItem8 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ToolStripMenuItem7 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents mnuLine03 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ToolStripMenuItem6 As System.Windows.Forms.ToolStripSeparator

End Class
