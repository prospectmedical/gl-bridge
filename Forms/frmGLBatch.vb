﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
Imports System.Data.OleDb
#End Region

Public Class frmGLBatch
#Region "Declarations"
  Private mbIsLoading As Boolean
  Private mbMarkBatch As Boolean = True
  Private miMonthSelected, miYearSelected, miGLEntityIDSelected, miBatchSequence, miDetailRecordCount, miTotalRecordCount As Int32
  Private mdAmount, mdBatchTotal, mdTotal As Double
  Private msGLEntityCode, msExportPath, msExportFileName, msExportFileExtension, msDetailHeader1, msDetailHeader2 As String
  Private msCreditAccountNumber, msCreditSubAccountNumber, msOutputPathFileName, msErrorMessage As String
  Private mdtBatchDate As Date
  Private moSettings As New Settings
  Private moGLEntity As New GLEntity
  Private moFileInfo As IO.FileInfo
  Private moFileStream As FileStream = Nothing
  Private moStreamWriter As StreamWriter = Nothing
#End Region

#Region "Form Properties"
  Public ReadOnly Property MonthSelected() As Int32
    Get
      Return miMonthSelected
    End Get
  End Property

  Public ReadOnly Property YearSelected() As Int32
    Get
      Return miYearSelected
    End Get
  End Property

  Public ReadOnly Property GLEntityIDSelected() As Int32
    Get
      Return miGLentityIDSelected
    End Get
  End Property
#End Region

#Region "Form Events"
  Private Sub frmGLBatch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Exit Sub
  End Sub
#End Region

#Region "Routines"
  Public Sub New()
    InitializeComponent()
    Initialize()
  End Sub

  Private Sub Initialize()
    Exit Sub
  End Sub

  Public Function Component(ByVal HeaderText As String) As Boolean
    Dim oPopulate As New Populate
    Dim iMonthCalc, iYear As Int32

    mbIsLoading = True
    Me.Text = HeaderText
    moSettings.FormStartPosition(Me)
    UpdateStatus("Waiting to start the upload.")
    pnlButtons.Left = CenterControl(Me.Width, pnlButtons.Width)
    oPopulate.GLEntityDescriptionComboBox(cbGLEntity, True, True)
    oPopulate.MonthsComboBox(cbMonth, False)
    oPopulate.YearsComboBox(cbYear, False)
    iMonthCalc = SetInt32(Month(Now) - 2)
    iYear = Year(Now)
    If iMonthCalc < 0 Then
      iMonthCalc = 11
      iYear = iYear - 1
    End If
    cbMonth.SelectedIndex = iMonthCalc
    miMonthSelected = SetInt32(cbMonth.SelectedValue)
    SetComboBox(cbYear, iYear)
    miYearSelected = SetInt32(cbYear.SelectedValue)
    cbGLEntity.SelectedIndex = 0
    SetButtons()
    mbIsLoading = False
    cbGLEntity.Focus()
    Return True
  End Function

  Private Sub SetButtons()
    If cbMonth.SelectedIndex >= 0 AndAlso cbYear.SelectedIndex >= 0 AndAlso cbGLEntity.SelectedIndex > 0 Then
      btnOk.Enabled = True
    Else
      btnOk.Enabled = False
    End If
  End Sub

  Private Sub ErrorProcessing(ByVal Message As String)
    mbMarkBatch = False
    UpdateStatus(" ")
    UpdateStatus(Message)
    UpdateStatus("Processing Canceled!")
    moSettings.DefaultCursor(Me)
    OKMessage(gsProductName, Message & vbCrLf & "Processing Canceled!", 3)
    btnCancel.Enabled = True
    FileClose()
  End Sub

  Private Function FileClose() As Boolean
    Try
      moStreamWriter.Close()
      moFileStream.Close()
      Return True
    Catch exExc As Exception
      Return False
    Finally
    End Try
  End Function

  Private Function OpenOutputFile() As Boolean
    Dim sOutRecord As String
    Dim bWork As Boolean = False

    Try
      miBatchSequence += 1
      msOutputPathFileName = gsExportBackupPath & msExportFileName & "_" & miYearSelected & Format$(miMonthSelected, "0#") & "." & SetString(miBatchSequence)
      'UpdateStatus(" ")
      UpdateStatus("Open Upload File: " & msOutputPathFileName)
      moFileInfo = Nothing
      moFileInfo = New IO.FileInfo(msOutputPathFileName)
      If moFileInfo.Exists Then moFileInfo.Delete()
      moFileStream = New FileStream(msOutputPathFileName, FileMode.Create, FileAccess.Write)
      moStreamWriter = New StreamWriter(moFileStream)
      With moGLEntity
        sOutRecord = .HeaderCode1(miGLEntityIDSelected) & .HeaderCode2(miGLEntityIDSelected) & .TerminalID(miGLEntityIDSelected) & _
        .SiemensHospitalCode(miGLEntityIDSelected) & Format$(mdtBatchDate, "MMddyy") & .HeaderCode3(miGLEntityIDSelected) & _
        msExportFileName & MakeLength(Format$(miBatchSequence, "####"), 4, "l")
      End With
      moStreamWriter.WriteLine(sOutRecord)
      bWork = True
    Catch exExc As Exception
      bWork = False
      mbMarkBatch = False
      moFileInfo = Nothing
    Finally
    End Try
    Return bWork
  End Function

  Private Function CloseOutputFile() As Boolean
    Dim bWork As Boolean = False
    Dim sOutRecord As String

    Try
      miDetailRecordCount += 1
      miTotalRecordCount += miDetailRecordCount
      mdTotal += mdBatchTotal
      UpdateStatus("Batch Records: " & Format$(miDetailRecordCount, "#,##0"))
      UpdateStatus("Batch Total: " & Format$((mdBatchTotal * -1), "#,###.#0"))
      UpdateStatus("Close Upload File: " & msOutputPathFileName)
      sOutRecord = msDetailHeader1 & Format$(mdtBatchDate, "MMddyy") & msDetailHeader2
      sOutRecord = sOutRecord & FormatDebitCredit(msCreditAccountNumber, msCreditSubAccountNumber, mdBatchTotal)
      moStreamWriter.WriteLine(sOutRecord)
      sOutRecord = moGLEntity.TrailerID(miGLEntityIDSelected) & Format$(miDetailRecordCount, "0##") & Format$(miDetailRecordCount, "0##")
      moStreamWriter.WriteLine(sOutRecord)
      moStreamWriter.Close()
      moFileStream.Close()
      mdBatchTotal = 0
      miDetailRecordCount = 1
      moFileInfo.CopyTo(msExportPath & msExportFileName & "." & SetString(miBatchSequence), True)
      bWork = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      bWork = False
      mbMarkBatch = False
    Finally
    End Try
    Return bWork
  End Function

  Private Sub WriteBatch()
    Dim sOutRecord As String
    Dim oDBC As OleDbConnection = New OleDb.OleDbConnection
    Dim oCmd As OleDbCommand = New OleDb.OleDbCommand
    Dim oRdr As OleDbDataReader

    UpdateStatus(" ")
    UpdateStatus("Upload Started")
    miBatchSequence -= 1
    Try
      'moFileInfo = New IO.FileInfo(gsExportBackupPath & msExportFileName & "_" & miYearSelected & miMonthSelected & "." & SetString(miBatchSequence))
      'If moFileInfo.Exists Then
      '  If YNQuestion("File " & moFileInfo.FullName & " already exists." & vbCrLf & "Do you want to over write this file?", Me.Text) <> MsgBoxResult.Yes Then
      '    mbMarkBatch = False
      '    OKMessage(Me.Text, "Processing Canceled!", 1)
      '    UpdateStatus("Processing Canceled!")
      '    moFileInfo = Nothing
      '    Exit Sub
      '  End If
      'End If

      'init output stream
      miDetailRecordCount = 0
      mdBatchTotal = 0
      msDetailHeader1 = moGLEntity.DetailHeader1(miGLEntityIDSelected)
      msDetailHeader2 = moGLEntity.DetailHeader2(miGLEntityIDSelected)
      msCreditAccountNumber = moGLEntity.CreditAccountNumber(miGLEntityIDSelected)
      msCreditSubAccountNumber = moGLEntity.CreditSubAccountNumber(miGLEntityIDSelected)

      'open output file and write batch header
      'If Not OpenOutputFile() Then
      '  ErrorProcessing("Error opening the batch output file.")
      '  Exit Sub
      'End If

      'detail records
      If OpenDBMessage(oDBC) <> 0 Then Exit Sub
      oCmd.CommandText = "sGLTransactions"
      oCmd.CommandTimeout = giCommandTimeoutLong
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@EntityID", OleDbType.Numeric).Value = giEntityID
      oCmd.Parameters.Add("@GLEntityID", OleDbType.Numeric).Value = miGLEntityIDSelected
      oCmd.Parameters.Add("@TransactionYear", OleDbType.Numeric).Value = miYearSelected
      oCmd.Parameters.Add("@TransactionMonth", OleDbType.Numeric).Value = miMonthSelected
      oCmd.Parameters.Add("@CreditAccountNumber", OleDbType.VarChar, 10).Value = msCreditAccountNumber
      oCmd.Parameters.Add("@CreditSubAccountNumber", OleDbType.VarChar, 5).Value = msCreditSubAccountNumber
      oCmd.Connection = oDBC
      oRdr = oCmd.ExecuteReader()
      If oRdr.HasRows Then
        moFileInfo = New IO.FileInfo(gsExportBackupPath & msExportFileName & "_" & miYearSelected & miMonthSelected & "." & SetString(miBatchSequence))
        If moFileInfo.Exists Then
          If YNQuestion("File " & moFileInfo.FullName & " already exists." & vbCrLf & "Do you want to over write this file?", Me.Text) <> MsgBoxResult.Yes Then
            mbMarkBatch = False
            OKMessage(Me.Text, "Processing Canceled!", 1)
            UpdateStatus("Processing Canceled!")
            moFileInfo = Nothing
            Exit Sub
          End If
        End If
        If Not OpenOutputFile() Then
          ErrorProcessing("Error opening the batch output file.")
          Exit Sub
        End If
        Do While oRdr.Read
          sOutRecord = msDetailHeader1 & Format$(mdtBatchDate, "MMddyy") & msDetailHeader2
          mdAmount = SetDouble(oRdr!Amount)
          sOutRecord = sOutRecord & FormatDebitCredit(SetString(oRdr!AccountNumber), SetString(oRdr!SubAccountNumber), mdAmount)
          miDetailRecordCount += 1
          If miDetailRecordCount = 96 Then
            miDetailRecordCount -= 1
            If Not CloseOutputFile() Then
              ErrorProcessing("WriteBatch.CloseOutputFile: " & msErrorMessage)
              Exit Sub
            End If
            If Not OpenOutputFile() Then
              ErrorProcessing("Error opening the batch output file.")
              Exit Sub
            End If
          End If
          ' miDetailRecordCount += 1
          moStreamWriter.WriteLine(sOutRecord)
          mdBatchTotal += (SetDouble(Format$(mdAmount, "#.##")) * -1)
        Loop
        If Not CloseOutputFile() Then
          ErrorProcessing("WriteBatch.CloseOutputFile: " & msErrorMessage)
          Exit Sub
        End If
      Else
        ErrorProcessing("Error: No data found.")
        Exit Sub
      End If
      FileClose()
    Catch exExc As Exception
      mbMarkBatch = False
      ErrorProcessing("Error: " & exExc.Message)
      Exit Sub
    End Try
  End Sub

  Private Function RemoveDecimal(ByVal InNumber As String) As String
    Dim iWork As Int32

    iWork = InStr(1, InNumber, ".")
    If iWork > 1 Then
      If iWork < Len(InNumber) Then
        RemoveDecimal = Mid$(InNumber, 1, iWork - 1) & Mid$(InNumber, iWork + 1, Len(InNumber))
        Exit Function
      End If
    End If
    If iWork <= 0 Then
      RemoveDecimal = InNumber
    Else
      If iWork = 1 Then
        If Len(InNumber) <= 1 Then
          RemoveDecimal = vbNullString
          Exit Function
        End If
      End If
      If iWork = 1 Then
        RemoveDecimal = Mid$(InNumber, 2, Len(InNumber))
        Exit Function
      End If
      RemoveDecimal = Mid$(InNumber, 1, iWork - 1)
    End If
  End Function

  Private Function FormatDebitCredit(ByVal AccountNumber As String, ByVal SubAccountNumber As String, ByVal Amount As Double) As String
    Dim sAmount, sTrailer As String
    Dim iWork, iWork1 As Int32

    If Amount >= 0 Then
      FormatDebitCredit = AccountNumber & SubAccountNumber
      sAmount = Format$(Amount, "0######.##")
      sTrailer = "#" & Space$(14) & "#"
    Else
      FormatDebitCredit = "#" & Space$(14) & "#" & Space$(8) & AccountNumber & SubAccountNumber
      sAmount = Format$(Amount * -1, "0######.##")
      sTrailer = vbNullString
    End If
    iWork = InStr(1, sAmount, ".")
    If iWork > 0 Then
      iWork1 = Len(Mid$(sAmount, iWork, Len(sAmount)))
      Select Case iWork1
        Case 1
          sAmount = sAmount & "00"
        Case 2
          sAmount = sAmount & "0"
      End Select
    Else
      sAmount = sAmount & ".00"
    End If
    Return FormatDebitCredit & RemoveDecimal(sAmount) & sTrailer
  End Function

  Private Sub UpdateStatus(ByVal Status As String)
    lbStatus.Items.Add(Status)
    lbStatus.SelectedIndex = lbStatus.Items.Count - 1
    lbStatus.ClearSelected()
  End Sub
#End Region

#Region "Buttons"
  Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
    Dim oSubmitted As New Submitted
    Dim iStartingBatchSequence As Int32
    Dim sExportDateTime, sWork As String

    moSettings.WaitCursor(Me)
    sExportDateTime = Format$(Now, "MM/dd/yyyy HH:mm:ss")
    cbGLEntity.Enabled = False
    cbMonth.Enabled = False
    cbYear.Enabled = False
    btnOk.Enabled = False
    btnCancel.Enabled = False

    sWork = oSubmitted.Submitted(miGLEntityIDSelected, miYearSelected, miMonthSelected)
    If sWork <> vbNullString Then
      If IsDate(sWork) Then
        ErrorProcessing("Upload for " & miMonthSelected & "/" & miYearSelected & " was submitted on " & sWork & ".")
        Exit Sub
      Else
        ErrorProcessing(sWork)
        Exit Sub
      End If
    End If
    msGLEntityCode = moGLEntity.GLEntityCode(miGLEntityIDSelected)
    msExportPath = AddBackslash(moGLEntity.ExportPath(miGLEntityIDSelected))

    'test
    'msExportPath = "C:\GLBridge\"
    'test 

    CreateDirectory(msExportPath)
    msExportFileName = moGLEntity.ExportFileName(miGLEntityIDSelected)
    msExportFileExtension = moGLEntity.ExportFileExtension(miGLEntityIDSelected)
    If Mid$(msExportFileExtension, 1, 1) <> "." Then msExportFileExtension = "." & msExportFileExtension
    mdtBatchDate = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, CDate(miMonthSelected & "/1/" & miYearSelected)))
    miBatchSequence = moGLEntity.BatchSequence(miGLEntityIDSelected)
    iStartingBatchSequence = miBatchSequence
    WriteBatch()
    If mbMarkBatch Then
      UpdateStatus(" ")
      UpdateStatus("Total Records: " & Format$(miTotalRecordCount, "#,##0"))
      UpdateStatus("Grand Total: " & Format$((mdTotal * -1), "#,###.#0"))
      UpdateStatus("Marking batch as submitted.")
      With oSubmitted
        .GLEntityID = miGLEntityIDSelected
        .ExportYear = miYearSelected
        .ExportMonth = miMonthSelected
        .ExportDateTime = sExportDateTime
        .ExportPath = msExportPath
        .ExportBackupPath = gsExportBackupPath
        .ExportFileName = msExportFileName
        .StartingBatchSequence = iStartingBatchSequence
        .NumberOfFiles = (miBatchSequence - iStartingBatchSequence) + 1
        .NumberOfTransactions = miTotalRecordCount
        .TotalBatchAmount = mdTotal * -1
        If .Insert Then
          UpdateStatus("Batch has been marked.")
          UpdateStatus(" ")
          UpdateStatus("Processing Complete!")
          OKMessage(Me.Text, "Processing Complete!", 1)
        Else
          ErrorProcessing("Error marking batch.")
        End If
      End With
    End If
    moSettings.DefaultCursor(Me)
    btnCancel.Text = "&Close"
    btnCancel.Enabled = True
  End Sub

  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    Me.Close()
  End Sub
#End Region

#Region "ComboBoxes"
  Private Sub cbGLEntity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbGLEntity.SelectedIndexChanged
    If mbIsLoading Then Exit Sub
    miGLEntityIDSelected = SetInt32(cbGLEntity.SelectedValue)
    SetButtons()
  End Sub

  Private Sub cbMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbMonth.SelectedIndexChanged
    If mbIsLoading Then Exit Sub
    miMonthSelected = SetInt32(cbMonth.SelectedValue)
    SetButtons()
  End Sub

  Private Sub cbYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbYear.SelectedIndexChanged
    If mbIsLoading Then Exit Sub
    miYearSelected = SetInt32(cbYear.SelectedValue)
    SetButtons()
  End Sub
#End Region
End Class