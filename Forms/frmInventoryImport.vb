﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
#End Region

Public Class frmInventoryImport
#Region "Declarations"
  Private miImportRecordCount As Int32
  Private miFileLength, miBytesRead As Long
  Private miTransactionMonth, miTransactionYear As Int32
  Private mbIsLoading, mbErrorFlag As Boolean
  Private moSettings As New Settings
  Private moFileStream As FileStream
  Private moStreamReader As StreamReader
  Private msImportFileName As String
#End Region

#Region "Form Events"
  Private Sub frmInventoryImport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Exit Sub
  End Sub
#End Region

#Region "Routines"
  Public Sub New()
    InitializeComponent()
  End Sub

  Public Function Component() As Boolean
    Dim oPopulate As New Populate
    Dim iMonthCalc, iYear As Int32

    mbIsLoading = True
    moSettings.FormStartPosition(Me)
    pnlButtons.Left = CenterControl(Me.Width, pnlButtons.Width)
    lblRecordsRead.Text = vbNullString
    lblBytesRead.Text = vbNullString
    UpdateStatus("Waiting to start the import.")
    oPopulate.MonthsComboBox(cbMonth, False)
    oPopulate.YearsComboBox(cbYear, False)
    iMonthCalc = SetInt32(Month(Now) - 2)
    iYear = Year(Now)
    If iMonthCalc < 0 Then
      iMonthCalc = 11
      iYear = iYear - 1
    End If
    cbMonth.SelectedIndex = iMonthCalc
    miTransactionMonth = SetInt32(cbMonth.SelectedValue)
    SetComboBox(cbYear, iYear)
    miTransactionYear = SetInt32(cbYear.SelectedValue)
    mbIsLoading = False
    Return True
  End Function

  Private Sub UpdateStatus(ByVal Status As String)
    lbStatus.Items.Add(Status)
    lbStatus.SelectedIndex = lbStatus.Items.Count - 1
    lbStatus.ClearSelected()
  End Sub

  Private Sub UpdateRead()
    lblRecordsRead.Text = "Records Read: " & Format$(miImportRecordCount, "#,##0")
    lblBytesRead.Text = "Processed: " & Format$(miBytesRead \ 1000, "#,##0k") & " of " & Format$(miFileLength \ 1000, "#,##0k")
    If Not miImportRecordCount Mod 500 = 0 Then
      lblRecordsRead.Refresh()
      lblBytesRead.Refresh()
    Else
      Me.Refresh()
      Application.DoEvents()
    End If
  End Sub

  Private Sub StatusAndClose(ByVal Line01 As String, ByVal Line02 As String, ByVal CloseFiles As Boolean)
    Dim sWork As String

    sWork = Line01
    If Trim$(Line02) <> vbNullString Then sWork = sWork & vbCrLf & Line02
    moSettings.DefaultCursor(Me)
    OKMessage(Me.Text, sWork, 3)
    UpdateStatus(Line01)
    If Trim$(Line02) <> vbNullString Then UpdateStatus(Line02)
    If CloseFiles Then
      moStreamReader.Close()
      moFileStream.Close()
    End If
    btnCancel.Text = "&Close"
    btnCancel.Enabled = True
  End Sub

  Private Sub PopulateStockTransactions()
    Dim sTransactionMonth, sInRecord, sFullAccountNumber, sDigit5 As String
    Dim sGLEntityCode, sGLEntityCodeHold As String
    Dim iGLEntityID, iWriteCount, iGLEntityWriteCount As Int32
    Dim oFileInfo As FileInfo
    Dim oStockTransactions As New StockTransactions
    Dim oGLEntity As New GLEntity
    Dim oInterfaceHistory As New InterfaceHistory
    Dim bWriteSkipped As Boolean
    Dim bProcessGLEntity As Boolean = False
    Dim bUnwrittenInterfaceHistoryDetail As Boolean = False

    ' moSettings.WaitCursor(Me)
    btnOk.Enabled = False
    btnCancel.Enabled = False
    sTransactionMonth = SetString(miTransactionMonth)
    If Len(sTransactionMonth) = 1 Then sTransactionMonth = "0" & sTransactionMonth
    msImportFileName = gsImportFileName & miTransactionYear & sTransactionMonth & "." & gsImportFileExtension

    'ini import file
    miBytesRead = 0
    miImportRecordCount = 0
    iWriteCount = 0
    iGLEntityWriteCount = 0
    UpdateStatus("Import Started.")
    UpdateStatus("Mark Import Start.")

    If Not oStockTransactions.StartImport(miTransactionYear, miTransactionMonth) Or _
    Not oInterfaceHistory.InsertInterfaceHeader(miTransactionYear, miTransactionMonth, gsImportPath, msImportFileName) Then
      moSettings.DefaultCursor(Me)
      OKMessage("Mark Import Start", "Unable to mark the start of the import." & vbCrLf & "Processing Canceled!", 3)
      UpdateStatus("Unable to mark the start of the import.")
      UpdateStatus("Processing Canceled!")
      btnCancel.Enabled = True
      mbErrorFlag = True
      Exit Sub
    End If
    UpdateStatus("Check Import File.")
    oFileInfo = New IO.FileInfo(gsImportPath & msImportFileName)
    If oFileInfo.Exists Then
      UpdateStatus("Import file is set to: " & gsImportPath & msImportFileName & ".")
    Else
      moSettings.DefaultCursor(Me)
      OKMessage(Me.Text, "Can't locate import file: " & gsImportPath & msImportFileName & "." & vbCrLf & "Processing Canceled!", 3)
      UpdateStatus("Can't locate import file: " & gsImportPath & msImportFileName & ".")
      UpdateStatus("Processing Canceled!")
      btnCancel.Enabled = True
      mbErrorFlag = True
      Exit Sub
    End If
    miFileLength = oFileInfo.Length
    UpdateStatus("Checking import file length.")
    If miFileLength > 0 Then
      UpdateStatus(gsImportPath & msImportFileName & " file length is " & Format$(miFileLength, "#,##0") & ".")
    Else
      moSettings.DefaultCursor(Me)
      OKMessage(Me.Text, "Error: " & gsImportPath & msImportFileName & " has an invalid file length of " & miFileLength & "." & vbCrLf & "Processing Canceled!", 3)
      UpdateStatus("Error: " & gsImportPath & msImportFileName & " has an invalid file length of " & Format$(miFileLength, "#,##0") & ".")
      UpdateStatus("Processing Canceled!")
      btnCancel.Enabled = True
      mbErrorFlag = True
      Exit Sub
    End If

    'open import file
    moFileStream = New FileStream(gsImportPath & msImportFileName, FileMode.Open, FileAccess.Read)
    moStreamReader = New StreamReader(moFileStream)
    moStreamReader.BaseStream.Seek(0, SeekOrigin.Begin)

    ' Delete DB transactions for import month
    UpdateStatus("Remove transactions for " & MonthName(miTransactionMonth) & " " & miTransactionYear & ".")
    If Not oStockTransactions.DeleteMonthlyStockTransactions(miTransactionYear, miTransactionMonth) Then
      StatusAndClose("Unable to remove current entries.", "Processing Canceled!", True)
      mbErrorFlag = True
      Exit Sub
    End If

    ' Process import file
    UpdateStatus("Import Stock Transactions")
    sGLEntityCodeHold = vbNullString
    While moStreamReader.Peek() > -1
      bWriteSkipped = True
      sInRecord = SetString(moStreamReader.ReadLine)
      miImportRecordCount += 1
      miBytesRead += Len(sInRecord)
      UpdateRead()
      If Len(sInRecord) <= 3 Then GoTo Skip01
      If LCase$(Mid$(sInRecord, 1, 3)) <> "zjd" Then GoTo skip01
      sFullAccountNumber = GetField(sInRecord, 4)
      sDigit5 = Mid$(sFullAccountNumber, 5, 1)
      If Len(sFullAccountNumber) <> 16 Then GoTo Skip01
      Select Case sDigit5
        Case "1", "2", "6"
          Mid$(sFullAccountNumber, 4, 1) = sDigit5
        Case "7", "8", "9"
          Mid$(sFullAccountNumber, 4, 1) = "6"
        Case Else
          GoTo Skip01
      End Select
      bWriteSkipped = False
      oStockTransactions = New StockTransactions
      With oStockTransactions
        sGLEntityCode = Trim$(Mid$(sFullAccountNumber, 1, 3))
        If sGLEntityCode <> sGLEntityCodeHold Or iGLEntityID <= 0 Then
          If bProcessGLEntity Then
            If Not oInterfaceHistory.InsertInterfaceDetail(iGLEntityID, iGLEntityWriteCount) Then
              StatusAndClose("Write G/L Entity Count", "Error writing the G/L entity count." & vbCrLf & "Processing Canceled!", True)
              mbErrorFlag = True
              Exit Sub
            End If
            iGLEntityWriteCount = 0
            bUnwrittenInterfaceHistoryDetail = False
          End If
          iGLEntityID = oGLEntity.GetGLEntityID(sGLEntityCode)
          sGLEntityCodeHold = sGLEntityCode
          bProcessGLEntity = True
        End If
        .GLEntityID = iGLEntityID
        .TransactionYear = miTransactionYear
        .TransactionMonth = miTransactionMonth
        .AccountNumber = Mid$(sFullAccountNumber, 1, 10)
        .SubAccountNumber = Mid$(sFullAccountNumber, 12, 5)
        .ItemNumber = GetSubField(GetField(sInRecord, 10), 1)
        .ItemDescription = GetField(sInRecord, 5)
        If LCase$(GetSubField(GetField(sInRecord, 10), 2)) = "y" Then
          .Chargeable = True
        Else
          .Chargeable = False
        End If
        .UnitOfMeasure = GetSubField(GetField(sInRecord, 9), 1)
        .Quantity = SetDouble(GetSubField(GetField(sInRecord, 7), 1))
        .Amount = SetDouble(GetField(sInRecord, 6))
        If Not .InsertRecord Then
          StatusAndClose("Add Stock Transaction", "Error inserting stock transaction." & vbCrLf & "Processing Canceled!", True)
          mbErrorFlag = True
          Exit Sub
        End If
        iWriteCount += 1
        iGLEntityWriteCount += 1
        bUnwrittenInterfaceHistoryDetail = True
      End With
Skip01:
      If bWriteSkipped Then
        If LCase$(Mid$(sInRecord, 1, 4)) <> "msh|" Then
          If LCase$(Mid$(sInRecord, 1, 4)) <> "zje|" Then
            If Not oStockTransactions.WriteSkippedRecord(oInterfaceHistory.InterfacehistoryHeaderID, sInRecord, miImportRecordCount) Then
              StatusAndClose("Write Skipped Transaction", "Error writing a skipped transaction." & vbCrLf & "Processing Canceled!", True)
              mbErrorFlag = True
              Exit Sub
            End If
          End If
        End If
      End If
    End While

    miBytesRead = miFileLength
    UpdateRead()
    If bProcessGLEntity AndAlso bUnwrittenInterfaceHistoryDetail Then
      If Not oInterfaceHistory.InsertInterfaceDetail(iGLEntityID, iGLEntityWriteCount) Then
        StatusAndClose("Write G/L Entity Count", "Error writing the G/L entity count." & vbCrLf & "Processing Canceled!", True)
        mbErrorFlag = True
        Exit Sub
      End If
      iGLEntityWriteCount = 0
      bUnwrittenInterfaceHistoryDetail = False
    End If
    UpdateStatus("Mark Import End.")
    If Not oStockTransactions.EndImport(miTransactionYear, miTransactionMonth) Or _
    oInterfaceHistory.UpdateInterfaceHeader(miImportRecordCount, iWriteCount, oStockTransactions.SkippedRecordsCount(oInterfaceHistory.InterfacehistoryHeaderID)) Then
      moSettings.DefaultCursor(Me)
      OKMessage("Mark Import End", "Unable to mark the end of the import." & vbCrLf & "Processing Canceled!", 3)
      UpdateStatus("Unable to mark the start of the import.")
      mbErrorFlag = True
    End If
    UpdateStatus("Import Processing Complete!")
    moStreamReader.Close()
    moFileStream.Close()
    moSettings.DefaultCursor(Me)
    Me.Refresh()
  End Sub

  Private Function GetField(ByVal Record As String, ByVal FieldPosition As Int32) As String
    Dim sRecord, sField As String
    Dim iWork, iWork1 As Int32

    sField = vbNullString
    sRecord = Record
    If Len(sRecord) <= 0 Then Return vbNullString
    For iWork = 0 To FieldPosition - 1
      iWork1 = InStr(1, sRecord, "|")
      If iWork1 = 0 AndAlso iWork <> FieldPosition - 1 Then Return vbNullString
      If iWork1 = 0 AndAlso iWork = FieldPosition - 1 Then Return Trim$(sField)
      If iWork1 = Len(sRecord) Then
        sRecord = vbNullString
        sField = vbNullString
      ElseIf iWork1 = 1 Then
        sField = vbNullString
        sRecord = Trim$(Mid$(sRecord, iWork1 + 1, Len(sRecord)))
      Else
        sField = Trim$(Mid$(sRecord, 1, iWork1 - 1))
        sRecord = Trim$(Mid$(sRecord, iWork1 + 1, Len(sRecord)))
      End If
    Next iWork
    Return sField
  End Function

  Private Function GetSubField(ByVal CombinedField As String, ByVal SubFieldPosition As Int32) As String
    Dim sCombinedField, sField As String
    Dim iWork, iWork1 As Int32

    sField = vbNullString
    sCombinedField = CombinedField
    If Len(sCombinedField) <= 0 Then Return vbNullString
    For iWork = 0 To SubFieldPosition - 1
      iWork1 = InStr(1, sCombinedField, "^")
      If iWork1 = 0 AndAlso iWork <> SubFieldPosition - 1 Then Return vbNullString
      If iWork1 = 0 AndAlso iWork = SubFieldPosition - 1 Then Return Trim$(sCombinedField)
      If iWork1 = Len(sCombinedField) Then
        sCombinedField = vbNullString
        sField = vbNullString
      ElseIf iWork1 = 1 Then
        sField = vbNullString
        sCombinedField = Trim$(Mid$(sCombinedField, iWork1 + 1, Len(sCombinedField)))
      Else
        sField = Trim$(Mid$(sCombinedField, 1, iWork1 - 1))
        sCombinedField = Trim$(Mid$(sCombinedField, iWork1 + 1, Len(sCombinedField)))
      End If
    Next iWork
    Return sField
  End Function

  Private Sub CrossReference()
    Dim sTransactionMonth As String
    Dim oCrossReference As New CrossReference
    Dim oGLEntity As New GLEntity
    Dim iWork, iGLEntityID150 As Int32

    '  moSettings.WaitCursor(Me)
    btnOk.Enabled = False
    btnCancel.Enabled = False
    sTransactionMonth = SetString(miTransactionMonth)
    If Len(sTransactionMonth) = 1 Then sTransactionMonth = "0" & sTransactionMonth

    ' init processing
    UpdateStatus(" ")
    UpdateStatus("Cross Reference Started.")
    oCrossReference.TransactionYear = miTransactionYear
    oCrossReference.TransactionMonth = miTransactionMonth

    iGLEntityID150 = oGLEntity.GetGLEntityID("150")
    If iGLEntityID150 > 0 Then
      UpdateStatus("150 G/L Entity ID is set to " & iGLEntityID150)
      oCrossReference.GLEntityID = iGLEntityID150
    Else
      moSettings.DefaultCursor(Me)
      OKMessage("Get 150 G/L Entity ID", "Unable to get G/L Entity ID for 150." & vbCrLf & "Processing Canceled!", 3)
      UpdateStatus("Unable to get G/L Entity ID for 150.")
      UpdateStatus("Processing Canceled!")
      btnCancel.Enabled = True
      mbErrorFlag = True
      Exit Sub
    End If

    iWork = oCrossReference.TransactionCount()
    If iWork > 0 Then
      UpdateStatus("Transactions Found: " & Format$(iWork, "#,##0"))
    Else
      moSettings.DefaultCursor(Me)
      OKMessage("Get Transaction Count", "No transaction were found for " & miTransactionMonth & "/" & miTransactionYear & "." & vbCrLf & "Processing Canceled!", 3)
      UpdateStatus("No transaction were found for " & miTransactionMonth & "/" & miTransactionYear & ".")
      UpdateStatus("Processing Canceled!")
      btnCancel.Enabled = True
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("Mark Cross Reference Start")
    If Not oCrossReference.StartCrossReference Then
      moSettings.DefaultCursor(Me)
      OKMessage("Mark Cross Reference Start", "Unable to mark the start of the cross reference." & vbCrLf & "Processing Canceled!", 3)
      UpdateStatus("Unable to mark the start of the cross reference.")
      UpdateStatus("Processing Canceled!")
      btnCancel.Enabled = True
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("Cross Reference:")
    UpdateStatus("   Step 1: Reset 150 Account Numbers")
    If oCrossReference.Reset("150") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests")
    Else
      StatusAndClose("Unable to reset account numbers.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 2: 15011327xx Accounts")
    If oCrossReference.Changexxx11327xx("150") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 15011327xx accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 3: 150xxxxxxx-37600 Accounts")
    If oCrossReference.Changexxxxxxxxxx37600("150") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 37600 accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 4: 150662600x-47200 and 150670700x-47200 Accounts")
    If oCrossReference.Changexxxxxxxxxx47200("150") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 47200 accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 5: 1506625100 Accounts")
    If oCrossReference.Changexxx662510035200("150") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 1506625100-35200 accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 6: 1506xxxxx2-3500 Accounts")
    If oCrossReference.Changexxx6xxxxx235200("150") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 1506xxxxx2-35200 accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 7: 1506xxxxx2-47200 Accounts")
    If oCrossReference.Changexxx6xxxxx247200("150") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 1506xxxxx2-47200 accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 8: 1506xxxxx3-35200 Accounts")
    If oCrossReference.Changexxx6xxxxx335200("150") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 1506xxxxx3-35200 accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 9: 1506xxxxx3-47200 Accounts")
    If oCrossReference.Changexxx6xxxxx347200("150") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 1506xxxxx3-47200 accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 10: Reset 120 Account Numbers")
    If oCrossReference.Reset("120") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests")
    Else
      StatusAndClose("Unable to reset account numbers.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 11: 12011327xx Accounts")
    If oCrossReference.Changexxx11327xx("120") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 15011327xx accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 12: 120xxxxxxx-37600 Accounts")
    If oCrossReference.Changexxxxxxxxxx37600("120") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 37600 accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 13: 120662600x-47200 and 120670700x-47200 Accounts")
    If oCrossReference.Changexxxxxxxxxx47200("120") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 47200 accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    UpdateStatus("   Step 14: 1206625100 Accounts")
    If oCrossReference.Changexxx662510035200("120") Then
      UpdateStatus("      Processed " & Format$(oCrossReference.RowCount, "#,##0") & " requests.")
    Else
      StatusAndClose("Unable to change 1506625100-35200 accounts.", "Processing Canceled!", False)
      mbErrorFlag = True
      Exit Sub
    End If

    'end processing
    UpdateStatus("Mark Cross Reference End.")
    If Not oCrossReference.EndCrossReference Then
      moSettings.DefaultCursor(Me)
      OKMessage("Mark Cross Reference End", "Unable to mark the start of the cross reference." & vbCrLf & "Processing Canceled!", 3)
      UpdateStatus("Unable to mark the end of the cross reference.")
      mbErrorFlag = True
      Exit Sub
    End If
    UpdateStatus("Cross Reference Processing Complete!")
    Me.Refresh()
  End Sub

  Private Sub MoveInputFile()
    Dim oFileInfo, oDeleteFileInfo As FileInfo

    UpdateStatus(" ")
    UpdateStatus("Move Input File")
    UpdateStatus("Move Location: " & gsProcessedPath)
    oFileInfo = New IO.FileInfo(gsImportPath & msImportFileName)
    If Not oFileInfo.Exists Then
      UpdateStatus("File " & msImportFileName & " was not found.")
      UpdateStatus("Move Canceled!")
      mbErrorFlag = False
      Exit Sub
    End If
    oDeleteFileInfo = New IO.FileInfo(gsProcessedPath & msImportFileName)
    If oDeleteFileInfo.Exists Then
      UpdateStatus("Move file exists and will be removed.")
      oDeleteFileInfo.Delete()
    End If
    oDeleteFileInfo = Nothing
    oFileInfo.CopyTo(gsProcessedPath & msImportFileName, True)
    oFileInfo.Delete()
    oFileInfo = Nothing
    UpdateStatus("Move File Processing Complete!")
    Me.Refresh()
  End Sub
#End Region

#Region "Buttons"
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    Me.Close()
  End Sub

  Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
    moSettings.WaitCursor(Me)
    mbErrorFlag = False

    PopulateStockTransactions()
    If mbErrorFlag Then
      moSettings.DefaultCursor(Me)
      Me.Refresh()
      Exit Sub
    End If

    CrossReference()
    If mbErrorFlag Then
      moSettings.DefaultCursor(Me)
      Me.Refresh()
      Exit Sub
    End If

    MoveInputFile()
    If mbErrorFlag Then
      moSettings.DefaultCursor(Me)
      Me.Refresh()
      Exit Sub
    End If

    UpdateStatus(" ")
    UpdateStatus("Processing Complete!")
    Me.Refresh()
    moSettings.DefaultCursor(Me)
    OKMessage(Me.Text, "Processing Complete!", 1)
    btnCancel.Text = "&Close"
    btnCancel.Enabled = True
    Me.Refresh()
  End Sub
#End Region

#Region "Comboboxes"
  Private Sub cbMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbMonth.SelectedIndexChanged
    If mbIsLoading Then Exit Sub
    miTransactionMonth = SetInt32(cbMonth.SelectedValue)
  End Sub

  Private Sub cbYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbYear.SelectedIndexChanged
    If mbIsLoading Then Exit Sub
    miTransactionYear = SetInt32(cbYear.SelectedValue)
  End Sub
#End Region
End Class