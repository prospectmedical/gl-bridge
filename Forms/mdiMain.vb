#Region "Init"
'Option Strict On
Option Explicit On
#End Region

Public Class mdiMain
#Region "Declarations"
  Private moSettings As New Settings
  Private miBlinkCount As Int32
  Private miBlinkInterval As Int32 = 1000
  Private mbBlinkVisible As Boolean
  Private msCRLF As String = "<br />"
#End Region

#Region "Form Events"
  Private Sub mdiMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Exit Sub
  End Sub

  Private Sub mdiMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    CloseApp()
  End Sub
#End Region

#Region "Routines"
  Public Function Component() As Int32
    Me.WindowState = FormWindowState.Normal
    Me.Text = gsProductName
    Me.mnuAbout.Text = "About " & gsProductName & "..."
    tsslPicture.Visible = False
    tsslProcessing.Visible = False
    ViewEmail(False)
    mnuMaintenance.Enabled = gbAllowMaintenance
    mnuMaintenance.Visible = gbAllowMaintenance
    mnuReports.Enabled = gbAllowReports
    mnuReports.Visible = gbAllowReports
    mnuMonthlyProcessing.Enabled = gbAllowImport Or gbAllowUpload Or gbAllowEmail
    mnuMonthlyProcessing.Visible = gbAllowImport Or gbAllowUpload Or gbAllowEmail
    mnuImportInventoryTransactions.Enabled = gbAllowImport
    mnuImportInventoryTransactions.Visible = gbAllowImport
    mnuLine01.Visible = gbAllowImport
    mnuUploadGLBatch.Enabled = gbAllowUpload
    mnuUploadGLBatch.Visible = gbAllowUpload
    mnuLine02.Visible = gbAllowUpload
    mnuGLTransactionDetailEmail.Enabled = gbAllowEmail
    mnuGLTransactionDetailEmail.Visible = gbAllowEmail
    mnuLine03.Visible = gbAllowEmail
    If Not gbAllowMaintenance AndAlso Not gbAllowImport AndAlso Not gbAllowReports AndAlso Not gbAllowUpload AndAlso Not gbAllowEmail Then
      tsslPicture.Text = vbNullString
      tsslPicture.Visible = True
      tsslProcessing.Visible = True
      tsslProcessing.ForeColor = System.Drawing.SystemColors.ActiveCaption
      tsslProcessing.Text = "***** No valid menu options have been authorized! *****"
      'tsslProcessing.Font.Style.Bold = FontStyle.Bold
      tmrBlink.Enabled = True
      tmrBlink.Interval = miBlinkInterval
    Else
      tmrBlink.Enabled = False
    End If
    miBlinkCount = 0
    mbBlinkVisible = True
    Component = 0
  End Function

  Private Sub CloseApp()
    Application.Exit()
  End Sub

  Private Sub ViewEmail(ByVal ViewStatus As Boolean)
    tsslSendingEmail.Visible = ViewStatus
    tsslProcessingEMail.Text = vbNullString
    tsslProcessingEMail.Visible = ViewStatus
    ssMain.Refresh()
  End Sub

  Private Sub UpdateEmailMessage(ByVal Message As String)
    tsslProcessingEMail.Text = Message
    ssMain.Refresh()
  End Sub
#End Region

#Region "Menu Items"
#Region "File"
  Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
    CloseApp()
  End Sub
#End Region

#Region "Maintenance"
  Private Sub mnuDepartment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDepartment.Click
    Dim ofrmDepartmentMaintenance As New frmDepartmentMaintenance

    With ofrmDepartmentMaintenance
      If .Component Then
        .ShowDialog()
      Else
        OKMessage("Department Maintenance", "Error starting the department maintenance function" & vbCrLf & "Processing Canceled!", 3)
      End If
    End With
  End Sub

  Private Sub mnuGLEntityMaintenance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGLEntityMaintenance.Click
    Dim ofrmGLEntityMaintenance As New frmGLEntityMaintenance

    With ofrmGLEntityMaintenance
      If .Component Then
        .ShowDialog()
      Else
        OKMessage("G/L Entity Maintenance", "Error starting the G/L entity maintenance function" & vbCrLf & "Processing Canceled!", 3)
      End If
    End With
  End Sub
#End Region

#Region "Monthly Processing"
  Private Sub mnuImportInventoryTransactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportInventoryTransactions.Click
    Dim ofrmInventoryImport As New frmInventoryImport

    With ofrmInventoryImport
      If .Component Then
        .ShowDialog()
      Else
        OKMessage("Inventory Import", "Error starting the inventory import function" & vbCrLf & "Processing Canceled!", 3)
      End If
    End With
  End Sub

  Private Sub mnuUploadGLBatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUploadGLBatch.Click
    Dim ofrmGLBatch As New frmGLBatch

    With ofrmGLBatch
      If .Component("G/L Upload") Then
        .ShowDialog()
      Else
        OKMessage("G/L Batch", "Error starting the G/L batch function" & vbCrLf & "Processing Canceled!", 3)
      End If
    End With
  End Sub

  Private Sub mnuGLTransactionDetailEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGLTransactionDetailEmail.Click
    Dim iGLEntityID, iYearSelected, iMonthSelected, iWork As Int32
    Dim sPathFileName, sMessage As String
    Dim asDepartmentNumber As ArrayList = Nothing
    Dim asDepartmentName As ArrayList = Nothing
    Dim asReportEmailAddress As ArrayList = Nothing
    Dim ofrmSelection01 As New frmSelection01
    Dim ofrmReports As New frmReports
    Dim sReportName1 As String = "G/L Transaction Detail"
    Dim oGLTransactions As New GLTransactions
    Dim oGLEntity As New GLEntity
    Dim oPDFExport As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
    Dim oarGLTransactionDetail As arGLTransactionDetail
    Dim oEMailSMTP As EMail.SMTP

    ViewEmail(True)
    UpdateEmailMessage(sReportName1)
    ofrmSelection01.Component(sReportName1, True)
    ofrmSelection01.ShowDialog()
    If ofrmSelection01.Canceled Then
      ViewEmail(False)
      Exit Sub
    End If
    iGLEntityID = ofrmSelection01.GLEntityIDSelected
    iYearSelected = ofrmSelection01.YearSelected
    iMonthSelected = ofrmSelection01.MonthSelected
    If Not oGLTransactions.DetailEmailArray(iGLEntityID, asDepartmentNumber, asDepartmentName, asReportEmailAddress) Then
      OKMessage(sReportName1, "No department email addresses were found to send email!" & vbCrLf & "Processing canceled!", 2)
      ViewEmail(False)
      Exit Sub
    End If
    If asDepartmentNumber.Count <= 0 Then
      OKMessage(sReportName1, "No department email addresses were found to send email!" & vbCrLf & "Processing canceled!", 2)
      ViewEmail(False)
      Exit Sub
    End If
    Try
      For iWork = 0 To asDepartmentNumber.Count - 1
        UpdateEmailMessage(sReportName1 & "-[Sending Email " & iWork + 1 & " of " & asDepartmentNumber.Count & "]")
        sPathFileName = gsReportEmailWorkPath & "GL Transaction Detail " & asDepartmentNumber.Item(iWork).ToString & " " & MonthName(iMonthSelected) & " " & iYearSelected & ".pdf"
        System.IO.File.Delete(sPathFileName)
        oarGLTransactionDetail = New arGLTransactionDetail
        With oarGLTransactionDetail
          If Not .Component(sReportName1, iGLEntityID, iYearSelected, iMonthSelected, "All Email Addresses", asDepartmentNumber.Item(iWork).ToString & ": " & asDepartmentName.Item(iWork).ToString, True) Then
            OKMessage(sReportName1, "Error processing report." & vbCrLf & "Processing Canceled!", 3)
            Exit Sub
          End If
          .PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Landscape
          .Run()
          oPDFExport.Export(.Document, sPathFileName)
          sMessage = "Department: " & asDepartmentName.Item(iWork).ToString & " (" & asDepartmentNumber.Item(iWork).ToString & ")" & msCRLF & msCRLF
          sMessage = sMessage & "Attached are your inventory transactions for " & MonthName(iMonthSelected) & " " & iYearSelected & "." & msCRLF
          sMessage = sMessage & "If you have any problems/questions please contact the Accounting Department at 15-6200/(610) 447-6200." & msCRLF & msCRLF
          sMessage = sMessage & "Thank You," & msCRLF
          sMessage = sMessage & "Accounting Department" & msCRLF
          sMessage = sMessage & "Crozer-Keystone Health System" & msCRLF & msCRLF & msCRLF
          sMessage = sMessage & "***** Do not reply to this email, this mailbox is not monitored! *****" & msCRLF
          sMessage = sMessage & "***** Do not reply to this email, this mailbox is not monitored! *****" & msCRLF & msCRLF
          oEMailSMTP = New EMail.SMTP
          If Not oEMailSMTP.Recipient(asReportEmailAddress.Item(iWork).ToString) Then Throw New ApplicationException("Email recipient couldn't be set." & vbCrLf & oEMailSMTP.ErrorMessage)
          If Not oEMailSMTP.Sender(gsEmailSender) Then Throw New ApplicationException("Email sender couldn't be set." & vbCrLf & oEMailSMTP.ErrorMessage)
          If Not oEMailSMTP.Subject("GL Transaction Detail " & MonthName(iMonthSelected) & " " & iYearSelected) Then Throw New ApplicationException("Email subject couldn't be set." & vbCrLf & oEMailSMTP.ErrorMessage)
          If Not oEMailSMTP.Message(sMessage) Then Throw New ApplicationException("Email message couldn't be set." & vbCrLf & oEMailSMTP.ErrorMessage)
          If Not oEMailSMTP.AddAttachment(sPathFileName) Then Throw New ApplicationException("Add attachment failed." & vbCrLf & oEMailSMTP.ErrorMessage)
          If Not oEMailSMTP.Send Then Throw New ApplicationException("Error sending this email." & vbCrLf & oEMailSMTP.ErrorMessage)
          Application.DoEvents()
        End With
      Next iWork
    Catch exExc As Exception
      OKMessage(Me.Text, exExc.Message & vbCrLf & "Processing Canceled!", 3)
      ViewEmail(False)
      Exit Sub
    Finally
      oEMailSMTP = Nothing
    End Try
    If oGLTransactions.DetailEmailTrackingInsert(iGLEntityID, iYearSelected, iMonthSelected) Then
      OKMessage(Me.Text, "G/L Transaction Detail Email processing is complete!", 1)
    Else
      OKMessage(Me.Text, "Error updating transaction detail email tracking." & vbCrLf & "Processing Canceled!", 3)
    End If
    ViewEmail(False)
  End Sub
#End Region

#Region "Reports"
  Private Sub mnuGLTransactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGLTransactions.Click
    Dim ofrmSelection01 As New frmSelection01
    Dim ofrmReports As New frmReports
    Dim sReportName1 As String = "G/L Transactions"

    ofrmSelection01.Component(sReportName1, False)
    ofrmSelection01.ShowDialog()
    If ofrmSelection01.Canceled Then Exit Sub
    moSettings.WaitCursor(Me)
    If ofrmReports.Component(sReportName1, vbNullString) Then
      If ofrmReports.GLTransactions(ofrmSelection01.MonthSelected, ofrmSelection01.YearSelected, ofrmSelection01.GLEntityIDSelected) Then
        ofrmReports.MdiParent = Me
        moSettings.DefaultCursor(Me)
        ofrmReports.Show()
      Else
        moSettings.DefaultCursor(Me)
        OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
        Exit Sub
      End If
    Else
      moSettings.DefaultCursor(Me)
      OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
      Exit Sub
    End If
  End Sub

  Private Sub mnuDepartmentListByNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDepartmentListByNumber.Click
    Dim ofrmGLEntitySelection As New frmGLEntitySelection
    Dim ofrmReports As New frmReports
    Dim sReportName1 As String = "Department List By Number"

    ofrmGLEntitySelection.Component(sReportName1)
    ofrmGLEntitySelection.ShowDialog()
    If ofrmGLEntitySelection.Canceled Then Exit Sub
    moSettings.WaitCursor(Me)
    If ofrmReports.Component(sReportName1, vbNullString) Then
      If ofrmReports.DepartmentListByNumber(ofrmGLEntitySelection.GLEntityIDSelected) Then
        ofrmReports.MdiParent = Me
        moSettings.DefaultCursor(Me)
        ofrmReports.Show()
      Else
        moSettings.DefaultCursor(Me)
        OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
        Exit Sub
      End If
    Else
      moSettings.DefaultCursor(Me)
      OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
      Exit Sub
    End If
  End Sub

  Private Sub mnuDepartmentListByName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDepartmentListByName.Click
    Dim ofrmGLEntitySelection As New frmGLEntitySelection
    Dim ofrmReports As New frmReports
    Dim sReportName1 As String = "Department List By Name"

    ofrmGLEntitySelection.Component(sReportName1)
    ofrmGLEntitySelection.ShowDialog()
    If ofrmGLEntitySelection.Canceled Then Exit Sub
    moSettings.WaitCursor(Me)
    If ofrmReports.Component(sReportName1, vbNullString) Then
      If ofrmReports.DepartmentListByName(ofrmGLEntitySelection.GLEntityIDSelected) Then
        ofrmReports.MdiParent = Me
        moSettings.DefaultCursor(Me)
        ofrmReports.Show()
      Else
        moSettings.DefaultCursor(Me)
        OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
        Exit Sub
      End If
    Else
      moSettings.DefaultCursor(Me)
      OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
      Exit Sub
    End If
  End Sub

  Private Sub mnuMissingDepartments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMissingDepartments.Click
    Dim ofrmGLEntitySelection As New frmGLEntitySelection
    Dim ofrmReports As New frmReports
    Dim sReportName1 As String = "Missing Departments"

    ofrmGLEntitySelection.Component(sReportName1)
    ofrmGLEntitySelection.ShowDialog()
    If ofrmGLEntitySelection.Canceled Then Exit Sub
    moSettings.WaitCursor(Me)
    If ofrmReports.Component(sReportName1, vbNullString) Then
      If ofrmReports.DepartmentListMissing(ofrmGLEntitySelection.GLEntityIDSelected) Then
        ofrmReports.MdiParent = Me
        moSettings.DefaultCursor(Me)
        ofrmReports.Show()
      Else
        moSettings.DefaultCursor(Me)
        OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
        Exit Sub
      End If
    Else
      moSettings.DefaultCursor(Me)
      OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
      Exit Sub
    End If
  End Sub

  Private Sub mnuMissingEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMissingEmail.Click
    Dim ofrmGLEntitySelection As New frmGLEntitySelection
    Dim ofrmReports As New frmReports
    Dim sReportName1 As String = "Missing Email"

    ofrmGLEntitySelection.Component(sReportName1)
    ofrmGLEntitySelection.ShowDialog()
    If ofrmGLEntitySelection.Canceled Then Exit Sub
    moSettings.WaitCursor(Me)
    If ofrmReports.Component(sReportName1, vbNullString) Then
      If ofrmReports.DepartmentListMissingEMail(ofrmGLEntitySelection.GLEntityIDSelected) Then
        ofrmReports.MdiParent = Me
        moSettings.DefaultCursor(Me)
        ofrmReports.Show()
      Else
        moSettings.DefaultCursor(Me)
        OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
        Exit Sub
      End If
    Else
      moSettings.DefaultCursor(Me)
      OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
      Exit Sub
    End If
  End Sub

  Private Sub mnuTransactionDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTransactionDetail.Click
    Dim ofrmSelection02 As New frmSelection02
    Dim ofrmReports As New frmReports
    Dim sReportName1 As String = "G/L Transaction Detail"

    With ofrmSelection02
      .Component(sReportName1)
      .ShowDialog()
      If .Canceled Then Exit Sub
      moSettings.WaitCursor(Me)
      If ofrmReports.Component(sReportName1, vbNullString) Then
        If ofrmReports.GLTransactionDetail(.GLEntityIDSelected, .YearSelected, .MonthSelected, .ReportEmailSelected, .DepartmentSelected) Then
          ofrmReports.MdiParent = Me
          moSettings.DefaultCursor(Me)
          ofrmReports.Show()
        Else
          moSettings.DefaultCursor(Me)
          OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
          Exit Sub
        End If
      Else
        moSettings.DefaultCursor(Me)
        OKMessage(sReportName1, "Error Generating Report." & vbCrLf & "Processing Canceled!", 3)
        Exit Sub
      End If
    End With
  End Sub
#End Region

#Region "Window"
  Private Sub mnuCascade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCascade.Click
    Me.LayoutMdi(MdiLayout.Cascade)
  End Sub

  Private Sub mnuTileHorizontal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTileHorizontal.Click
    Me.LayoutMdi(MdiLayout.TileHorizontal)
  End Sub

  Private Sub mnuArrangeIcons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuArrangeIcons.Click
    Me.LayoutMdi(MdiLayout.ArrangeIcons)
  End Sub

  Private Sub mnuTileVertical_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTileVertical.Click
    Me.LayoutMdi(MdiLayout.TileVertical)
  End Sub
#End Region

#Region "Help"
  Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
    Dim ofrmAssemblyInformation As New frmAssemblyInformation

    With ofrmAssemblyInformation
      If ofrmAssemblyInformation.Component Then
        .MdiParent = Me
        .Show()
      Else
        Exit Sub
      End If
    End With
  End Sub
#End Region

#Region "Timers"
  Private Sub tmrBlink_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrBlink.Tick
    If miBlinkCount < 10 Then
      mbBlinkVisible = Not mbBlinkVisible
      tsslProcessing.Visible = mbBlinkVisible
      miBlinkCount += 1
      tmrBlink.Enabled = True
      tmrBlink.Interval = miBlinkInterval
    Else
      tmrBlink.Stop()
      tmrBlink.Dispose()
      tsslProcessing.Visible = mbBlinkVisible
      Me.Refresh()
    End If
  End Sub
#End Region
#End Region
End Class
